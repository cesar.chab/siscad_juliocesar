-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 11, 2020 at 02:02 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sislar`
--

-- --------------------------------------------------------

--
-- Table structure for table `articulo`
--

DROP TABLE IF EXISTS `articulo`;
CREATE TABLE IF NOT EXISTS `articulo` (
  `idarticulo` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) DEFAULT NULL,
  `codigo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `descripcion` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `ENABLED_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `PURCHASING_ITEM_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `SHIPPABLE_ITEM_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `CUSTOMER_ORDER_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `SERVICE_ITEM_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `INVENTORY_ITEM_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `ENG_ITEM_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `INVENTORY_ASSET_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `MTL_TRANSACTIONS_ENABLED_FLA` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `STOCK_ENABLED_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `RETURNABLE_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `QTY_RCV_EXCEPTION_CODE` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `ALLOW_ITEM_DESC_UPDATE_FLAG` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `INSPECTION_REQUIRED_FLAG` varchar(1) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `RECEIPT_REQUIRED_FLAG` varchar(1) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `RFQ_REQUIRED_FLAG` varchar(1) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `LIST_PRICE_PER_UNIT` decimal(10,0) DEFAULT NULL,
  `ASSET_CATEGORY_ID` int(11) NOT NULL,
  `DAYS_EARLY_RECEIPT_ALLOWED` int(11) NOT NULL,
  `DAYS_LATE_RECEIPT_ALLOWED` int(11) NOT NULL,
  `PRIMARY_UOM_CODE` varchar(3) COLLATE utf8_spanish2_ci NOT NULL,
  `INVENTORY_ITEM_STATUS_CODE` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `MIN_MINMAX_QUANTITY` int(11) NOT NULL,
  `MAX_MINMAX_QUANTITY` int(11) NOT NULL,
  `MINIMUM_ORDER_QUANTITY` int(11) NOT NULL,
  `MAXIMUM_ORDER_QUANTITY` int(11) DEFAULT NULL,
  `COSTING_ENABLED_FLAG` int(11) DEFAULT NULL,
  `INVOICEABLE_ITEM_FLAG` varchar(1) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ITEM_TYPE` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `CONSIGNED_FLAG` varchar(1) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `CRITICAL_COMPONENT_FLAG` varchar(1) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `LAST_UPDATED_BY` int(11) DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`idarticulo`),
  KEY `fk_articulo_categoria_idx` (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `articulo`
--

INSERT INTO `articulo` (`idarticulo`, `idcategoria`, `codigo`, `nombre`, `stock`, `descripcion`, `imagen`, `estado`, `ENABLED_FLAG`, `PURCHASING_ITEM_FLAG`, `SHIPPABLE_ITEM_FLAG`, `CUSTOMER_ORDER_FLAG`, `SERVICE_ITEM_FLAG`, `INVENTORY_ITEM_FLAG`, `ENG_ITEM_FLAG`, `INVENTORY_ASSET_FLAG`, `MTL_TRANSACTIONS_ENABLED_FLA`, `STOCK_ENABLED_FLAG`, `RETURNABLE_FLAG`, `QTY_RCV_EXCEPTION_CODE`, `ALLOW_ITEM_DESC_UPDATE_FLAG`, `INSPECTION_REQUIRED_FLAG`, `RECEIPT_REQUIRED_FLAG`, `RFQ_REQUIRED_FLAG`, `LIST_PRICE_PER_UNIT`, `ASSET_CATEGORY_ID`, `DAYS_EARLY_RECEIPT_ALLOWED`, `DAYS_LATE_RECEIPT_ALLOWED`, `PRIMARY_UOM_CODE`, `INVENTORY_ITEM_STATUS_CODE`, `MIN_MINMAX_QUANTITY`, `MAX_MINMAX_QUANTITY`, `MINIMUM_ORDER_QUANTITY`, `MAXIMUM_ORDER_QUANTITY`, `COSTING_ENABLED_FLAG`, `INVOICEABLE_ITEM_FLAG`, `ITEM_TYPE`, `CONSIGNED_FLAG`, `CRITICAL_COMPONENT_FLAG`, `updated_at`, `LAST_UPDATED_BY`, `CREATED_BY`, `created_at`) VALUES
(1, 1, '7701234000011', 'Impresora Epson Lx200', 8, '', 'impresora.jpg', 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(2, 1, '7702004003508', 'Impresora Epson M300', 2, '', 'Impresora Epson.jpeg', 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(4, 3, '5901234123457', 'Cable UTP Cat-5', 65, '', 'descarga.jpg', 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(5, 3, '8412345678905', 'Cable VGA 2Mt', 38, '', 'images.jpg', 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(6, 1, 'ABCDX', 'AAAA1111', 4, '3FFF', NULL, 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(7, NULL, 'ffff', 'ff', NULL, 'fffff', NULL, 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(8, NULL, 'ASD', 'DSDS', NULL, 'DSDSD', NULL, 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(9, NULL, 'ASDX', 'FF', NULL, '', NULL, 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(10, NULL, '01.DEA', 'CARTON DE PRADO', NULL, 'DESCRIPCION LARG', 'Captura de pantalla (1).png', 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00'),
(11, NULL, 'ABC', 'ASD', NULL, 'DDDDD', NULL, 'Activo', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) NOT NULL,
  `second_name` varchar(25) DEFAULT NULL,
  `first_last_name` varchar(25) NOT NULL,
  `second_last_name` varchar(25) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `effective_end_date` date DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `sex` varchar(1) NOT NULL,
  `tipo_documento` int(11) NOT NULL,
  `num_documento` varchar(25) NOT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `contacto` varchar(50) DEFAULT NULL,
  `telef1` varchar(20) NOT NULL,
  `telef2` varchar(25) DEFAULT NULL,
  `no_atender` tinyint(1) NOT NULL DEFAULT 0,
  `motivo_no_atencion` text DEFAULT NULL,
  `disccount` int(11) DEFAULT NULL,
  `country` varchar(25) DEFAULT NULL,
  `user_app` varchar(25) DEFAULT NULL,
  `pwd_app` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`idcliente`, `first_name`, `second_name`, `first_last_name`, `second_last_name`, `full_name`, `effective_end_date`, `date_of_birth`, `sex`, `tipo_documento`, `num_documento`, `email_address`, `contacto`, `telef1`, `telef2`, `no_atender`, `motivo_no_atencion`, `disccount`, `country`, `user_app`, `pwd_app`, `address`, `created_by`, `last_updated_by`, `updated_at`, `created_at`) VALUES
(1, 'MINISTERIO DE TRANSPORTE', '.', '.', '.', '. . MINISTERIO DE TRANSPORTE .', '2017-05-03', '2050-01-01', 'M', 0, '20550277493', NULL, 'juan torres alva / dni: 408450445', '(518) 25836940_', '(511) 999544455', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-04-14 06:10:25', '2017-04-13 05:55:53'),
(0, '-', '', '-', '-', 'SIN CLIENTE ASIGNADO', '2050-05-12', '2050-01-01', 'M', 0, '99999999999', '', '-', '(511) 2583640__', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-10 10:10:21', '2017-04-30 01:39:00'),
(4, 'ROGER', 'R', 'TORRES', 'GARCIA', 'TORRES GARCIA ROGER R', '2017-04-28', '2050-01-01', 'M', 0, '40845455', 'JUAN@100.COM', '.', '(556) 6666666__', '(511) 955262547', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-04-15 07:35:24', '2017-04-14 06:09:40'),
(5, 'MUNUCIPALIDAD DE LIMA', '.', '.', '.', '. . MUNUCIPALIDAD DE LIMA .', '2050-05-03', '2050-01-01', 'M', 0, '20550277469', 'INFO@MUN.COM', '.', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-10 10:16:49', '2017-04-15 07:20:11'),
(10, 'JORGE', 'JUAN', 'OBLITAS', 'TORRE', 'OBLITAS TORRE JORGE JUAN', '2017-06-09', '2050-01-01', 'M', 0, '40845457', 'julio.yanarico.c@gmail.com', 'CONTACTO ABC', '(511) 4551297', '(511) 997290987', 0, '', NULL, NULL, NULL, NULL, '', 1, 1, '2018-08-08 23:08:26', '2017-05-10 10:05:20'),
(6, 'REPUESTOS ALVA', '', '', '', '  REPUESTOS ALVA ', '2050-05-27', '2050-01-01', 'M', 1, '20550277489', '', 'miguel perez ', '(511) 2577648__', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-10 10:08:14', '2017-04-15 07:33:12'),
(8, 'TITO', 'PEDRO', 'SOTO', 'ROJAS', 'SOTO ROJAS TITO PEDRO', '2017-06-10', '2050-01-01', 'M', 0, '789654121', 'JUAN@100.COM', '-', '(511) 56651188', '(511) 991120937', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-03 10:17:30', '2017-05-03 10:16:32'),
(9, 'ANA', 'MARIA', 'ROJAS', 'VARGAS', 'ROJAS VARGAS ANA MARIA', '2050-06-10', '2050-01-01', 'F', 0, '104084545542', 'kkk@gmail.com', '.', '(515) 2589840__', '(511) 998882878', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-04 06:37:36', '2017-05-04 06:35:48'),
(11, 'ANA', 'TERESA', 'ROJAS', 'SOLAR', 'ROJAS SOLAR ANA TERESA', '2017-06-02', '2050-01-01', 'F', 0, '105454545', '', '-', '(511) 2589687', '(514) 878785541', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-10 10:20:00', '2017-05-10 10:18:35'),
(12, 'MINISTERIO DE EDUCACION', '-', '-', '-', '- - MINISTERIO DE EDUCACION -', '2050-06-02', '2050-01-01', 'M', 0, '9999999999', '', 'ANEXO 1040', '(511) _______', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-05-11 10:05:14', '2017-05-11 10:05:14'),
(13, 'julio', 'cesar', '-', '-', '- - julio cesar', '2017-05-31', '2050-01-01', 'M', 0, '', 'julio.yanarico.c@gmail.com', 'XX', '(511) 2583644', '(511) 977290838', 0, 'CCC', NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-07-25 06:30:00', '2017-05-11 10:06:47'),
(14, 'JAVIER', '-', 'BARRANTES', '-', 'BARRANTES - JAVIER -', '2018-06-01', '2050-01-01', 'M', 0, '', 'AJBE21@HOTMAIL.COM', '-ANTONY', '(511) _______', '', 0, 'UUU', NULL, NULL, NULL, NULL, NULL, 1, 10, '2017-07-26 08:24:17', '2017-05-11 10:08:17'),
(15, 'XCD', 'E', 'E', 'E', 'E E XCD E', '2017-05-24', '2050-01-01', 'M', 0, '11111', '', '.', '(1__) _______', '', 1, 'aaazz', NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-11 10:42:45', '2017-05-11 10:15:37'),
(16, 'QWE', 'A', 'X', 'FD', 'X FD QWE A', '2017-05-25', '2050-01-01', 'M', 0, 'FFFF', '', 'R', '(2__) _______', '', 1, 'VV', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-05-11 10:34:18', '2017-05-11 10:34:18'),
(17, 'daniel', '', 'ponce', 'messi', 'ponce messi daniel ', '2017-05-18', '2050-01-01', 'M', 0, '408454557', '', 'demo@gmail.com', '(511) 2583567', '(511) 99999999_', 1, 'cliente no paga a tiempo', NULL, NULL, NULL, NULL, '', 1, 1, '2018-06-20 23:43:13', '2017-05-25 10:44:50'),
(18, 'JUANA', '', 'CHIZA', 'MENDOZA', 'CHIZA MENDOZA JUANA ', '2019-01-01', '2050-01-01', 'M', 0, '23456789', '', 'JUANA', '(054) 258965_', '(054) 932555980', 0, '', NULL, NULL, NULL, NULL, 'AV MATEO SUAREZ 2540 / SURCO', 10, 1, '2018-06-20 23:38:01', '2017-06-22 08:39:04'),
(19, 'ANTONY', 'ROOSEVELT', 'TORBISCO', 'AVENDAÑO', 'TORBISCO AVENDAÑO ANTONY ROOSEVELT', '2017-08-01', '2050-01-01', 'M', 0, '48194256', 'MALACONY@HOTMAIL.COM', 'ROOSEVELT', '(054) 3001627', '(054) 93888991', 0, '', NULL, NULL, NULL, NULL, 'AV. VIA DE EVITAMIENTO', 10, 9, '2017-11-14 08:00:31', '2017-08-03 13:47:41'),
(20, 'CORPAC S.A.', '', '', '', '  CORPAC S.A. ', NULL, '2050-01-01', '', 1, '20100004675', 'PTABOADA@CORPAC.GOB.PE', 'PATRICIA TABOADA', '', '(054) 987999328', 0, '', NULL, NULL, NULL, NULL, 'AV. ELMER FAUCET S/N CALLAO', 9, 9, '2017-09-27 12:20:19', '2017-09-27 12:20:19'),
(21, 'CCFFAA', '', '', '', '  CCFFAA ', NULL, '2050-01-01', '', 0, '20131380870', '', 'TEC. NINANYA ', '', '', 0, '', NULL, NULL, NULL, NULL, 'JR. NICOLAS CORPANCHO N° 289 - SANTA BEATRIZ - LIM', 9, 9, '2017-11-14 09:22:56', '2017-11-14 09:22:56'),
(22, 'M Y M CONSULTORES S.R.L', '', '', '', '  M Y M CONSULTORES S.R.L ', NULL, '2050-01-01', '', 1, '20102287437', 'MYMCONS@QNET.COM.PE', '', '(054) 3725281', '', 0, '', NULL, NULL, NULL, NULL, 'CAL.MAYORAZGO N°159 LIMA SAN BORJA', 9, 9, '2017-11-14 15:46:06', '2017-11-14 15:46:06'),
(23, 'JANET', 'JARVIS', 'CASTILLO', 'ALVARADO', 'CASTILLO ALVARADO JANET JARVIS', NULL, '2050-01-01', 'M', 0, '4197513', '', 'JANET ALVARADO', '', '(054) 952229254', 0, '', NULL, NULL, NULL, NULL, 'JR.GENARO COBIAN 198 DPTO 303', 9, 9, '2017-11-14 16:09:03', '2017-11-14 16:09:03'),
(24, 'JULIO', 'CESAR', 'MENDOZA', 'CASTILLO', 'ALVARADO CASTILLO JANET JARVIS', NULL, '2050-01-01', 'M', 0, '41975713', '', '', '', '(054) 950444454', 0, '', NULL, NULL, NULL, NULL, '', 9, 9, '2017-11-14 16:14:05', '2017-11-14 16:14:05'),
(25, 'RESINPLAST SA', '', '', '', '  RESINPLAST SA ', NULL, '2050-01-01', '', 1, '20100065038', 'gjauregui@resinplast.com.pe', 'GUILLERMO JAUREGI', '', '(054) 998344460', 0, '', NULL, NULL, NULL, NULL, 'AV. BENJAMIN FRANKLIN NRO.233 SANTA ROSA LIMA LIMA', 9, 9, '2017-11-14 16:20:41', '2017-11-14 16:20:41'),
(26, 'MINISTERIO DEL INTERIOR', '', '', '', '  MINISTERIO DEL INTERIOR ', NULL, '2050-01-01', '', 1, '20131366966', '', '', '', '', 0, '', NULL, NULL, NULL, NULL, 'PLAZA 30 DE AGOSTO N° 150 LIMA LIMA SAN ISIDRO', 9, 9, '2017-11-14 16:22:33', '2017-11-14 16:21:45'),
(27, 'MINISTERIO DE SALUD', '', '', '', '  MINISTERIO DE SALUD ', NULL, '2050-01-01', '', 0, '20131373237', '', '', '', '', 0, '', NULL, NULL, NULL, NULL, 'AV. SALAVERRY N°801 LIMA LIMA JESUS MARIA', 9, 9, '2017-11-14 16:24:31', '2017-11-14 16:23:55'),
(28, 'MUNICIPALIDAD DE LIMA', '', '', '', '  MUNICIPALIDAD DE LIMA ', NULL, '2050-01-01', '', 1, '20131380951', '', '', '', '', 0, '', NULL, NULL, NULL, NULL, 'JR.CAMANA N°564 LIMA LIMA LIMA', 9, 9, '2017-11-14 16:28:36', '2017-11-14 16:28:36'),
(29, 'MINISTERIO DE CULTURA', '', '', '', '  MINISTERIO DE CULTURA ', NULL, '2050-01-01', '', 1, '20537630222', '', '', '', '', 0, '', NULL, NULL, NULL, NULL, 'AV.JAVIER PRADO ESTE NRO° 2465 LIMA LIMA SAN BORJA', 9, 9, '2017-11-14 16:34:09', '2017-11-14 16:34:09'),
(30, 'TECNOFLEX S.AC.', '', 'TECNOFLEX S.A.C', '', '.', NULL, '2050-01-01', 'F', 0, '', '', '', '', '(054) 944576699', 0, '', NULL, NULL, NULL, NULL, '', 9, 9, '2017-11-14 17:45:43', '2017-11-14 17:45:43'),
(31, 'MINISTERIO DE SALUD', '', '', '', '  MINISTERIO DE SALUD ', NULL, '2050-01-01', '', 1, '00000', '', '', '', '', 0, '', NULL, NULL, NULL, NULL, '', 9, 9, '2017-11-15 15:09:54', '2017-11-15 15:09:54'),
(32, 'CONEXION AUTOMOTRIZ', '', '', '', '  CONEXION AUTOMOTRIZ ', '2099-01-01', '2050-01-01', 'M', 1, '48194256', 'malacony@hotmail.com', 'ANTONY TORBISCO ', '', '', 0, '', NULL, NULL, NULL, NULL, 'AV. VIA DE EVITAMIENTO 989 ', 9, 9, '2018-03-06 08:44:38', '2018-03-06 08:44:38'),
(33, 'francisco', '', 'velasquez', '', 'velasquez  francisco ', '2099-01-01', '2050-01-01', 'M', 0, '02816978', '', '', '', '', 0, '', NULL, NULL, NULL, NULL, '', 9, 9, '2018-03-19 18:03:13', '2018-03-19 18:03:13'),
(34, 'RENZO', '', 'TAIPE', 'HUAMANI', 'TAIPE HUAMANI RENZO ', '2099-01-01', '2050-01-01', 'M', 0, '46477687', '', '', '', '(051) 992037375', 0, '', NULL, NULL, NULL, NULL, 'SURCO', 9, 9, '2018-04-25 09:43:15', '2018-04-25 09:43:15'),
(35, 'JULIO 2', NULL, 'YANARICOF', '', 'YANARICOF JULIO 2', '2099-01-01', '1980-05-22', '', 0, '104050225', 'JULIO.YANARICO.C@GMAIL.COM', NULL, '2583640', NULL, 0, NULL, NULL, 'SURCO', NULL, NULL, 'AV LIMA 205 SURCO', 1, 1, '2020-05-11 11:20:13', '2020-05-11 11:20:13'),
(36, 'JUAN ', NULL, 'MENDOZA VARA', '', 'MENDOZA VARA JUAN ', '2099-01-01', '1980-05-14', '', 0, '10408054225', 'JULIO.YANARICO@ASVNETS.COM', NULL, '932266980', NULL, 0, NULL, NULL, 'SURCO', NULL, NULL, 'AV LIMA  20 55 0', 1, 1, '2020-05-18 01:41:07', '2020-05-18 01:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `detalle_ingreso`
--

DROP TABLE IF EXISTS `detalle_ingreso`;
CREATE TABLE IF NOT EXISTS `detalle_ingreso` (
  `iddetalle_ingreso` int(11) NOT NULL AUTO_INCREMENT,
  `idingreso` int(11) NOT NULL,
  `idarticulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_compra` decimal(11,2) NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  PRIMARY KEY (`iddetalle_ingreso`),
  KEY `fk_detalle_ingreso_idx` (`idingreso`),
  KEY `fk_detalle_ingreso_articulo_idx` (`idarticulo`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `detalle_ingreso`
--

INSERT INTO `detalle_ingreso` (`iddetalle_ingreso`, `idingreso`, `idarticulo`, `cantidad`, `precio_compra`, `precio_venta`) VALUES
(18, 13, 1, 10, '500.00', '600.00'),
(19, 13, 4, 100, '1.00', '2.00'),
(20, 13, 5, 50, '10.00', '15.00'),
(21, 14, 1, 1, '10.00', '15.00'),
(22, 15, 1, 1, '10.00', '15.00'),
(23, 16, 1, 1, '10.00', '15.00'),
(24, 17, 2, 10, '150.00', '200.00');

--
-- Triggers `detalle_ingreso`
--
DROP TRIGGER IF EXISTS `tr_updStockIngreso`;
DELIMITER $$
CREATE TRIGGER `tr_updStockIngreso` AFTER INSERT ON `detalle_ingreso` FOR EACH ROW BEGIN
	UPDATE articulo SET stock = stock + NEW.cantidad 
	WHERE articulo.idarticulo = NEW.idarticulo;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detalle_venta`
--

DROP TABLE IF EXISTS `detalle_venta`;
CREATE TABLE IF NOT EXISTS `detalle_venta` (
  `iddetalle_venta` int(11) NOT NULL AUTO_INCREMENT,
  `idventa` int(11) NOT NULL,
  `idarticulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  `descuento` decimal(11,2) NOT NULL,
  PRIMARY KEY (`iddetalle_venta`),
  KEY `fk_detalle_venta_articulo_idx` (`idarticulo`),
  KEY `fk_detalle_venta_idx` (`idventa`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `detalle_venta`
--

INSERT INTO `detalle_venta` (`iddetalle_venta`, `idventa`, `idarticulo`, `cantidad`, `precio_venta`, `descuento`) VALUES
(1, 1, 1, 10, '15.00', '0.00'),
(2, 4, 2, 1, '303.60', '0.00'),
(3, 5, 4, 1, '6.60', '0.00'),
(4, 6, 1, 1, '52.60', '0.60'),
(5, 6, 2, 1, '303.60', '0.30'),
(6, 7, 5, 1, '25.00', '0.00'),
(7, 8, 4, 37, '6.60', '0.00'),
(8, 8, 5, 18, '25.00', '0.00'),
(9, 8, 2, 45, '303.60', '0.00'),
(10, 8, 1, 41, '52.60', '0.00'),
(11, 9, 5, 1, '25.00', '0.00'),
(12, 9, 4, 1, '6.60', '0.00'),
(13, 10, 5, 2, '15.00', '0.00'),
(14, 10, 1, 1, '600.00', '50.00'),
(15, 11, 5, 1, '15.00', '0.00'),
(16, 12, 5, 1, '15.00', '0.00'),
(17, 13, 5, 2, '15.00', '0.00'),
(18, 13, 4, 5, '2.00', '0.00'),
(19, 14, 1, 1, '161.25', '0.00'),
(20, 15, 5, 2, '15.00', '1.00'),
(21, 16, 2, 4, '200.00', '0.00'),
(22, 16, 4, 30, '2.00', '2.00'),
(23, 17, 1, 10, '161.25', '0.00'),
(24, 17, 5, 1, '15.00', '2.00'),
(25, 18, 1, 1, '161.25', '0.00'),
(26, 19, 2, 4, '200.00', '0.00'),
(27, 19, 5, 3, '15.00', '0.00');

--
-- Triggers `detalle_venta`
--
DROP TRIGGER IF EXISTS `tr_updStockVenta`;
DELIMITER $$
CREATE TRIGGER `tr_updStockVenta` AFTER INSERT ON `detalle_venta` FOR EACH ROW BEGIN
	UPDATE articulo SET stock = stock - NEW.cantidad 
	WHERE articulo.idarticulo = NEW.idarticulo;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `eam_work_material`
--

DROP TABLE IF EXISTS `eam_work_material`;
CREATE TABLE IF NOT EXISTS `eam_work_material` (
  `eam_material_id` int(11) NOT NULL AUTO_INCREMENT,
  `wip_entity_id` int(11) NOT NULL,
  `wip_line_id` int(11) NOT NULL,
  `orden` int(11) NOT NULL DEFAULT 0,
  `type_material` varchar(255) DEFAULT NULL COMMENT 'L=> LABORES\r\nM=> MATERIAL',
  `item_id` int(11) NOT NULL,
  `item_descripcion` varchar(50) DEFAULT NULL,
  `operation` int(11) NOT NULL DEFAULT 10,
  `is_manual` tinyint(1) NOT NULL DEFAULT 0,
  `quantity` decimal(10,2) NOT NULL,
  `uom_code` varchar(25) DEFAULT NULL,
  `unit_price` decimal(10,2) DEFAULT NULL,
  `quantity_delivered` decimal(10,2) NOT NULL DEFAULT 0.00,
  `status_delivered` varchar(25) NOT NULL DEFAULT 'UNDELIVERED',
  `required_date` date NOT NULL,
  `source_material` varchar(25) NOT NULL,
  `source_line_id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`eam_material_id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eam_work_material`
--

INSERT INTO `eam_work_material` (`eam_material_id`, `wip_entity_id`, `wip_line_id`, `orden`, `type_material`, `item_id`, `item_descripcion`, `operation`, `is_manual`, `quantity`, `uom_code`, `unit_price`, `quantity_delivered`, `status_delivered`, `required_date`, `source_material`, `source_line_id`, `enabled`, `created_by`, `last_updated_by`, `updated_at`, `created_at`) VALUES
(1, 27, 116, 0, NULL, 621, 'BUJIAS DE ITRIO ', 10, 0, '4.00', NULL, '10.00', '3.10', 'PARTIAL', '2018-01-10', 'Quotation', NULL, 1, 1, 1, '2018-03-18 06:20:00', '2018-01-10 19:33:08'),
(2, 27, 115, 0, NULL, 387, 'FILTRO DE AIRE FILTRON', 10, 0, '2.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-01-10', 'Quotation', NULL, 1, 1, 1, '2018-01-10 19:33:08', '2018-01-10 19:33:08'),
(3, 27, 114, 0, NULL, 875, 'FILTRO DE ACEITE FILTRON', 10, 0, '1.00', NULL, '80.00', '1.00', 'FULLY', '2018-01-10', 'Quotation', NULL, 1, 1, 1, '2018-01-10 19:33:08', '2018-01-10 19:33:08'),
(4, 27, 113, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '4.00', NULL, '120.00', '1.70', 'PARTIAL', '2018-01-10', 'Quotation', NULL, 1, 1, 1, '2018-01-10 19:33:08', '2018-01-10 19:33:08'),
(5, 27, 112, 0, NULL, 621, 'BUJIAS DE ITRIO ', 10, 0, '4.00', NULL, '12.00', '3.00', 'PARTIAL', '2018-01-10', 'Quotation', NULL, 1, 1, 1, '2018-01-10 19:33:08', '2018-01-10 19:33:08'),
(6, 28, 127, 0, NULL, 988, 'ACEITE MINERAL 15W40 2 GL', 10, 0, '1.00', NULL, '176.00', '1.00', 'FULLY', '2018-01-11', 'Quotation', NULL, 1, 1, 1, '2018-01-11 04:43:44', '2018-01-11 04:43:44'),
(7, 28, 128, 0, NULL, 80, 'FILTRO DE AIRE ALT.', 10, 0, '1.00', NULL, '65.00', '1.00', 'FULLY', '2018-01-11', 'Quotation', NULL, 1, 1, 1, '2018-01-11 04:43:44', '2018-01-11 04:43:44'),
(8, 28, 19, 0, NULL, 1652, 'UNA MANGUERA DE RADIADOR ', 10, 1, '1.00', NULL, '10.00', '1.00', 'FULLY', '2018-01-11', 'Quotation', NULL, 1, 1, 1, '2018-01-11 04:43:44', '2018-01-11 04:43:44'),
(9, 29, 59, 0, NULL, 860, 'FILTRO DE ACEITE WIX', 10, 0, '20.00', NULL, '25.00', '20.00', 'FULLY', '2018-01-11', 'Quotation', NULL, 1, 1, 1, '2019-03-23 09:17:34', '2018-01-11 04:55:33'),
(10, 30, 1, 0, NULL, 1229, ' ', 10, 0, '1.00', NULL, '90.00', '1.00', 'FULLY', '2018-01-15', 'Quotation', 42, 1, 1, 1, '2019-03-23 10:08:57', '2018-01-15 08:01:45'),
(11, 30, 2, 0, NULL, 1230, 'ROTULA SUPERIOR', 10, 0, '1.00', NULL, '126.00', '1.00', 'FULLY', '2018-01-15', 'Quotation', 43, 1, 1, 1, '2019-03-23 10:08:57', '2018-01-15 08:01:45'),
(12, 30, 3, 0, NULL, 1234, 'RODAJE DE BOCAMAZA DEL.', 10, 0, '1.00', NULL, '200.00', '1.00', 'FULLY', '2018-01-15', 'Quotation', 44, 1, 1, 1, '2019-03-23 10:08:57', '2018-01-15 08:01:45'),
(13, 30, 4, 0, NULL, 1231, 'ROTULA INFERIOR', 10, 0, '1.00', NULL, '135.00', '1.00', 'FULLY', '2018-01-15', 'Quotation', 45, 1, 1, 1, '2019-03-23 10:08:58', '2018-01-15 08:01:45'),
(14, 31, 1, 0, NULL, 859, 'FILTRO DE ACEITE BOSCH', 10, 0, '6.00', NULL, '20.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 130, 1, 1, 1, '2018-01-15 08:53:26', '2018-01-15 08:53:26'),
(15, 31, 2, 0, NULL, 1652, 'CAMBIO DE PINES CALIPER DE FRENO', 10, 1, '1.00', NULL, '90.00', '1.00', 'FULLY', '2018-01-15', 'Quotation', 28, 1, 1, 1, '2018-12-22 07:41:14', '2018-01-15 08:53:26'),
(16, 31, 3, 0, NULL, 1652, ' CAMBIO PASTILLAS DE FRENO DELANTERAS', 10, 1, '1.00', NULL, '180.00', '1.00', 'FULLY', '2018-01-15', 'Quotation', 27, 1, 1, 1, '2020-05-03 12:36:50', '2018-01-15 08:53:26'),
(17, 31, 4, 0, NULL, 1652, 'CAMBIO DE 2 DISCOS DE FRENO', 10, 1, '1.00', NULL, '560.00', '0.50', 'PARTIAL', '2018-01-15', 'Quotation', 26, 1, 1, 1, '2018-07-06 22:57:04', '2018-01-15 08:53:26'),
(18, 32, 1, 0, NULL, 978, 'ACEITE MINERAL SHELL 10W30 1 1/2 GL', 17, 0, '1.00', NULL, '150.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 88, 1, 1, 1, '2018-01-15 13:46:03', '2018-01-15 13:46:03'),
(19, 32, 2, 0, NULL, 465, 'FILTRO DE AIRE ALT.', 17, 0, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 87, 1, 1, 1, '2018-01-15 13:46:03', '2018-01-15 13:46:03'),
(20, 32, 3, 0, NULL, 999, 'LIQUIDO DE FRENO BOSCH DOT 4 500ML', 17, 0, '2.00', NULL, '20.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 86, 1, 1, 1, '2018-01-15 13:46:03', '2018-01-15 13:46:03'),
(21, 33, 4, 0, NULL, 618, 'BUJIAS DE ITRIO ', 10, 0, '6.00', NULL, '9.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Manual', NULL, 1, 1, 1, '2018-01-31 00:51:26', '2018-01-31 00:51:26'),
(22, 33, 2, 0, NULL, 1652, 'mat 1 9', 10, 1, '1.00', NULL, '154.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 32, 1, 1, 1, '2018-01-15 13:57:09', '2018-01-15 13:57:09'),
(23, 33, 3, 0, NULL, 1652, 'rep man 2', 10, 1, '1.00', NULL, '94.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 33, 1, 1, 1, '2018-01-15 13:57:09', '2018-01-15 13:57:09'),
(24, 34, 34, 0, NULL, 518, 'FILTRO DE AIRE ALT.', 19, 0, '1.00', NULL, '50.00', '1.00', 'UNDELIVERED', '2018-02-09', 'Manual', NULL, 1, 1, 1, '2018-01-26 23:32:19', '2018-01-26 23:32:19'),
(120, 35, 4, 0, NULL, 1652, 'aaa1 ffftt gggg tttttttt', 10, 1, '3.00', NULL, '90.00', '3.00', 'FULLY', '2018-07-18', 'Manual', NULL, 1, 1, 1, '2020-05-03 12:35:38', '2018-07-18 08:29:24'),
(121, 35, 4, 0, NULL, 1652, ' bb2ggggg ttty yyyy uuuuuuuuuuuu ggggg', 10, 1, '5.00', NULL, '70.00', '0.00', 'UNDELIVERED', '2018-07-18', 'Manual', NULL, 1, 1, 1, '2018-07-18 08:28:53', '2018-07-18 08:28:53'),
(122, 35, 4, 0, NULL, 1652, ' ccc3', 10, 1, '2.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-07-18', 'Manual', NULL, 1, 1, 1, '2018-07-18 08:27:57', '2018-07-18 08:27:57'),
(28, 36, 1, 0, NULL, 621, 'BUJIAS DE ITRIO ', 10, 0, '4.00', NULL, '10.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 116, 1, 1, 1, '2018-01-15 16:14:48', '2018-01-15 16:14:48'),
(29, 36, 2, 0, NULL, 387, 'FILTRO DE AIRE FILTRON', 10, 0, '1.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 115, 1, 1, 1, '2018-01-15 16:14:48', '2018-01-15 16:14:48'),
(30, 36, 3, 0, NULL, 875, 'FILTRO DE ACEITE FILTRON', 10, 0, '1.00', NULL, '80.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 114, 1, 1, 1, '2018-01-15 16:14:48', '2018-01-15 16:14:48'),
(31, 36, 4, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '1.00', NULL, '120.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 113, 1, 1, 1, '2018-01-15 16:14:48', '2018-01-15 16:14:48'),
(32, 36, 5, 0, NULL, 621, 'BUJIAS DE ITRIO ', 10, 0, '4.00', NULL, '12.00', '0.00', 'UNDELIVERED', '2018-01-15', 'Quotation', 112, 1, 1, 1, '2018-01-15 16:14:48', '2018-01-15 16:14:48'),
(33, 37, 1, 0, NULL, 387, 'FILTRO DE AIRE FILTRON', 10, 0, '4.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 147, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(34, 37, 2, 0, NULL, 999, 'LIQUIDO DE FRENO BOSCH DOT 4 500ML', 10, 0, '6.00', NULL, '20.00', '0.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 146, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(35, 37, 3, 0, NULL, 621, 'BUJIAS DE ITRIO ', 10, 0, '4.00', NULL, '10.00', '0.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 145, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(36, 37, 4, 0, NULL, 387, 'FILTRO DE AIRE FILTRON', 10, 0, '1.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 144, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(37, 37, 5, 0, NULL, 875, 'FILTRO DE ACEITE FILTRON', 10, 0, '1.00', NULL, '80.00', '0.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 143, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(38, 37, 6, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '1.00', NULL, '120.00', '0.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 142, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(39, 37, 7, 0, NULL, 621, 'BUJIAS DE ITRIO ', 10, 0, '4.00', NULL, '12.00', '0.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 141, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(40, 37, 8, 0, NULL, 1652, 'REP MAN 1', 10, 1, '3.00', NULL, '15.00', '0.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 34, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(41, 37, 9, 0, NULL, 1652, 'REP MAN 2', 10, 1, '5.00', NULL, '20.00', '1.00', 'UNDELIVERED', '2018-01-17', 'Quotation', 35, 1, 1, 1, '2018-01-17 00:09:03', '2018-01-17 00:09:03'),
(42, 34, 34, 0, NULL, 343, 'FAJA DE ACCESORIOS', 10, 0, '2.00', NULL, '72.00', '2.00', 'UNDELIVERED', '2018-02-02', 'Manual', NULL, 1, 1, 1, '2018-01-26 23:26:11', '2018-01-26 23:26:11'),
(43, 34, 34, 0, NULL, 844, 'ZAPATAS DE FRENO', 10, 0, '5.00', NULL, '160.00', '0.00', 'UNDELIVERED', '2018-03-10', 'Manual', NULL, 1, 1, 1, '2018-01-26 23:32:37', '2018-01-26 23:32:37'),
(44, 26, 3, 0, NULL, 999, 'LIQUIDO DE FRENO BOSCH DOT 4 500ML', 10, 0, '5.00', NULL, '80.15', '0.00', 'UNDELIVERED', '2018-01-29', 'Manual', NULL, 1, 1, 1, '2018-01-29 23:29:14', '2018-01-29 23:29:14'),
(45, 26, 2, 0, NULL, 999, 'LIQUIDO DE FRENO BOSCH DOT 4 500ML', 10, 0, '3.00', NULL, '20.46', '0.00', 'UNDELIVERED', '2018-01-29', 'Manual', NULL, 1, 1, 1, '2018-01-29 23:30:39', '2018-01-29 23:30:39'),
(46, 26, 1, 0, NULL, 74, 'RETEN', 10, 0, '1.00', NULL, '3.00', '1.00', 'UNDELIVERED', '2018-01-29', 'Manual', NULL, 1, 1, 1, '2018-01-29 23:08:13', '2018-01-29 23:08:13'),
(47, 26, 26, 0, NULL, 999, 'LIQUIDO DE FRENO BOSCH DOT 4 500ML', 20, 0, '3.00', NULL, '4.00', '0.00', 'UNDELIVERED', '2018-01-29', 'Manual', NULL, 1, 1, 1, '2018-01-29 23:56:54', '2018-01-29 23:56:54'),
(48, 36, 6, 0, NULL, 1639, 'AMORTIGUADOR DELANTERO L/R FMX', 10, 0, '2.00', NULL, '120.00', '1.00', 'PARTIAL', '2018-03-19', 'Manual', NULL, 1, 1, 1, '2018-03-19 05:22:22', '2018-03-19 05:14:16'),
(49, 38, 1, 0, NULL, 73, 'FILTRO DE AIRE ALT.', 10, 0, '3.00', NULL, '60.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 192, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(50, 38, 2, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '2.00', NULL, '90.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 193, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(51, 38, 3, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '2.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 194, 0, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(52, 38, 4, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '3.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 195, 0, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(53, 38, 5, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '1.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 196, 0, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(54, 38, 6, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '4.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 197, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(55, 38, 7, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '5.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 198, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(56, 38, 8, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '2.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 199, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(57, 38, 9, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '1.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 200, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(58, 38, 10, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '2.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 201, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(59, 38, 11, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '3.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 202, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(60, 38, 12, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 0, '4.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 203, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(61, 38, 13, 0, NULL, 1652, '2item', 10, 1, '1.00', NULL, '3.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 98, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(62, 38, 14, 0, NULL, 1652, '1item', 10, 1, '1.00', NULL, '5.00', '0.00', 'UNDELIVERED', '2018-04-01', 'Quotation', 97, 1, 1, 1, '2018-04-01 19:24:25', '2018-04-01 19:24:25'),
(63, 39, 1, 0, NULL, 976, 'ACEITE MINERAL SHELL 10W30 1GL', 10, 1, '4.00', NULL, '85.00', '4.00', 'FULLY', '2018-05-25', 'Quotation', 204, 1, 1, 1, '2020-05-03 12:37:46', '2018-05-25 06:31:05'),
(64, 39, 2, 0, NULL, 372, 'FILTRO DE AIRE MANN FILTER ', 10, 0, '1.00', NULL, '55.00', '0.00', 'UNDELIVERED', '2018-05-25', 'Quotation', 205, 1, 1, 1, '2018-07-09 06:37:32', '2018-05-25 06:31:05'),
(65, 39, 3, 0, NULL, 1652, 'PERNO', 10, 1, '2.00', NULL, '17.00', '2.00', 'FULLY', '2018-05-25', 'Quotation', 99, 1, 1, 1, '2020-05-03 18:20:07', '2018-05-25 06:31:05'),
(66, 39, 7, 0, NULL, 1652, 'PASTILLAS DE FRENO DEL.', 10, 1, '1.00', NULL, '3.00', '0.00', 'UNDELIVERED', '2018-07-14', 'Manual', NULL, 0, 1, 1, '2018-07-14 06:48:28', '2018-07-14 06:48:28'),
(67, 39, 7, 0, NULL, 1652, 'RADIADOR 50', 10, 1, '4.00', NULL, '15.00', '2.00', 'PARTIAL', '2018-07-17', 'Manual', NULL, 1, 1, 1, '2020-05-04 09:57:19', '2018-07-16 21:46:02'),
(68, 40, 1, 0, NULL, 465, 'FILTRO DE AIRE ALT.', 10, 0, '1.00', NULL, '1.00', '1.00', 'FULLY', '2018-06-22', 'Quotation', 84, 1, 1, 1, '2018-06-22 04:45:12', '2018-06-22 04:45:12'),
(69, 40, 2, 0, NULL, 999, 'LIQUIDO DE FRENO BOSCH DOT 4 500ML', 10, 0, '1.00', NULL, '20.00', '0.00', 'UNDELIVERED', '2018-06-22', 'Quotation', 85, 1, 1, 1, '2018-06-22 04:45:12', '2018-06-22 04:45:12'),
(70, 39, 7, 0, NULL, 999, 'LIQUIDO DE FRENO BOSCH DOT 4 500ML', 10, 0, '6.00', NULL, '35.00', '1.50', 'PARTIAL', '2018-06-30', 'Manual', NULL, 1, 1, 1, '2018-12-21 01:38:20', '2018-06-30 06:26:11'),
(71, 41, 1, 0, NULL, 844, 'ZAPATAS DE FRENO', 10, 0, '4.00', NULL, '250.00', '4.00', 'UNDELIVERED', '2018-07-02', 'Quotation', 249, 1, 1, 1, '2018-07-02 23:02:11', '2018-07-02 23:02:11'),
(72, 42, 1, 0, NULL, 844, 'ZAPATAS DE FRENO', 10, 0, '2.00', NULL, '250.00', '0.00', 'UNDELIVERED', '2018-07-07', 'Quotation', 250, 1, 1, 1, '2018-07-07 06:46:39', '2018-07-07 06:46:39'),
(73, 43, 1, 0, NULL, 992, 'TEMPLADOR DE FAJA ', 10, 0, '3.00', NULL, '50.00', '3.00', 'FULLY', '2018-07-07', 'Quotation', 251, 1, 1, 1, '2020-05-27 10:53:22', '2018-07-07 22:54:03'),
(74, 43, 2, 0, NULL, 909, 'FILTRO DE COMBUSTIBLE BOSCH', 10, 0, '2.00', NULL, '70.00', '0.00', 'UNDELIVERED', '2018-07-07', 'Quotation', 252, 1, 1, 1, '2018-07-07 22:54:03', '2018-07-07 22:54:03'),
(75, 43, 3, 0, NULL, 1652, 'xxxx', 10, 1, '1.00', NULL, '100.00', '1.00', 'FULLY', '2018-07-07', 'Quotation', 44, 1, 1, 1, '2018-07-07 22:57:17', '2018-07-07 22:54:03'),
(76, 44, 1, 0, NULL, 978, 'ACEITE MINERAL SHELL 10W30 1 1/2 GL', 10, 0, '3.00', NULL, '150.00', '0.00', 'UNDELIVERED', '2018-07-07', 'Quotation', 253, 1, 1, 1, '2018-07-07 23:07:00', '2018-07-07 23:07:00'),
(77, 44, 2, 0, NULL, 73, 'FILTRO DE AIRE ALT.', 10, 0, '2.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-07-07', 'Quotation', 254, 1, 1, 1, '2018-07-07 23:07:00', '2018-07-07 23:07:00'),
(78, 44, 3, 0, NULL, 1652, 'aaaaa', 10, 1, '1.00', NULL, '50.00', '1.00', 'FULLY', '2018-07-07', 'Quotation', 45, 1, 1, 1, '2018-07-07 23:10:45', '2018-07-07 23:07:00'),
(79, 44, 4, 0, NULL, 1652, 'bbbbbb', 10, 1, '1.00', NULL, '20.00', '1.00', 'FULLY', '2018-07-07', 'Quotation', 46, 1, 1, 1, '2018-07-07 23:10:45', '2018-07-07 23:07:00'),
(83, 31, 7, 0, NULL, 1652, 'bbt', 10, 1, '20.00', NULL, '5.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual', NULL, 1, 1, 1, '2018-07-17 04:44:16', '2018-07-17 04:44:16'),
(84, 31, 7, 0, NULL, 1652, 'vgghh6', 10, 1, '2.00', NULL, '120.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual', NULL, 1, 1, 1, '2018-07-17 04:44:23', '2018-07-17 04:44:23'),
(85, 45, 1, 0, NULL, 664, 'PASTILLAS DE FRENO DEL.', 10, 0, '5.00', NULL, '165.00', '0.00', 'UNDELIVERED', '2018-07-16', 'Quotation', 271, 1, 1, 1, '2018-07-16 05:17:13', '2018-07-16 05:17:13'),
(86, 45, 2, 0, NULL, 909, 'FILTRO DE COMBUSTIBLE BOSCH', 10, 0, '7.00', NULL, '65.00', '0.00', 'UNDELIVERED', '2018-07-16', 'Quotation', 272, 1, 1, 1, '2018-07-16 05:17:13', '2018-07-16 05:17:13'),
(87, 45, 3, 0, NULL, 1543, 'AMORTIGUADOR POSTERIOR L/R TOKICO', 10, 0, '1.00', NULL, '130.00', '0.00', 'UNDELIVERED', '2018-07-16', 'Quotation', 273, 1, 1, 1, '2018-07-16 05:17:13', '2018-07-16 05:17:13'),
(88, 45, 4, 0, NULL, 1652, 'gggg', 10, 1, '1.00', NULL, '10.00', '0.00', 'UNDELIVERED', '2018-07-16', 'Quotation', 57, 1, 1, 1, '2018-07-16 05:17:13', '2018-07-16 05:17:13'),
(89, 45, 5, 0, NULL, 1652, 'iiiiiii', 10, 1, '1.00', NULL, '30.00', '0.00', 'UNDELIVERED', '2018-07-16', 'Quotation', 58, 1, 1, 1, '2018-07-16 05:17:13', '2018-07-16 05:17:13'),
(90, 46, 1, 0, NULL, 518, 'FILTRO DE AIRE ALT.', 10, 0, '4.00', NULL, '60.00', '0.00', 'UNDELIVERED', '2018-07-16', 'Quotation', 269, 1, 1, 1, '2018-07-16 10:26:06', '2018-07-16 10:26:06'),
(91, 46, 2, 0, NULL, 844, 'ZAPATAS DE FRENO', 10, 0, '2.00', NULL, '250.00', '0.00', 'UNDELIVERED', '2018-07-16', 'Quotation', 270, 1, 1, 1, '2018-07-16 10:26:06', '2018-07-16 10:26:06'),
(92, 46, 3, 0, NULL, 1652, 'ggggg', 10, 1, '1.00', NULL, '6.00', '0.00', 'UNDELIVERED', '2018-07-16', 'Quotation', 56, 1, 1, 1, '2018-07-16 10:26:06', '2018-07-16 10:26:06'),
(93, 39, 2, 0, NULL, 1652, 'TUERCA RUEDA', 10, 1, '1.00', NULL, '40.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-16 21:47:07', '2018-07-16 21:47:07'),
(94, 39, 2, 0, NULL, 1652, ' SOLAPA MOTOR', 10, 1, '2.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-16 21:47:22', '2018-07-16 21:47:22'),
(95, 39, 2, 0, NULL, 1652, 'ZAPATA DE FRENO', 10, 1, '3.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-16 21:47:37', '2018-07-16 21:47:37'),
(96, 39, 2, 0, NULL, 1652, 'AGUUA RADIADOR', 10, 1, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-16 21:49:13', '2018-07-16 21:49:13'),
(97, 39, 2, 0, NULL, 1652, 'FILTRO DE ACEITE', 10, 1, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-16 21:51:09', '2018-07-16 21:51:09'),
(98, 39, 2, 0, NULL, 1652, 'RADIADOR MOTOR', 10, 1, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-16 21:51:37', '2018-07-16 21:51:37'),
(99, 39, 2, 0, NULL, 1652, 'BATERIA DELL', 10, 1, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-16 21:53:04', '2018-07-16 21:53:04'),
(100, 39, 2, 0, NULL, 1652, ' FILTRO ETNA', 10, 1, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-16 21:53:36', '2018-07-16 21:53:36'),
(101, 31, 2, 0, NULL, 1652, ' hhj77', 10, 1, '2.00', NULL, '60.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-17 04:49:29', '2018-07-17 04:49:29'),
(102, 31, 2, 0, NULL, 1652, ' jjj88k gfgfgfgf fggfggfg', 10, 1, '5.00', NULL, '20.00', '0.00', 'UNDELIVERED', '2018-07-17', 'Manual OT', NULL, 1, 1, 1, '2018-07-17 04:49:46', '2018-07-17 04:49:46'),
(103, 31, 12, 0, NULL, 1652, ' 2222 hghgh6 gghghghghghg', 10, 1, '9.00', NULL, '5.00', '0.00', 'UNDELIVERED', '2018-07-18', 'Manual', NULL, 0, 1, 1, '2018-07-18 11:16:08', '2018-07-18 11:16:08'),
(104, 31, 12, 0, NULL, 1652, '1111 fdfdfd gfgf fgf', 10, 1, '9.00', NULL, '5.00', '0.00', 'UNDELIVERED', '2018-07-18', 'Manual', NULL, 0, 1, 1, '2018-07-18 11:15:56', '2018-07-18 11:15:56'),
(105, 31, 12, 0, NULL, 1652, ' ffff ggfg66 ', 10, 1, '6.00', NULL, '6.00', '0.00', 'UNDELIVERED', '2018-07-18', 'Manual', NULL, 0, 1, 1, '2018-07-18 11:15:47', '2018-07-18 11:15:47'),
(127, 55, 1, 0, NULL, 1652, 'rei 1', 10, 1, '1.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2018-07-23', 'Quotation', 63, 1, 1, 1, '2018-07-23 05:07:57', '2018-07-23 05:07:57'),
(123, 49, 1, 0, NULL, 1652, 'xxx', 10, 1, '1.00', NULL, '20.00', '0.00', 'UNDELIVERED', '2018-07-21', 'Quotation', 60, 1, 1, 1, '2018-07-21 05:23:49', '2018-07-21 05:23:49'),
(124, 50, 1, 0, NULL, 1652, 'rrrr', 10, 1, '1.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-07-22', 'Quotation', 62, 1, 1, 1, '2018-07-22 07:45:23', '2018-07-22 07:45:23'),
(125, 51, 1, 0, NULL, 1652, 'rrr', 10, 1, '1.00', NULL, '50.00', '0.00', 'UNDELIVERED', '2018-07-22', 'Quotation', 61, 1, 1, 1, '2018-07-22 21:43:58', '2018-07-22 21:43:58'),
(126, 54, 1, 0, NULL, 1652, 'aaa', 10, 1, '1.00', NULL, '110.00', '0.00', 'UNDELIVERED', '2018-07-23', 'Quotation', 59, 1, 1, 1, '2018-07-23 04:57:31', '2018-07-23 04:57:31'),
(111, 30, 14, 0, NULL, 1652, 'v4 xxd', 10, 1, '1.00', NULL, '70.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:51:39', '2019-03-23 09:51:39'),
(112, 30, 14, 0, NULL, 1652, ' fff7', 10, 1, '2.00', NULL, '65.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:52:01', '2019-03-23 09:52:01'),
(113, 30, 14, 0, NULL, 1652, ' c5', 10, 1, '3.00', NULL, '100.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:52:07', '2019-03-23 09:52:07'),
(114, 30, 14, 0, NULL, 1652, ' f6', 10, 1, '4.00', NULL, '80.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:52:04', '2019-03-23 09:52:04'),
(115, 30, 14, 0, NULL, 1652, ' e', 10, 1, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:52:14', '2019-03-23 09:52:14'),
(116, 30, 14, 0, NULL, 1652, ' fffffff', 10, 1, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:53:18', '2019-03-23 09:53:18'),
(117, 30, 14, 0, NULL, 1652, ' tttttt', 10, 1, '3.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:53:09', '2019-03-23 09:53:09'),
(118, 30, 14, 0, NULL, 1652, ' 99o', 10, 1, '1.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:52:45', '2019-03-23 09:52:45'),
(119, 30, 14, 0, NULL, 1652, ' mnul', 10, 1, '1.00', NULL, '5.00', '0.00', 'UNDELIVERED', '2019-03-23', 'Manual', NULL, 0, 1, 1, '2019-03-23 09:51:57', '2019-03-23 09:51:57'),
(128, 56, 1, 0, NULL, 1652, 'aaarrrr', 10, 1, '1.00', NULL, '40.00', '0.00', 'UNDELIVERED', '2018-07-23', 'Quotation', 64, 1, 1, 1, '2018-07-23 05:16:43', '2018-07-23 05:16:43'),
(129, 57, 1, 0, NULL, 1652, 'EEEEE', 10, 1, '1.00', NULL, '55.00', '0.00', 'UNDELIVERED', '2018-07-23', 'Quotation', 65, 1, 1, 1, '2018-07-23 05:20:30', '2018-07-23 05:20:30'),
(130, 58, 1, 0, NULL, 1652, 'rrrr', 10, 1, '1.00', NULL, '40.00', '0.00', 'UNDELIVERED', '2018-07-23', 'Quotation', 66, 1, 1, 1, '2018-07-23 05:25:46', '2018-07-23 05:25:46'),
(131, 59, 1, 0, NULL, 1652, 'rrrrr', 10, 1, '1.00', NULL, '10.00', '0.00', 'UNDELIVERED', '2018-07-23', 'Quotation', 67, 1, 1, 1, '2018-07-23 05:28:07', '2018-07-23 05:28:07'),
(132, 60, 1, 0, NULL, 798, 'PASTILLAS DE FRENO DEL.', 10, 0, '1.00', NULL, '200.00', '1.00', 'FULLY', '2018-07-28', 'Quotation', 283, 1, 1, 1, '2018-07-28 09:09:29', '2018-07-28 09:05:47'),
(133, 39, 15, 0, NULL, 999, 'LIQUIDO DE FRENO BOSCH DOT 4 500ML', 10, 0, '1.00', NULL, '40.00', '0.00', 'UNDELIVERED', '2018-10-19', 'Manual', NULL, 1, 1, 1, '2018-10-19 22:42:52', '2018-10-19 22:42:52'),
(134, 31, 2, 0, NULL, 1652, 'ORUEBA ITEM MANUAL', 10, 1, '2.00', NULL, '15.00', '0.00', 'UNDELIVERED', '2018-12-22', 'Manual OT', NULL, 1, 1, 1, '2018-12-22 00:07:14', '2018-12-22 00:07:14'),
(135, 31, 2, 0, NULL, 1652, 'ITEM MANUAL 21122018', 10, 1, '2.00', NULL, '200.00', '0.00', 'UNDELIVERED', '2018-12-22', 'Manual OT', NULL, 1, 1, 1, '2018-12-22 00:20:05', '2018-12-22 00:20:05'),
(136, 29, 3, 0, NULL, 1652, 'manual', 10, 1, '1.00', NULL, '4.00', '0.00', 'UNDELIVERED', '2019-03-22', 'Manual', NULL, 0, 1, 1, '2019-03-22 10:23:25', '2019-03-22 10:23:25'),
(137, 61, 1, 0, NULL, 73, 'FILTRO DE AIRE ALT.', 10, 0, '3.00', NULL, '75.00', '0.00', 'UNDELIVERED', '2019-09-26', 'Quotation', 162911, 1, 1, 1, '2019-09-26 06:19:38', '2019-09-26 06:19:38'),
(138, 61, 2, 0, NULL, 1652, 'Bujia 45v', 10, 1, '1.00', NULL, '20.00', '0.00', 'UNDELIVERED', '2019-09-26', 'Quotation', 80, 1, 1, 1, '2019-09-26 06:19:38', '2019-09-26 06:19:38'),
(139, 61, 3, 0, NULL, 1652, 'Radio Ysb', 10, 1, '1.00', NULL, '150.00', '0.00', 'UNDELIVERED', '2019-09-26', 'Quotation', 81, 1, 1, 1, '2019-09-26 06:19:38', '2019-09-26 06:19:38'),
(140, 62, 1, 0, NULL, 465, 'FILTRO DE AIRE ALT.', 10, 0, '20.00', NULL, '1.00', '0.00', 'UNDELIVERED', '2019-10-02', 'Quotation', 10, 1, 1, 1, '2019-10-02 00:26:54', '2019-10-02 00:26:54'),
(141, 66, 1, 1, NULL, 1652, 'perno', 10, 1, '1.00', NULL, '20.00', '0.00', 'UNDELIVERED', '2020-05-08', 'Quotation', 117, 1, 1, 1, '2020-05-08 11:30:55', '2020-05-08 11:30:55'),
(142, 66, 2, 2, NULL, 1652, 'tuerca', 10, 1, '1.00', NULL, '45.00', '0.00', 'UNDELIVERED', '2020-05-08', 'Quotation', 118, 1, 1, 1, '2020-05-08 11:30:55', '2020-05-08 11:30:55'),
(143, 66, 3, 3, NULL, 1652, 'rodaje', 10, 1, '1.00', NULL, '25.00', '0.00', 'UNDELIVERED', '2020-05-08', 'Quotation', 119, 1, 1, 1, '2020-05-08 11:30:55', '2020-05-08 11:30:55');

-- --------------------------------------------------------

--
-- Table structure for table `eam_work_orders`
--

DROP TABLE IF EXISTS `eam_work_orders`;
CREATE TABLE IF NOT EXISTS `eam_work_orders` (
  `wip_entity_id` int(11) NOT NULL AUTO_INCREMENT,
  `wip_entity_name` varchar(20) NOT NULL,
  `status_type` varchar(3) NOT NULL,
  `parent_wip_entity_name` varchar(20) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `object_id` int(11) NOT NULL,
  `scheduled_start_date` date DEFAULT NULL,
  `scheduled_completion_date` date DEFAULT NULL,
  `activity_type` int(11) DEFAULT NULL,
  `actual_meter` decimal(11,5) DEFAULT 0.00000,
  `activity_source` int(11) DEFAULT NULL,
  `activity_cause` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `work_order_type` int(11) DEFAULT NULL,
  `requested_start_date` date DEFAULT NULL,
  `requested_due_date` date DEFAULT NULL,
  `applied_invoice_elect` varchar(1) DEFAULT 'N' COMMENT 'Y= se creo factura electronica',
  `estimation_status` int(11) DEFAULT NULL,
  `notification_required` varchar(1) DEFAULT NULL,
  `shutdown_type` int(11) DEFAULT NULL,
  `finished_date` datetime DEFAULT NULL,
  `firm_planned_flag` int(11) DEFAULT NULL,
  `parent_wip_entity_id` int(11) DEFAULT NULL,
  `failure_id` int(11) DEFAULT NULL,
  `cause_id` int(11) DEFAULT NULL,
  `solution` varchar(100) DEFAULT NULL,
  `failure_date` date DEFAULT NULL,
  `ending_date` datetime DEFAULT NULL,
  `ending_by` int(11) DEFAULT NULL,
  `comments` varchar(50) DEFAULT NULL,
  `retirement` tinyint(4) NOT NULL DEFAULT 0,
  `retirement_date` datetime DEFAULT NULL,
  `invoiced_flag` varchar(1) DEFAULT 'N',
  `retirement_by` int(11) DEFAULT NULL,
  `retirement_comments` varchar(150) DEFAULT NULL,
  `maintenance_object_type` int(11) DEFAULT NULL,
  `paying_date` datetime DEFAULT NULL,
  `invoice_number` varchar(50) DEFAULT NULL,
  `source` varchar(25) NOT NULL,
  `rebuild_source` varchar(25) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`wip_entity_id`),
  UNIQUE KEY `unq_ot` (`wip_entity_name`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eam_work_orders`
--

INSERT INTO `eam_work_orders` (`wip_entity_id`, `wip_entity_name`, `status_type`, `parent_wip_entity_name`, `site_id`, `owner_id`, `assignment_id`, `description`, `object_id`, `scheduled_start_date`, `scheduled_completion_date`, `activity_type`, `actual_meter`, `activity_source`, `activity_cause`, `priority`, `work_order_type`, `requested_start_date`, `requested_due_date`, `applied_invoice_elect`, `estimation_status`, `notification_required`, `shutdown_type`, `finished_date`, `firm_planned_flag`, `parent_wip_entity_id`, `failure_id`, `cause_id`, `solution`, `failure_date`, `ending_date`, `ending_by`, `comments`, `retirement`, `retirement_date`, `invoiced_flag`, `retirement_by`, `retirement_comments`, `maintenance_object_type`, `paying_date`, `invoice_number`, `source`, `rebuild_source`, `created_by`, `last_updated_by`, `updated_at`, `created_at`) VALUES
(26, 'OT-2540', 'LIQ', NULL, 4, 5, 22, 'des 1', 107, '2018-01-11', '2018-07-07', NULL, '5000.00000', NULL, NULL, 0, 28, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-05 03:20:19', NULL, NULL, 21, 49, '', '2018-03-29', NULL, NULL, '', 1, '2019-02-21 03:48:16', 'N', 1, 'salida de ot', NULL, '2018-07-28 09:02:16', '1233', '', '', 1, 1, '2019-02-21 03:48:16', '2018-01-10 18:01:45'),
(27, '56', 'LIQ', '1', 4, 18, 22, ' ', 26, '2018-03-16', '2018-03-31', NULL, '234.00000', NULL, NULL, NULL, 26, NULL, NULL, 'N', NULL, NULL, NULL, '2018-06-07 06:04:55', NULL, NULL, 22, 51, '', '2018-03-31', NULL, NULL, 'SSD', 1, NULL, 'N', 1, 'retiro el dia de  jp', NULL, '2018-06-12 05:53:13', 'GGG', 'Quotation', '', 1, 1, '2018-06-15 05:27:45', '2018-01-10 19:33:08'),
(28, '98', 'LIQ', '22', 4, 14, 12, ' ', 107, '2018-01-12', '2018-02-02', NULL, '20000.00000', NULL, NULL, 23, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2018-06-14 05:00:14', NULL, NULL, 21, 52, '', '2018-01-11', NULL, NULL, 'comnnnnnnn', 0, NULL, 'N', NULL, NULL, NULL, '2018-07-31 05:20:39', '001-12348', 'Quotation', '', 1, 1, '2018-06-14 05:00:14', '2018-02-11 04:43:44'),
(29, '45', 'FIN', '14', 4, 20, 21, ' rettt', 26, '2018-03-23', '2018-07-04', NULL, '14000.00000', NULL, NULL, 23, 26, NULL, NULL, 'N', NULL, NULL, NULL, '2019-03-23 09:38:10', NULL, NULL, 21, 50, '', '2018-03-31', NULL, NULL, 'ddd', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2019-03-23 09:38:10', '2018-03-11 04:55:33'),
(30, '9', 'FIN', '10', 4, 19, 22, ' terct', 139, '2018-03-23', '2018-07-02', NULL, '14000.00000', NULL, NULL, 24, 26, NULL, NULL, 'N', NULL, NULL, NULL, '2019-03-23 10:10:29', NULL, 10, 22, 51, '', '2019-03-23', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2019-03-23 10:10:29', '2018-02-15 08:01:45'),
(31, '12', 'IP', ' 23 ', 4, 30, 20, ' FDFDFDF', 170, '2018-01-01', '2018-01-30', NULL, '1.00000', NULL, NULL, 23, 26, NULL, NULL, 'N', NULL, NULL, NULL, '2018-06-14 06:59:10', NULL, 23, 21, 50, 'AAAAA', '2018-01-28', NULL, NULL, '', 0, NULL, 'N', 1, 'aaaa', NULL, NULL, NULL, 'Quotation', '', 1, 1, '2018-06-14 06:59:10', '2018-05-15 08:53:26'),
(32, '568', 'LIQ', ' 9 ', 4, 14, 12, ' ', 100, '2018-01-03', '2018-07-07', NULL, '10000.00000', NULL, NULL, 23, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-05 02:46:02', NULL, 9, 21, 0, '', '2018-07-05', NULL, NULL, '', 1, '2019-02-21 03:45:26', 'N', 1, 'ot 568 registro de salisalida\r\n\r\nddddd', NULL, '2018-12-22 15:00:33', '1174', 'Quotation', '', 1, 1, '2019-02-21 03:45:26', '2018-03-15 13:46:03'),
(33, '98705', 'IP', ' 3 ', 4, 18, 21, ' ', 80, '2018-01-10', '2018-01-17', NULL, '15000.00000', NULL, NULL, 25, 26, NULL, NULL, 'N', NULL, NULL, NULL, '2018-06-14 05:01:21', NULL, 3, 21, 0, '', '2018-06-14', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, '2018-07-04 05:01:43', '222', 'Quotation', '', 1, 1, '2020-05-27 17:16:58', '2018-01-15 13:57:09'),
(34, '12450', 'FIN', ' 12 ', 4, 11, 20, ' vvvvvvvvvv', 142, '2018-01-10', '2018-02-01', NULL, '15000.00000', NULL, NULL, 23, 26, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-02 22:51:56', NULL, 12, 21, 52, '', '2018-01-28', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2018-07-02 22:51:56', '2018-05-15 15:05:01'),
(35, '914', 'IP', ' 3 ', 4, 14, 18, ' ', 80, '2018-01-03', '2018-07-05', NULL, '0.00000', NULL, NULL, 23, 26, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 3, 21, 0, '', '2020-03-23', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2020-03-23 21:41:44', '2018-05-15 15:06:19'),
(36, '9078', 'PAY', '1', 4, 18, 12, ' ', 98, '2018-01-25', '2018-01-25', NULL, NULL, NULL, NULL, 23, 26, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-02-21 03:52:59', 'N', 1, 'ggggggggg', NULL, '2018-07-02 05:57:01', '22222', 'Quotation', NULL, 1, 1, '2020-05-27 17:46:32', '2018-04-15 16:14:48'),
(37, '1540a', 'PAY', ' 1 ', 4, 18, 12, ' vvv', 98, '2018-01-10', '2018-07-17', NULL, '10000.00000', NULL, NULL, 23, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-07 22:48:00', NULL, 1, 21, 50, '', '2018-02-06', NULL, NULL, '', 0, NULL, 'Y', NULL, NULL, NULL, '2018-10-18 05:49:11', '22344', 'Quotation', '', 1, 1, '2020-06-03 10:03:17', '2018-02-17 00:09:03'),
(38, '2', 'CAN', ' 31 ', 4, 19, 14, ' ', 107, '2018-04-02', '2018-07-02', NULL, '782.00000', NULL, NULL, 23, 26, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 31, 21, 52, 'prueba de freno', '2018-05-26', NULL, NULL, 'aaaa', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2018-07-16 10:04:29', '2018-04-01 19:24:25'),
(39, '1001', 'IP', ' 32 ', 4, 27, 22, ' ', 107, '2018-05-22', '2018-07-05', NULL, '10000.00000', NULL, NULL, 24, 28, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-15 00:25:58', NULL, 32, 22, 52, 'solucion para ot', '2018-05-25', NULL, NULL, 'cracion OT', 1, NULL, 'N', 1, 'salid vlus dsdsd sd dsdsdd', NULL, '2018-06-14 04:46:38', '001-22222', 'Quotation', 'ccccc', 1, 1, '2018-07-15 00:25:58', '2018-05-25 06:31:05'),
(40, '1D', 'IP', ' 7 ', 4, 11, 22, ' ', 107, '2018-01-11', '2018-07-01', NULL, '0.00000', NULL, NULL, 25, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-06-22 04:45:12', '2018-06-22 04:45:12'),
(41, '124', 'FIN', ' 51 ', 4, 11, 22, ' ', 142, '2018-07-09', '2018-07-13', NULL, '15001.00000', NULL, NULL, 25, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-02 23:09:38', NULL, 51, 21, 0, '', '2018-07-02', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2018-07-02 23:09:38', '2018-07-02 23:02:11'),
(42, 'A123', 'CAN', ' 52 ', 4, 12, 25, ' ', 142, '2018-07-09', '2018-07-12', NULL, '0.00000', NULL, NULL, 24, 27, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 52, 21, 0, '', '2018-07-16', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2018-07-16 10:05:31', '2018-07-07 06:46:39'),
(43, '12345B', 'IP', ' 33 ', 4, 19, 22, ' ', 169, '2018-07-07', '2018-07-11', NULL, '5000.00000', NULL, NULL, 24, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-07 22:59:05', NULL, 33, 22, 0, '', '2018-07-07', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2018-07-17 10:00:39', '2018-07-07 22:54:03'),
(44, '1234U', 'PAY', ' 47 ', 4, 9, 12, ' vvv', 100, '2018-07-07', '2018-07-10', NULL, '12000.00000', NULL, NULL, 24, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-07 23:35:59', NULL, 47, 21, 50, '', '2018-07-07', NULL, NULL, 'kkkkkk', 1, '2019-02-21 03:25:37', 'N', 1, 'servicio pagado por completo', NULL, '2018-10-17 06:40:59', '3rrr', 'Quotation', '', 1, 1, '2020-05-27 17:43:14', '2018-07-07 23:07:00'),
(45, '7777', 'CAN', ' 66 ', 4, 33, 18, ' ', 142, '2018-07-16', '2018-07-20', NULL, '25000.00000', NULL, NULL, 0, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-16 09:05:59', NULL, 66, 21, 51, '', '2018-07-16', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2018-07-16 10:01:43', '2018-07-16 05:17:13'),
(46, '666666', 'CAN', ' 65 ', 4, 9, 22, ' ', 142, '2018-07-10', '2018-07-17', NULL, '0.00000', NULL, NULL, 24, 27, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 65, 22, 0, '', '2018-07-16', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2018-07-16 10:26:40', '2018-07-16 10:26:06'),
(47, '257b', 'FIN', ' 67 ', 4, 11, 18, ' destr', 142, '2018-07-19', '2018-07-19', NULL, '31000.00000', NULL, NULL, 0, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2019-03-23 09:02:50', NULL, 67, 22, 0, '', '2019-03-23', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2019-03-23 09:02:50', '2018-07-18 18:22:00'),
(48, 'S1111', 'IP', ' 68 ', 4, 9, 22, ' ', 169, '2018-07-18', '2018-07-20', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 68, NULL, NULL, NULL, NULL, NULL, NULL, 'ffff', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-18 18:29:48', '2018-07-18 18:29:48'),
(49, '222AD', 'IP', ' 71 ', 4, 19, 22, ' ', 59, '2018-07-20', '2018-07-23', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 71, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-21 05:23:49', '2018-07-21 05:23:49'),
(50, 'ART-987', 'IP', ' 74 ', 7, 19, 22, ' ', 142, '2018-07-20', '2018-07-24', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 74, NULL, NULL, NULL, NULL, NULL, NULL, 'ccxxxxxxx', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-22 07:45:23', '2018-07-22 07:45:23'),
(51, 'OT-11', 'IP', ' 73 ', 8, 9, 18, ' ', 105, '2018-07-20', '2018-07-23', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 73, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-22 21:43:58', '2018-07-22 21:43:58'),
(52, 'OT-10', 'IP', ' 72 ', 4, 19, 18, ' ', 105, '2018-07-20', '2018-07-27', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 72, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-22 22:02:24', '2018-07-22 22:02:24'),
(53, 'OT-14', 'IP', ' 70 ', 4, 21, 18, ' ', 59, '2018-07-21', '2018-07-24', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 70, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-22 22:10:32', '2018-07-22 22:10:32'),
(54, 'OR-5', 'IP', ' 69 ', 4, 11, 18, ' ', 105, '2018-07-21', '2018-07-25', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 69, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-23 04:57:31', '2018-07-23 04:57:31'),
(55, 'OT-4', 'IP', ' 75 ', 9, 19, 18, ' ', 105, '2018-07-23', '2018-07-25', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 75, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-23 05:07:57', '2018-07-23 05:07:57'),
(56, 'OT-6', 'IP', ' 76 ', 9, 19, 18, ' ', 105, '2018-07-22', '2018-07-25', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 76, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-23 05:16:43', '2018-07-23 05:16:43'),
(57, 'OT-7', 'IP', ' 77 ', 9, 19, 18, ' ', 59, '2018-07-23', '2018-07-24', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 77, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-23 05:20:30', '2018-07-23 05:20:30'),
(58, 'AR-5', 'IP', ' 78 ', 9, 19, 18, ' ', 105, '2018-07-23', '2018-07-24', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 78, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-23 05:25:46', '2018-07-23 05:25:46'),
(59, 'AR-7', 'IP', ' 79 ', 9, 19, 18, ' ', 105, '2018-07-23', '2018-07-25', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 79, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2018-07-23 05:28:07', '2018-07-23 05:28:07'),
(60, 'OT-5040', 'PAY', '74', 7, 21, 18, ' ', 86, '2018-07-22', '2018-07-26', NULL, '15000.00000', NULL, NULL, 25, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2018-07-28 09:11:43', NULL, 81, 22, 50, '', '2018-07-28', NULL, NULL, '', 1, '2019-02-21 03:50:20', 'N', 1, 'aaaaaaa', NULL, '2018-10-20 19:36:08', '0005-55444', 'Quotation', '', 1, 1, '2020-05-27 17:47:12', '2018-07-28 09:05:47'),
(61, 'OT-20190925', 'IP', ' 22631 ', 7, 8, 21, ' ', 735, '2019-09-25', '2019-09-30', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 22631, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2019-09-26 06:19:38', '2019-09-26 06:19:38'),
(62, 'ot-20191002', 'IP', ' 5 ', 4, 13, 21, ' ', 63, '2019-10-02', '2019-10-05', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2019-10-02 00:26:54', '2019-10-02 00:26:54'),
(63, 'OT-DEMO15', 'FIN', ' 22632 ', 9, 20, 21, ' ', 105, '2019-10-14', '2019-10-17', NULL, '0.00000', NULL, NULL, 0, 27, NULL, NULL, 'N', NULL, NULL, NULL, '2019-10-12 00:21:50', NULL, 22632, 21, 0, '', '2019-10-12', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2019-10-12 00:21:50', '2019-10-12 00:11:22'),
(64, 'OT-DEMO20', 'FIN', ' 22633 ', 9, 20, 21, ' ', 105, '2019-10-15', '2019-10-26', NULL, '0.00000', NULL, NULL, 24, 28, NULL, NULL, 'N', NULL, NULL, NULL, '2019-10-12 01:18:08', NULL, 22633, 22, 0, '', '2019-10-12', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2019-10-12 01:18:08', '2019-10-12 01:17:14'),
(65, 'OT-12102019', 'FIN', ' 22634 ', 9, 20, 21, ' ', 107, '2019-10-15', '2019-10-17', NULL, '50000.00000', NULL, NULL, 0, 26, NULL, NULL, 'N', NULL, NULL, NULL, '2019-10-12 06:53:06', NULL, 22634, 21, 0, '', '2019-10-12', NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', '', 1, 1, '2019-10-12 06:53:06', '2019-10-12 06:48:47'),
(66, '20200608', 'IP', ' 22663 ', 9, 0, 21, ' ', 940, '2020-05-08', '2020-05-15', NULL, '0.00000', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 22663, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, 'N', NULL, NULL, NULL, NULL, NULL, 'Quotation', NULL, 1, 1, '2020-05-08 11:30:55', '2020-05-08 11:30:55');

-- --------------------------------------------------------

--
-- Table structure for table `eam_work_resource`
--

DROP TABLE IF EXISTS `eam_work_resource`;
CREATE TABLE IF NOT EXISTS `eam_work_resource` (
  `eam_resource_id` int(11) NOT NULL AUTO_INCREMENT,
  `wip_entity_id` int(11) NOT NULL,
  `wip_line_id` int(11) NOT NULL,
  `orden` int(11) NOT NULL DEFAULT 0,
  `operation` int(11) NOT NULL,
  `labor_id` int(11) NOT NULL,
  `is_manual` tinyint(1) NOT NULL DEFAULT 0,
  `source_line_id` int(11) DEFAULT NULL,
  `resource_name` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `comments` varchar(50) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 1,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`eam_resource_id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eam_work_resource`
--

INSERT INTO `eam_work_resource` (`eam_resource_id`, `wip_entity_id`, `wip_line_id`, `orden`, `operation`, `labor_id`, `is_manual`, `source_line_id`, `resource_name`, `price`, `comments`, `completed`, `enabled`, `created_at`, `created_by`, `updated_at`, `last_updated_by`) VALUES
(1, 27, 27, 0, 7, 16, 0, NULL, ' MANT. PREV. 10KM SEDAN CLASE B', '150.00', '', 0, 1, '2018-01-10 19:33:08', 1, '2018-01-10 19:33:08', 1),
(2, 32, 1, 0, 10, 15, 0, 22, 'MANTENIMIENTO PREVENTIVO 5KM SEDAN TIPO C', '90.00', NULL, 0, 1, '2018-01-15 13:46:03', 1, '2018-01-15 13:46:03', 1),
(3, 33, 1, 0, 10, 18, 0, 27, 'LAVADO MANTENIMIENTO SEDAN', '17.00', NULL, 0, 1, '2018-01-15 13:57:09', 1, '2018-01-15 13:57:09', 1),
(4, 35, 1, 0, 10, 18, 0, 27, 'LAVADO MANTENIMIENTO SEDAN', '17.00', NULL, 0, 1, '2018-01-15 15:06:19', 1, '2018-01-15 15:06:19', 1),
(5, 35, 2, 0, 10, 30, 1, 18, 'mano obre 1', '150.00', NULL, 0, 1, '2018-01-15 15:06:19', 1, '2018-01-15 15:06:19', 1),
(6, 35, 3, 0, 10, 30, 1, 19, 'man ob 2', '9.00', NULL, 0, 1, '2018-01-15 15:06:19', 1, '2018-01-15 15:06:19', 1),
(7, 36, 1, 0, 10, 8, 0, 26, 'MANT. PREV. 10KM SUV CLASE C', '150.00', NULL, 0, 1, '2018-01-15 16:14:48', 1, '2018-01-15 16:14:48', 1),
(8, 37, 1, 0, 10, 8, 0, 29, 'MANT. PREV. 10KM SUV CLASE C', '150.00', NULL, 0, 1, '2018-01-17 00:09:03', 1, '2018-01-17 00:09:03', 1),
(9, 37, 2, 0, 10, 30, 1, 20, 'MO MAN 1', '100.00', NULL, 0, 1, '2018-01-17 00:09:03', 1, '2018-01-17 00:09:03', 1),
(10, 37, 3, 0, 10, 30, 1, 21, 'MO MAN 20', '200.00', NULL, 0, 1, '2018-01-17 00:09:03', 1, '2018-01-17 00:09:03', 1),
(11, 26, 1, 0, 20, 31, 0, NULL, ' LABOR CHEVROLET', '152.00', 'dsdsdsd', 0, 1, '2018-01-31 00:06:38', 1, '2018-01-31 00:06:38', 1),
(12, 26, 2, 0, 20, 31, 0, NULL, ' LABOR CHEVROLET', '160.00', 'aadaaaa', 0, 1, '2018-01-31 00:09:56', 1, '2018-01-31 00:09:56', 1),
(13, 27, 4, 0, 10, 29, 0, NULL, ' SERVICIO DE PRUEBA', '20.00', 'LAVADO DE LLANTAS', 0, 0, '2018-01-31 00:15:24', 1, '2018-01-31 00:15:24', 1),
(14, 27, 3, 0, 10, 29, 0, NULL, ' SERVICIO DE PRUEBA', '15.80', 'pppppp', 0, 1, '2018-01-31 00:17:16', 1, '2018-01-31 00:17:16', 1),
(15, 35, 4, 0, 10, 32, 0, NULL, ' LABORES MANUALES', '45.00', 'LAVADO DEAUTO', 0, 1, '2018-01-31 00:24:36', 1, '2018-01-31 00:24:36', 1),
(16, 26, 3, 0, 10, 31, 0, NULL, ' LABOR CHEVROLET', '1400.00', 'PPP', 0, 1, '2018-01-31 00:53:37', 1, '2018-01-31 00:53:37', 1),
(17, 33, 2, 0, 10, 29, 0, NULL, ' SERVICIO DE PRUEBA', '25.00', 'YYY', 0, 1, '2018-01-31 00:57:01', 1, '2018-01-31 00:57:01', 1),
(18, 33, 3, 0, 10, 29, 0, NULL, ' SERVICIO DE PRUEBA', '25.00', 'SDSD', 0, 1, '2018-01-31 00:58:18', 1, '2018-01-31 00:58:18', 1),
(19, 27, 4, 0, 10, 33, 0, NULL, ' LAVADO SUBARU', '47.00', 'CAPOTE Y ASIENTOS', 0, 1, '2018-02-17 00:30:48', 1, '2018-02-17 00:30:48', 1),
(20, 38, 1, 0, 10, 11, 0, 82, 'MANTENIMIENTO EXPRESS', '90.00', NULL, 0, 1, '2018-04-01 19:24:25', 1, '2018-04-01 19:24:25', 1),
(21, 38, 2, 0, 10, 11, 0, 83, 'MANTENIMIENTO EXPRESS', '70.00', NULL, 0, 1, '2018-04-01 19:24:25', 1, '2018-04-01 19:24:25', 1),
(22, 38, 3, 0, 10, 15, 0, 84, 'MANTENIMIENTO PREVENTIVO 5KM SEDAN TIPO C', '160.00', NULL, 0, 1, '2018-04-01 19:24:25', 1, '2018-04-01 19:24:25', 1),
(23, 38, 4, 0, 10, 30, 1, 38, '11manual', '20.00', NULL, 0, 1, '2018-04-01 19:24:25', 1, '2018-04-01 19:24:25', 1),
(24, 38, 5, 0, 10, 30, 1, 37, '2manual', '40.00', NULL, 0, 1, '2018-04-01 19:24:25', 1, '2018-04-01 19:24:25', 1),
(25, 38, 6, 0, 10, 32, 0, NULL, ' LABORES MANUALES', '60.00', 'LAVADO', 0, 1, '2018-04-01 20:01:22', 1, '2018-04-01 20:01:22', 1),
(26, 39, 1, 0, 10, 27, 0, 85, 'CAMBIO DE DISCOS DE FRENO DELANTEROS', '30.00', NULL, 0, 1, '2018-05-25 06:31:05', 1, '2018-11-22 23:55:48', 1),
(27, 39, 2, 0, 10, 30, 1, 39, 'lllll', '20.00', NULL, 2, 1, '2018-05-25 06:31:05', 1, '2018-11-23 06:38:03', 1),
(28, 39, 3, 0, 10, 30, 1, 40, 'mmmm', '50.00', NULL, 0, 1, '2018-05-25 06:31:05', 1, '2018-05-25 06:31:05', 1),
(29, 39, 4, 0, 10, 11, 0, NULL, ' MANTENIMIENTO EXPRESS', '60.00', 'ULTIMA HORA', 0, 1, '2018-06-06 04:58:04', 1, '2018-06-06 04:58:04', 1),
(30, 39, 5, 0, 10, 31, 0, NULL, ' LABOR CHEVROLET', '200.00', 'labor de 06.06', 0, 1, '2018-06-06 06:32:29', 1, '2018-06-06 06:32:29', 1),
(31, 39, 6, 0, 10, 32, 0, NULL, ' LABORES MANUALES', '150.00', 'planchado', 0, 1, '2018-06-21 06:09:22', 1, '2018-06-21 06:09:22', 1),
(32, 45, 1, 0, 10, 22, 0, 58, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '120.00', NULL, 0, 1, '2018-07-16 05:17:13', 1, '2018-07-16 05:17:13', 1),
(33, 45, 2, 0, 10, 24, 0, 59, 'CAMBIO DE ROTULAS DEL. PICK UP', '140.00', NULL, 0, 1, '2018-07-16 05:17:13', 1, '2018-07-16 05:17:13', 1),
(34, 47, 1, 0, 10, 23, 0, 60, 'CAMBIO DE AMORTIGUADORES POS. PICK UP', '80.00', NULL, 1, 1, '2018-07-18 18:22:00', 1, '2018-07-18 18:22:00', 1),
(35, 47, 2, 0, 10, 29, 0, 61, 'SERVICIO DE PRUEBA', '15.00', NULL, 1, 1, '2018-07-18 18:22:00', 1, '2018-07-18 18:22:00', 1),
(36, 47, 3, 0, 10, 30, 1, 32, 'CAMBIO DE MUELLE', '200.00', NULL, 1, 1, '2018-07-18 18:22:00', 1, '2018-07-18 18:22:00', 1),
(37, 47, 4, 0, 10, 30, 1, 33, 'NBBBB', '300.00', NULL, 1, 1, '2018-07-18 18:22:00', 1, '2018-11-23 00:03:35', 1),
(38, 48, 1, 0, 10, 22, 0, 62, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '120.00', NULL, 0, 1, '2018-07-18 18:29:48', 1, '2018-07-18 18:29:48', 1),
(39, 48, 2, 0, 10, 23, 0, 63, 'CAMBIO DE AMORTIGUADORES POS. PICK UP', '80.00', NULL, 0, 1, '2018-07-18 18:29:48', 1, '2018-07-18 18:29:48', 1),
(40, 48, 3, 0, 10, 30, 1, 34, 'MANTENIMIENTO DE 10 KM ', '150.00', NULL, 0, 1, '2018-07-18 18:29:48', 1, '2018-07-18 18:29:48', 1),
(41, 49, 1, 0, 10, 30, 1, 36, 'ffff', '50.00', NULL, 0, 1, '2018-07-21 05:23:49', 1, '2018-11-22 05:59:15', 1),
(42, 49, 2, 0, 10, 30, 1, 37, 'hhhhh', '10.00', NULL, 0, 1, '2018-07-21 05:23:49', 1, '2018-11-22 05:28:12', 1),
(43, 50, 1, 0, 10, 22, 0, 70, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '120.00', NULL, 0, 1, '2018-07-22 07:45:23', 1, '2018-07-22 07:45:23', 1),
(44, 50, 2, 0, 10, 30, 1, 40, 'nnnnn', '80.00', NULL, 0, 1, '2018-07-22 07:45:23', 1, '2018-07-22 07:45:23', 1),
(45, 50, 3, 0, 10, 30, 1, 41, 'mmmmmbbbbb', '40.00', NULL, 0, 1, '2018-07-22 07:45:23', 1, '2018-07-22 07:45:23', 1),
(46, 51, 1, 0, 10, 22, 0, 68, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '120.00', NULL, 0, 1, '2018-07-22 21:43:58', 1, '2018-07-22 21:43:58', 1),
(47, 51, 2, 0, 10, 32, 0, 69, 'LABORES MANUALES', '10.00', NULL, 0, 1, '2018-07-22 21:43:58', 1, '2018-07-22 21:43:58', 1),
(48, 51, 3, 0, 10, 30, 1, 38, 'mmmm', '40.00', NULL, 0, 1, '2018-07-22 21:43:58', 1, '2018-07-22 21:43:58', 1),
(49, 51, 4, 0, 10, 30, 1, 39, 'nnnn', '50.00', NULL, 0, 1, '2018-07-22 21:43:58', 1, '2018-07-22 21:43:58', 1),
(50, 52, 1, 0, 10, 22, 0, 67, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '120.00', NULL, 0, 1, '2018-07-22 22:02:24', 1, '2018-07-22 22:02:24', 1),
(51, 53, 1, 0, 10, 2, 0, 66, 'MANT. PREV. 10KM PICK UP CLASE B', '240.00', NULL, 0, 1, '2018-07-22 22:10:32', 1, '2018-07-22 22:10:32', 1),
(52, 54, 1, 0, 10, 28, 0, 64, 'CAMBIO DE TAMBORES DE FRENO POST.', '60.00', NULL, 0, 1, '2018-07-23 04:57:31', 1, '2018-07-23 04:57:31', 1),
(53, 54, 2, 0, 10, 22, 0, 65, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '120.00', NULL, 0, 1, '2018-07-23 04:57:31', 1, '2018-07-23 04:57:31', 1),
(54, 54, 3, 0, 10, 30, 1, 35, 'jjjj', '40.00', NULL, 0, 1, '2018-07-23 04:57:31', 1, '2018-07-23 04:57:31', 1),
(55, 55, 1, 0, 10, 22, 0, 71, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '120.00', NULL, 0, 1, '2018-07-23 05:07:57', 1, '2018-07-23 05:07:57', 1),
(56, 55, 2, 0, 10, 30, 1, 42, 'mmmmm', '20.00', NULL, 0, 1, '2018-07-23 05:07:57', 1, '2018-07-23 05:07:57', 1),
(57, 55, 3, 0, 10, 30, 1, 43, 'nnnnnn', '10.00', NULL, 0, 1, '2018-07-23 05:07:57', 1, '2018-07-23 05:07:57', 1),
(58, 56, 1, 0, 10, 30, 1, 44, 'mmmmm', '50.00', NULL, 0, 1, '2018-07-23 05:16:43', 1, '2018-07-23 05:16:43', 1),
(59, 57, 1, 0, 10, 30, 1, 45, 'MMMM4', '30.00', NULL, 0, 1, '2018-07-23 05:20:30', 1, '2018-07-23 05:20:30', 1),
(60, 58, 1, 0, 10, 30, 1, 46, 'nnnnnn', '50.00', NULL, 0, 1, '2018-07-23 05:25:46', 1, '2018-07-23 05:25:46', 1),
(61, 59, 1, 0, 10, 22, 0, 72, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '120.00', NULL, 0, 1, '2018-07-23 05:28:07', 1, '2018-07-23 05:28:07', 1),
(62, 59, 2, 0, 10, 30, 1, 47, 'nnnn', '40.00', NULL, 0, 1, '2018-07-23 05:28:07', 1, '2018-07-23 05:28:07', 1),
(63, 60, 1, 0, 10, 6, 0, 78, 'MANTENIMIENTO PREVENTIVO 10KM SEDAN TIPO C', '140.00', NULL, 0, 1, '2018-07-28 09:05:47', 1, '2018-07-28 09:05:47', 1),
(64, 29, 1, 0, 10, 32, 0, NULL, ' LABORES MANUALES', '40.00', 'aire', 0, 0, '2019-03-22 09:50:27', 1, '2019-03-22 09:50:27', 1),
(65, 61, 1, 0, 10, 30, 1, 93, 'Lavado de motor', '200.00', NULL, 0, 1, '2019-09-26 06:19:38', 1, '2019-09-26 06:19:38', 1),
(66, 41, 1, 0, 10, 35, 0, 92, 'CAMBIO DE ZAPATAS SEDAN TIPO A', '220.00', NULL, 1, 1, '2019-10-05 23:57:34', 1, '2019-10-05 23:57:41', 1),
(67, 41, 2, 0, 10, 36, 0, 93, 'CAMBIO DE ZAPATAS SEDAN TIPO B', '150.00', NULL, 1, 1, '2019-10-05 23:58:58', 1, '2019-10-05 23:59:04', 1),
(68, 41, 4, 0, 10, 48, 0, 96, 'RECTIFICACION DE DISCO DE FRENO SEDAN TIPO A', '320.00', NULL, 1, 1, '2019-10-06 00:00:05', 1, '2019-10-06 00:00:11', 1),
(69, 29, 2, 0, 10, 60, 0, 92, 'RECTIFICACION DE DISCO DE FRENO CAMION ', '140.00', NULL, 1, 1, '2019-10-06 00:13:21', 1, '2019-10-06 00:13:29', 1),
(70, 29, 4, 0, 10, 51, 0, 98, 'RECTIFICACION DE DISCO DE FRENO FURGON ', '130.00', NULL, 1, 1, '2019-10-06 00:14:14', 1, '2019-10-06 00:14:18', 1),
(71, 29, 4, 0, 10, 71, 0, 95, 'CAMBIO DE CALIPERS FURGON', '90.00', NULL, 1, 1, '2019-10-06 00:15:35', 1, '2019-10-06 00:15:39', 1),
(72, 54, 3, 0, 10, 96, 0, 98, 'CAMBIO DE CALIPERS FURGON', '120.00', NULL, 1, 1, '2019-10-06 00:36:23', 1, '2019-10-06 00:36:27', 1),
(73, 54, 4, 0, 10, 97, 0, 79, 'CAMBIO DE PASTILLAS DE FRENO CAMION', '150.00', NULL, 1, 1, '2019-10-06 00:36:56', 1, '2019-10-06 00:36:59', 1),
(74, 26, 4, 0, 10, 70, 0, 98, 'CAMBIO DE CALIPERS PICK UP TIPO C', '140.00', NULL, 1, 1, '2019-10-06 00:42:49', 1, '2019-10-06 00:42:52', 1),
(75, 26, 5, 0, 10, 71, 0, 99, 'CAMBIO DE CALIPERS FURGON', '150.00', NULL, 1, 1, '2019-10-06 00:43:25', 1, '2019-10-06 00:43:28', 1),
(76, 26, 50, 0, 10, 75, 0, 98, 'CAMBIO DE DISCO DE FRENO SEDAN TIPO C', '120.00', NULL, 1, 1, '2019-10-06 00:48:56', 1, '2019-10-06 00:49:01', 1),
(77, 63, 1, 0, 10, 357, 0, 11890, 'CAMBIO DE ACEITE ', '90.00', NULL, 1, 1, '2019-10-12 00:11:22', 1, '2019-10-12 00:11:22', 1),
(78, 63, 2, 0, 10, 28, 0, 11891, 'CAMBIO DE TAMBORES DE FRENO POST.', '100.00', NULL, 1, 1, '2019-10-12 00:11:22', 1, '2019-10-12 00:11:22', 1),
(79, 63, 3, 0, 10, 22, 0, 11892, 'CAMBIO DE AMORTIGUADORES DEL. PICK UP', '150.00', NULL, 1, 1, '2019-10-12 00:11:22', 1, '2019-10-12 00:11:22', 1),
(80, 64, 1, 0, 10, 357, 0, 11893, 'CAMBIO DE ACEITE ', '50.00', NULL, 1, 1, '2019-10-12 01:17:14', 1, '2019-10-12 01:17:14', 1),
(81, 65, 1, 0, 10, 359, 0, 11894, 'CAMBIO DE ACEITE DE CAJA MECANICA', '120.00', NULL, 1, 1, '2019-10-12 06:48:47', 1, '2019-10-12 06:48:47', 1),
(82, 66, 1, 1, 10, 30, 1, 168, 'lavado', '50.00', NULL, 0, 1, '2020-05-08 11:30:55', 1, '2020-05-08 11:30:55', 1),
(83, 66, 2, 2, 10, 30, 1, 169, 'mantenimiento', '100.00', NULL, 0, 1, '2020-05-08 11:30:55', 1, '2020-05-08 11:30:55', 1),
(84, 66, 3, 3, 10, 30, 1, 170, 'desmontaje', '40.00', NULL, 0, 1, '2020-05-08 11:30:55', 1, '2020-05-08 11:30:55', 1),
(85, 66, 4, 4, 10, 30, 1, 171, 'armado', '60.00', NULL, 0, 1, '2020-05-08 11:30:55', 1, '2020-05-08 11:30:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fnd_lookup`
--

DROP TABLE IF EXISTS `fnd_lookup`;
CREATE TABLE IF NOT EXISTS `fnd_lookup` (
  `idlookup` int(11) NOT NULL AUTO_INCREMENT,
  `lookup_type` varchar(50) NOT NULL,
  `customization_level` varchar(1) NOT NULL,
  `description` varchar(100) NOT NULL,
  `active` varchar(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`idlookup`),
  UNIQUE KEY `lookup_type` (`lookup_type`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fnd_lookup`
--

INSERT INTO `fnd_lookup` (`idlookup`, `lookup_type`, `customization_level`, `description`, `active`, `created_at`, `updated_at`, `created_by`, `last_updated_by`) VALUES
(1, 'TIPO_DESPACHO_FUNERARIA', 'E', 'TIPO_DESPACHO_FUNERARIA', '1', '2017-03-25 07:13:28', '2017-03-25 07:13:28', 1, 0),
(2, 'TIPO_DOC_IDENTIFICACION', 'S', 'Tipos de documento de identidad', '1', '2017-03-25 07:18:26', '2017-03-25 07:18:26', 1, 0),
(3, 'TIPO_DOC_FACTURACION', 'S', 'use in Module AP', '1', '2017-03-25 21:43:56', '2020-04-11 02:13:23', 1, 0),
(4, 'MOVILIDAD_FUNERAL', 'E', 'OVILIDAD_FUNERAL', '1', '2017-03-25 21:44:20', '2017-03-25 21:44:20', 1, 0),
(5, 'TIPO_DOC_VENTA', 'E', 'documento venta', '1', '2017-03-31 23:39:43', '2017-03-31 23:39:43', 1, 0),
(8, 'MODELO_CAPILLA_FUNERAL', 'E', 'DFFFFF33', '0', '2017-04-13 17:01:56', '2017-04-13 17:01:56', 1, 0),
(9, 'Tipo_doc_Ingreso', 'E', 'usado para documentos de ingreso', '1', '2017-05-05 10:37:05', '2017-05-05 10:37:05', 1, 0),
(10, 'TIPO_LABOR', 'S', 'Tipo de Labor por AJM', '1', '2017-05-12 16:01:32', '2017-05-12 16:01:32', 1, 0),
(11, 'GASTOS_VARIOS', 'S', 'GASTOS_VARIOS', '1', '2017-05-18 12:02:24', '2017-05-18 12:02:24', 1, 0),
(12, 'FALLA_OT', 'E', 'FALLA OT', '1', '2018-01-05 16:34:20', '2018-01-05 16:34:20', 1, 0),
(13, 'PRIORIDAD_OT', 'E', 'PRIORIDAD_OT', '1', '2018-01-05 16:35:10', '2018-01-05 16:35:10', 1, 0),
(14, 'TIPO_OT', 'E', 'TIPO_OT', '1', '2018-01-05 16:36:15', '2018-01-05 16:36:15', 1, 0),
(15, 'Activity_Type_OT', 'E', 'Activity Type', '1', '2018-01-05 16:37:40', '2018-01-05 16:37:40', 1, 0),
(16, 'Activity_Cause_OT', 'E', 'Activity Cause', '1', '2018-01-05 16:46:38', '2018-01-05 16:46:38', 1, 0),
(17, 'Activity_Source_OT', 'E', 'Activity Source', '1', '2018-01-05 16:47:32', '2018-01-05 16:47:32', 1, 0),
(18, 'TIPO_OC', 'E', 'TIPO_OC', '1', '2018-01-05 16:50:05', '2018-01-05 16:50:05', 1, 0),
(19, 'ESTADO_OT', 'E', 'ESTADO_OT', '1', '2018-01-05 17:11:04', '2018-01-05 17:11:04', 1, 0),
(20, 'CAUSA_OT', 'E', 'Causa de OT', '1', '2018-01-08 06:02:27', '2018-01-08 06:02:27', 1, 0),
(21, 'MEDIO_PAGO_OT', 'S', 'Medio de Pago en Liquidacion', '1', '2018-03-26 23:18:51', '2018-03-26 23:18:51', 1, 0),
(22, 'OM_ CARD', 'E', 'Tipo de Tarjeta', '1', '2018-06-07 06:49:31', '2018-06-07 06:49:31', 1, 0),
(23, 'OM_SALES_CHANNEL', 'E', 'canal de venta', '1', '2018-06-07 06:50:25', '2018-06-07 06:50:25', 1, 0),
(24, 'OM_SHIP_METHOD', 'E', 'MEtodo de envio', '1', '2018-06-07 06:51:09', '2018-06-07 06:51:09', 1, 0),
(25, 'OM_PAY_TYPE', 'E', 'Tipo de Pago', '1', '2018-06-07 06:52:21', '2018-06-07 06:52:21', 1, 0),
(26, 'ORDER_TYPE_OM', 'E', 'Tipo de Pedido', '1', '2018-06-08 06:15:14', '2018-06-08 06:15:14', 1, 0),
(27, 'INV_TYPE_SITE', 'S', 'Tipo de Sucursal', '1', '2018-07-28 06:14:31', '2018-07-28 06:14:31', 1, 0),
(28, 'AS_TYPE_VEHICLE', 'S', 'Tipo de Vehiculos', '1', '2020-01-29 12:25:09', '2020-01-29 12:25:09', 1, 0),
(29, 'AS_USE_VEHICLE', 'E', 'Uso de Vehiculo', '1', '2020-01-29 12:31:12', '2020-01-29 12:31:12', 1, 0),
(30, 'EAM_TYPE_MAINTENANCE', 'S', 'Tipo de Mantenimiento Vehicular', '1', '2020-04-04 14:51:43', '2020-04-04 15:26:03', 1, 0),
(31, 'TYPE_EXPENSE', 'E', 'Tipo Gasto de Caja Chica', '1', '2020-04-11 16:02:53', '2020-04-11 16:02:53', 1, 0),
(32, 'TYPE_REVENUE', 'E', 'Tipos de ingresos a caja', '1', '2020-04-12 11:40:49', '2020-04-12 11:40:49', 1, 0),
(33, 'DOCUMENTOS_CONTRATO', 'E', 'Lista de Documentos a contratar', '1', '2020-04-17 08:43:10', '2020-04-17 08:43:10', 1, 0),
(34, 'TERMINO_PAGO', 'S', 'lista de termino de pago', '1', '2020-04-22 23:29:27', '2020-04-22 23:29:27', 1, 0),
(36, 'AAA', 'E', 'DDDD', '1', '2020-04-23 11:21:37', '2020-04-23 11:21:37', 1, 0),
(37, 'A1', 'E', 'AAA', '1', '2020-04-23 11:23:10', '2020-04-23 11:23:10', 1, 0),
(38, 'A2', 'E', 'DDDD', '1', '2020-04-23 11:24:09', '2020-04-23 11:24:09', 1, 0),
(39, 'LISTA_ASEGURADORAS', 'E', 'lista de aseguradoras', '1', '2020-05-02 11:05:34', '2020-05-02 11:05:34', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fnd_lookup_value`
--

DROP TABLE IF EXISTS `fnd_lookup_value`;
CREATE TABLE IF NOT EXISTS `fnd_lookup_value` (
  `idlvalue` int(11) NOT NULL AUTO_INCREMENT,
  `idlookup` int(11) NOT NULL,
  `code_value` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`idlvalue`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fnd_lookup_value`
--

INSERT INTO `fnd_lookup_value` (`idlvalue`, `idlookup`, `code_value`, `description`, `date_from`, `date_to`, `active`, `created_at`, `updated_at`, `created_by`, `last_updated_by`) VALUES
(1, 1, 'tipo 1', 'tipo 1', '2017-03-20', '2017-03-28', 'Y', '2017-03-25 07:13:28', '2017-03-25 07:13:28', 1, 0),
(2, 2, 'DNI', 'documento', '2017-03-01', '2017-03-15', 'Y', '2017-03-25 07:18:26', '2017-03-25 07:18:26', 1, 0),
(3, 2, 'LE', 'LIBRETA', '2017-03-22', '2017-03-29', 'Y', '2017-03-25 07:18:26', '2017-03-25 07:18:26', 1, 0),
(4, 2, 'CE', 'carnet', '2017-03-14', '2017-03-29', 'Y', '2017-03-25 07:18:26', '2017-03-25 07:18:26', 1, 0),
(5, 1, 'tipo 2', 'tipo 2', '2017-03-21', '2017-03-27', 'Y', '2017-03-25 21:44:20', '2017-03-25 21:44:20', 1, 0),
(6, 5, 'BO', 'BOLETA VENTA', '2017-03-09', '2017-11-16', '', '2017-03-31 23:39:43', '2017-03-31 23:39:43', 1, 0),
(7, 4, 'auto', 'auto', '2017-03-01', '2017-03-29', '', '2017-03-31 23:41:41', '2017-03-31 23:41:41', 1, 0),
(8, 4, 'minivan', 'minivan', '2017-04-04', '2017-04-18', '', '2017-04-01 06:27:11', '2017-04-01 06:27:11', 1, 0),
(9, 8, 'ccd', 'ccd', '2017-04-03', '2017-04-05', '', '2017-04-13 17:01:56', '2017-04-13 17:01:56', 1, 0),
(10, 4, 'limosina', 'limosina', '2017-04-11', '2017-04-18', '', '2017-04-13 17:01:56', '2017-04-13 17:01:56', 1, 0),
(11, 8, 'madera', 'madera', '2017-04-11', '2017-04-18', '', '2017-04-13 17:01:56', '2017-04-13 17:01:56', 1, 0),
(12, 8, 'lata', 'lata', '2017-04-04', '2017-04-06', '', '2017-04-13 17:06:12', '2017-04-13 17:06:12', 1, 0),
(13, 8, 'cedro', 'cedro', '2017-04-11', '2017-04-19', '', '2017-04-13 17:06:29', '2017-04-13 17:06:29', 1, 0),
(14, 9, 'OC', 'usado', '0000-00-00', '2017-09-06', '', '2017-05-05 10:37:05', '2017-05-05 10:37:05', 1, 0),
(15, 9, 'Bol', 'Boleta', '2017-08-05', '2017-03-08', '', '2017-05-05 10:37:05', '2017-05-05 10:37:05', 1, 0),
(16, 10, 'P', 'Labor', '2017-01-05', '0000-00-00', '', '2017-05-12 16:01:32', '2017-05-12 16:01:32', 1, 0),
(17, 10, 'T', 'Labor', '2017-09-05', '0000-00-00', '', '2017-05-12 16:01:32', '2017-05-12 16:01:32', 1, 0),
(18, 11, 'MOVILIDAD', 'gastos', '2017-01-05', '0000-00-00', '', '2017-05-18 12:02:24', '2017-05-18 12:02:24', 1, 0),
(19, 11, 'RESTAURANT', 'comida', '2017-01-05', '0000-00-00', '', '2017-05-18 12:02:24', '2017-05-18 12:02:24', 1, 0),
(20, 11, 'REFRIGERIO', 'gastos', '2017-01-05', '0000-00-00', '', '2017-05-18 12:02:24', '2017-05-18 12:02:24', 1, 0),
(21, 12, 'Mecanico', 'Mecanico', '2018-01-01', '0000-00-00', '', '2018-01-05 16:34:20', '2018-01-05 16:34:20', 1, 0),
(22, 12, 'Sistema', 'Sistema', '2018-01-01', '0000-00-00', '', '2018-01-05 16:34:20', '2018-01-05 16:34:20', 1, 0),
(23, 13, 'Alto', 'Alto', '2018-01-01', '2018-06-07', '', '2018-01-05 16:35:10', '2018-01-05 16:35:10', 1, 0),
(24, 13, 'Medio', 'Medio', '2018-01-01', '2018-06-07', '', '2018-01-05 16:35:10', '2018-01-05 16:35:10', 1, 0),
(25, 13, 'Bajo', 'Bajo', '2018-01-01', '2018-06-07', '', '2018-01-05 16:35:10', '2018-01-05 16:35:10', 1, 0),
(26, 14, 'Rutina', 'Rutina', '2018-01-01', '2018-05-10', '', '2018-01-05 16:36:15', '2018-01-05 16:36:15', 1, 0),
(27, 14, 'Preventiva', 'Preventiva', '2018-01-01', '2018-05-10', '', '2018-01-05 16:36:15', '2018-01-05 16:36:15', 1, 0),
(28, 14, 'Emergencia', 'Emergencia', '2018-01-01', '0000-00-00', '', '2018-01-05 16:36:15', '2018-01-05 16:36:15', 1, 0),
(29, 14, 'Modificacion', 'Modificacion', '2018-01-01', '0000-00-00', '', '2018-01-05 16:36:15', '2018-01-05 16:36:15', 1, 0),
(30, 15, 'Inspeccion', 'Inspeccion', '2018-01-01', '0000-00-00', '', '2018-01-05 16:37:40', '2018-01-05 16:37:40', 1, 0),
(31, 15, 'Revision', 'Revision', '2018-01-01', '0000-00-00', '', '2018-01-05 16:37:40', '2018-01-05 16:37:40', 1, 0),
(32, 15, 'Lubricacion', 'Lubricacion', '2018-01-01', '0000-00-00', '', '2018-01-05 16:37:40', '2018-01-05 16:37:40', 1, 0),
(33, 15, 'Reparacion', 'Reparacion', '2018-01-01', '0000-00-00', '', '2018-01-05 16:37:40', '2018-01-05 16:37:40', 1, 0),
(34, 15, 'Mantenimiento', 'Mantenimiento', '2018-01-01', '0000-00-00', '', '2018-01-05 16:37:40', '2018-01-05 16:37:40', 1, 0),
(35, 15, 'Limpieza', 'Limpieza', '2018-08-01', '0000-00-00', '', '2018-01-05 16:37:40', '2018-01-05 16:37:40', 1, 0),
(36, 16, 'Desglose', 'Desglose', '2018-08-01', '0000-00-00', '', '2018-01-05 16:46:38', '2018-01-05 16:46:38', 1, 0),
(37, 16, 'Vandalismo', 'Vandalismo', '2018-08-01', '0000-00-00', '', '2018-01-05 16:46:38', '2018-01-05 16:46:38', 1, 0),
(38, 16, 'Desgaste', 'Desgaste', '2018-08-01', '0000-00-00', '', '2018-01-05 16:46:38', '2018-01-05 16:46:38', 1, 0),
(39, 16, 'Ajustes', 'Ajustes', '2018-08-01', '0000-00-00', '', '2018-01-05 16:46:38', '2018-01-05 16:46:38', 1, 0),
(40, 17, 'Regulador', 'Regulador', '2018-01-01', '2018-07-09', '', '2018-01-05 16:47:32', '2018-01-05 16:47:32', 1, 0),
(41, 17, 'Cumplimiento', 'Cumplimiento', '2018-01-01', '2018-07-09', '', '2018-01-05 16:47:32', '2018-01-05 16:47:32', 1, 0),
(42, 17, 'Garantia', 'Garantia', '2018-01-01', '2018-07-09', '', '2018-01-05 16:47:32', '2018-01-05 16:47:32', 1, 0),
(43, 17, 'Accidente', 'Accidente', '2018-01-01', '0000-00-00', '', '2018-01-05 16:47:32', '2018-01-05 16:47:32', 1, 0),
(44, 18, 'STANDARD', 'STANDARD', '2018-01-01', '0000-00-00', '', '2018-01-05 16:50:05', '2018-01-05 16:50:05', 1, 0),
(60, 22, 'VISA', 'Visa', '2018-06-06', '2018-06-07', '', '2018-06-07 06:49:31', '2018-06-07 06:49:31', 1, 0),
(46, 19, 'IP', 'EN PROCESO', '0000-00-00', '0000-00-00', '', '2018-01-05 17:11:04', '2018-01-05 17:11:04', 1, 0),
(47, 19, 'FIN', 'FINALIZADO', '0000-00-00', '0000-00-00', '', '2018-01-05 17:11:04', '2018-01-05 17:11:04', 1, 0),
(61, 22, 'MASTERCARD', 'MasterCard', '2018-06-06', '0000-00-00', '', '2018-06-07 06:49:31', '2018-06-07 06:49:31', 1, 0),
(49, 20, 'AI', 'Averias Inesperada', '2018-01-01', '0000-00-00', '', '2018-01-08 06:02:27', '2018-01-08 06:02:27', 1, 0),
(50, 20, 'AT', 'Averia Total', '2018-01-01', '0000-00-00', '', '2018-01-08 06:02:27', '2018-01-08 06:02:27', 1, 0),
(51, 20, 'AC', 'Averia Cronica', '2018-01-01', '0000-00-00', '', '2018-01-08 06:02:27', '2018-01-08 06:02:27', 1, 0),
(52, 20, 'MD', 'Mant. Deficiente', '2018-01-01', '2018-07-02', '', '2018-01-08 06:02:27', '2018-01-08 06:02:27', 1, 0),
(53, 21, 'EFT', 'Electronico', '2018-01-03', '2019-04-01', '', '2018-03-26 23:18:51', '2018-03-26 23:18:51', 1, 0),
(54, 21, 'CHECK', 'Cheque', '2018-01-03', '0000-00-00', '', '2018-03-26 23:18:51', '2018-03-26 23:18:51', 1, 0),
(55, 21, 'CARD_CREDIT', 'Tarjeta Credito', '2018-01-03', '2019-08-02', '', '2018-03-26 23:18:51', '2018-03-26 23:18:51', 1, 0),
(56, 21, 'CARD_DEBIT', 'Tarjeta Debito', '2018-01-03', '2019-07-03', '', '2018-03-26 23:18:51', '2018-03-26 23:18:51', 1, 0),
(57, 21, 'DEPOSIT', 'Deposito Banco', '2018-01-03', '2019-03-04', '', '2018-03-26 23:18:51', '2018-03-26 23:18:51', 1, 0),
(58, 21, 'TRANSFER', 'Transferencia', '2018-02-05', '0000-00-00', '', '2018-05-24 03:29:35', '2018-05-24 03:29:35', 1, 0),
(59, 21, 'CONTADO', 'Contado', '0000-00-00', '2018-07-06', '', '2018-05-24 03:33:15', '2018-05-24 03:33:15', 1, 0),
(62, 23, 'INT', 'Internet', '2018-12-06', '2018-04-07', '', '2018-06-07 06:50:25', '2018-06-07 06:50:25', 1, 0),
(63, 23, 'VOL', 'Volante', '0000-00-00', '0000-00-00', '', '2018-06-07 06:50:25', '2018-06-07 06:50:25', 1, 0),
(64, 23, 'REC', 'Recomendacion', '0000-00-00', '0000-00-00', '', '2018-06-07 06:50:25', '2018-06-07 06:50:25', 1, 0),
(65, 24, 'TER', 'Terrestre', '2018-11-06', '2018-04-07', '', '2018-06-07 06:51:09', '2018-06-07 06:51:09', 1, 0),
(66, 24, 'AER', 'Aereo', '2018-11-06', '2018-01-08', '', '2018-06-07 06:51:09', '2018-06-07 06:51:09', 1, 0),
(67, 24, 'MAR', 'Maritimo', '2018-11-06', '2018-06-09', '', '2018-06-07 06:51:09', '2018-06-07 06:51:09', 1, 0),
(68, 25, 'CONT', 'Venta', '2018-05-06', '2018-04-07', '', '2018-06-07 06:52:21', '2018-06-07 06:52:21', 1, 0),
(69, 25, 'CRED', 'Venta', '0000-00-00', '2018-01-08', '', '2018-06-07 06:52:21', '2018-06-07 06:52:21', 1, 0),
(70, 26, 'SITE', 'Venta en Tienda', '2018-04-06', '0000-00-00', '', '2018-06-08 06:15:14', '2018-06-08 06:15:14', 1, 0),
(71, 26, 'PER', 'Venta Personal', '2018-04-06', '0000-00-00', '', '2018-06-08 06:15:14', '2018-06-08 06:15:14', 1, 0),
(72, 26, 'TEL', 'Venta por Telefono', '2018-04-06', '2018-04-07', '', '2018-06-08 06:15:14', '2018-06-08 06:15:14', 1, 0),
(73, 18, 'ANULADO', 'Anular OT', '2018-06-14', '2018-06-14', '1', '2018-06-14 04:42:48', '2018-06-14 04:42:51', 1, 1),
(74, 19, 'CAN', 'CANCELADO', '2018-07-05', '2018-07-05', '1', '2018-07-05 02:45:30', '2018-07-05 02:45:35', 1, 1),
(75, 27, 'MASTER', 'Principal', '2018-07-28', '2018-07-28', '1', '2018-07-28 06:14:31', '2018-07-28 06:14:31', 1, 0),
(76, 27, 'ORGANIZATION', 'Sucursal', '2018-07-28', '2018-07-28', '1', '2018-07-28 06:14:31', '2018-07-28 06:14:31', 1, 0),
(77, 28, 'VAN', 'Van', '2020-01-01', '0000-00-00', '', '2020-01-29 12:25:09', '2020-01-29 12:25:09', 1, 0),
(78, 28, 'MINIVAN', 'Minivan', '2020-01-01', '0000-00-00', '', '2020-01-29 12:25:09', '2020-01-29 12:25:09', 1, 0),
(79, 28, 'CAMIONETA', 'Caimoneta', '2020-01-01', '0000-00-00', '', '2020-01-29 12:25:09', '2020-01-29 12:25:09', 1, 0),
(80, 28, 'MOTO', 'Motocileta', '2020-01-01', '2029-08-02', '', '2020-01-29 12:25:09', '2020-01-29 12:25:09', 1, 0),
(81, 28, 'AUTO', 'Auto', '2020-01-01', '2029-01-03', '', '2020-01-29 12:25:09', '2020-01-29 12:25:09', 1, 0),
(82, 29, 'PART', 'Prticular', '2020-01-01', '2020-05-02', '', '2020-01-29 12:31:12', '2020-01-29 12:31:12', 1, 0),
(83, 29, 'TAXI', 'Taxi', '2020-01-01', '2020-04-03', '', '2020-01-29 12:31:12', '2020-01-29 12:31:12', 1, 0),
(84, 29, 'ALQ', 'Alquiler', '2020-01-01', '2020-01-04', '', '2020-01-29 12:31:12', '2020-01-29 12:31:12', 1, 0),
(85, 30, 'MANT.PREDICTIVO', 'Mantenimiento predictivo', '0000-00-00', '2020-10-04', '', '2020-04-04 14:51:43', '2020-04-04 14:51:43', 1, 0),
(86, 30, 'MANT.CORRECTIVO', 'Mantenimiento Correctivo', '2020-05-04', '0000-00-00', '', '2020-04-04 15:22:43', '2020-04-04 15:22:43', 1, 0),
(87, 30, 'MANT.PREVNTIVO', 'Mantenimiento Preventivo', '2020-01-04', '2020-06-05', '', '2020-04-04 15:23:55', '2020-04-04 15:23:55', 1, 0),
(88, 30, 'MANT.HORAS', 'Mantenimiento por horas', '2020-01-04', '2020-05-05', '', '2020-04-04 15:34:21', '2020-04-04 15:34:21', 1, 0),
(89, 3, 'Factura', 'Factura', '2020-01-04', '2020-03-10', '', '2020-04-11 02:09:50', '2020-04-11 02:09:50', 1, 0),
(90, 3, 'Boleta', 'Boleta de Venta ', '0000-00-00', '2020-06-05', '', '2020-04-11 02:12:47', '2020-04-11 02:12:47', 1, 0),
(91, 3, 'Ticket', 'Ticket', '0000-00-00', '2020-04-06', '', '2020-04-11 02:13:04', '2020-04-11 02:13:04', 1, 0),
(92, 3, 'Recibo', 'Recibo', '0000-00-00', '2020-09-07', '', '2020-04-11 02:13:19', '2020-04-11 02:13:19', 1, 0),
(93, 31, 'Gastos.Varios', 'Gastos movilidad', '2020-01-04', '2020-05-05', 'Y', '2020-04-11 16:02:53', '2020-04-11 16:02:53', 1, 0),
(94, 31, 'Movilidad', 'movilidad', '2020-01-04', '2020-03-06', 'Y', '2020-04-11 16:02:53', '2020-04-11 16:02:53', 1, 0),
(95, 31, 'Almuerzo', 'almuerzo', '2020-05-06', '2020-08-07', 'Y', '2020-04-11 16:02:53', '2020-04-11 16:02:53', 1, 0),
(96, 32, 'DEVOLUCION', 'Devolucion dinero', '0000-00-00', '2020-07-05', 'Y', '2020-04-12 11:40:49', '2020-04-12 11:40:49', 1, 0),
(97, 32, 'SERVICIO', 'servicio', '2020-05-04', '2020-02-06', 'Y', '2020-04-12 11:40:49', '2020-04-12 11:40:49', 1, 0),
(98, 30, 'SIN MANT.', 'Sin manttenimieto asignado', '2020-04-01', '2020-04-30', 'Y', '2020-04-15 14:02:18', '2020-04-15 14:02:18', 1, 0),
(99, 33, 'SOAT', 'SOAT', '2020-04-01', '2020-04-30', 'Y', '2020-04-17 08:43:10', '2020-04-17 08:43:10', 1, 0),
(100, 33, 'SCTR', 'SCTR', '2020-04-01', '2020-04-30', 'Y', '2020-04-17 08:43:10', '2020-04-17 08:43:10', 1, 0),
(101, 33, 'REV.TEC', 'Revisiones', '2020-04-01', '2020-04-30', 'Y', '2020-04-17 08:43:10', '2020-04-17 08:43:10', 1, 0),
(102, 34, '45 DIAS', 'pago 45 d', '2020-04-01', '2020-04-30', 'Y', '2020-04-22 23:29:27', '2020-04-22 23:29:27', 1, 0),
(103, 34, '60 DIAS', 'pago 60 d', '2020-04-01', '2020-04-29', 'Y', '2020-04-22 23:29:27', '2020-04-22 23:29:27', 1, 0),
(104, 39, 'RIMAC', 'RIMAC', '0000-00-00', '0000-00-00', 'Y', '2020-04-23 11:21:37', '2020-04-23 11:21:37', 1, 0),
(105, 39, 'PACICIFO', 'PACIFICO', '0000-00-00', '0000-00-00', 'Y', '2020-04-23 11:23:10', '2020-04-23 11:23:10', 1, 0),
(106, 39, 'MAPFRE', 'MAPFRE', '0000-00-00', '0000-00-00', 'Y', '2020-04-23 11:24:09', '2020-04-23 11:24:09', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ingreso`
--

DROP TABLE IF EXISTS `ingreso`;
CREATE TABLE IF NOT EXISTS `ingreso` (
  `idingreso` int(11) NOT NULL AUTO_INCREMENT,
  `idproveedor` int(11) NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `serie_comprobante` varchar(7) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `num_comprobante` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idingreso`),
  KEY `fk_ingreso_persona_idx` (`idproveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `ingreso`
--

INSERT INTO `ingreso` (`idingreso`, `idproveedor`, `tipo_comprobante`, `serie_comprobante`, `num_comprobante`, `fecha_hora`, `impuesto`, `estado`) VALUES
(3, 5, 'Boleta', '001', '001', '2016-08-19 22:19:34', '18.00', 'A'),
(4, 6, 'Boleta', '001', '0001', '2016-08-30 16:19:48', '18.00', 'A'),
(5, 5, 'Factura', '001', '00004', '2016-08-30 16:29:00', '18.00', 'A'),
(6, 5, 'Boleta', '001', '0001', '2016-08-31 11:02:43', '18.00', 'A'),
(7, 5, 'Boleta', '001', '0008', '2016-08-31 11:22:33', '18.00', 'A'),
(8, 5, 'Boleta', '001', '0001', '2016-08-31 11:37:20', '18.00', 'A'),
(9, 5, 'Boleta', '007', '0008', '2016-08-31 15:37:16', '18.00', 'A'),
(10, 6, 'Factura', '001', '001', '2016-09-01 16:33:03', '18.00', 'A'),
(11, 5, 'Boleta', '001', '0003', '2016-09-01 17:00:51', '18.00', 'A'),
(12, 5, 'Boleta', '001', '00077', '2016-09-27 15:19:58', '18.00', 'A'),
(13, 5, 'Boleta', '001', '00002', '2016-09-29 16:12:58', '18.00', 'A'),
(14, 5, 'Boleta', '001', '0002', '2016-10-02 13:42:22', '0.00', 'A'),
(15, 5, 'Boleta', '002', '0003', '2016-10-02 13:43:51', '18.00', 'A'),
(16, 5, 'Ticket', '001', '0005', '2016-10-02 13:44:13', '0.00', 'A'),
(17, 5, 'Boleta', '001', '000155', '2016-11-01 10:38:48', '18.00', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `mort_service_part`
--

DROP TABLE IF EXISTS `mort_service_part`;
CREATE TABLE IF NOT EXISTS `mort_service_part` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tipomort` int(10) UNSIGNED NOT NULL,
  `status` varchar(10) NOT NULL,
  `numpart` varchar(10) NOT NULL,
  `service_natural_death` varchar(1) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `datetime_death` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `fact` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `certificate` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `requestor_name` varchar(150) NOT NULL,
  `requestor_telephone` varchar(250) NOT NULL,
  `requestor_mail` varchar(50) NOT NULL,
  `exit_address` int(11) NOT NULL,
  `exit_hour` datetime NOT NULL,
  `installation_address` varchar(250) DEFAULT NULL,
  `embalmment` int(10) UNSIGNED NOT NULL,
  `cemetery_id` int(10) UNSIGNED NOT NULL,
  `comments` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `niche` varchar(250) DEFAULT NULL,
  `coffin` varchar(25) NOT NULL,
  `chapel_model` int(10) NOT NULL,
  `chapel_id` int(11) DEFAULT NULL,
  `chapel_date` date DEFAULT NULL,
  `chapel_time` time DEFAULT NULL,
  `batch_coffin` varchar(50) NOT NULL,
  `coffin_code` varchar(50) NOT NULL,
  `installed_service` int(10) UNSIGNED NOT NULL,
  `custody_flag` varchar(1) NOT NULL,
  `embalming_flag` varchar(1) NOT NULL,
  `floral_apparatus` varchar(250) DEFAULT NULL,
  `dispatch_date` date NOT NULL,
  `dispatch_time` time NOT NULL,
  `dispatch_type` int(10) NOT NULL,
  `mobility_type` int(10) NOT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `color_cloak_rug` varchar(250) DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
CREATE TABLE IF NOT EXISTS `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_persona` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_documento` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `num_documento` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`idpersona`, `tipo_persona`, `nombre`, `tipo_documento`, `num_documento`, `direccion`, `telefono`, `email`) VALUES
(1, 'Cliente', 'Ana Montenegro', 'DNI', '74125863', 'Margaritas 1368 - Chiclayo', '', ''),
(2, 'Inactivo', 'Juan Perez', 'DNI', '36985214', NULL, NULL, NULL),
(3, 'Cliente', 'Jose Martinez', 'PAS', '0174589632', 'José Gálvez 1368 - Trujillo', '96325871', 'jose@gmail.com'),
(4, 'Cliente', 'Juan Carlos Arcila', 'DNI', '47715777', 'José Gálvez 1368', '', 'jcarlos.ad7@gmail.com'),
(5, 'Proveedor', 'Soluciones Innovadoras Perú S.A.C', 'RUC', '20600121234', 'Chiclayo 0123', '931742904', 'informes.solinperu@gmail.com'),
(6, 'Proveedor', 'Inversiones SantaAna S.A.C', 'RUC', '20546231478', 'Chongoyape 01', '074963258', 'santaana@gmail.com'),
(7, 'Proveedor', '100 PRE FELIZ', 'RUC', '2055027749', 'LIMA', '111111', 'J@GMAIL.COM'),
(8, 'Proveedor', 'fff', 'DNI', '44444', 'ffff', '44444', ''),
(9, 'Cliente', 'Julio Yanarico', 'DNI', '40845459', 'Mateo Pumacahua', '932266980', 'julio.yanarico.c@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(11) NOT NULL,
  `effective_end_date` date DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cid` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Interno',
  `pwd_app` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `person_id`, `effective_end_date`, `email`, `cid`, `type`, `pwd_app`, `password`, `remember_token`, `created_at`, `updated_at`, `created_by`, `last_updated_by`) VALUES
(1, 'sysadmin', 0, '2018-10-04', 'prueba@gmail.com', '10408454255', 'Interno', 'Lima20', '$2y$10$Q5rlMuYLDFA0hEGgG4DjseIyP.iUN7FzZTbM/yAdSursgYdfvlKyy', 'm6J3VDSVnVIOgXk0Q3bS3Ummw7t4y2RrdiUNeglrHspox0zdZ3zF6wGsfrta', '2016-10-24 21:33:02', '2020-06-11 12:23:47', 0, 0),
(3, 'Admin JCM', 17, '2018-10-05', 'admin@jcm.com', NULL, 'Interno', NULL, '$2y$10$l/nHWQZBG.F/XRwZACgcYeovrC90kHPHesq46V7FRLH0V1v.LvTWC', 'PMX2ptUwil0MXtH17Xriq56dn0HLyQtRJjFu6tKrtGVDIsfyL8TjjAgKWW62', '2016-10-25 00:37:20', '2020-06-05 15:10:14', 0, 1),
(8, 'arojas', 21, '2018-05-05', 'arojas@gmail.com', NULL, 'Interno', NULL, '$2y$10$SErderIhspKVPh6KiCM8muKbM.2sEk9ulw.1VeiBfBujX7BKDwQ4e', 'X7YrjVDTQuVfMQL5qxs8NhgNNEHPD07Q8VuG8HXmJA4pQ2FWWcRvmBZETx3s', '2017-04-21 11:31:29', '2019-05-07 16:17:43', 0, 1),
(9, 'administrador', 21, '2018-05-03', 'administrador@gmail.com', '20550277849', 'Interno', 'tomate15', '$2y$10$PRj8cWNvrEM8Rt.63GtJP.dRWUo03ihUJBwA2yQNd/P9zv52.kEJq', 'GzSkOJIo3GgFnvhhJaueFb5eZyeuyJHeIfM4b8Ed0F5MCg1EfmCuX92VkH2x', '2017-04-21 11:42:33', '2020-03-23 14:44:04', 0, 0),
(10, 'almacen1', 19, '2018-12-02', 'almacen1@gmail.com', NULL, 'Interno', NULL, '11111111', '8vnBOsXT8swXiSNu1JtGL4owXA7Tz3wOZhTz5U2C4CUMhafV4Ziq3xMPeTdD', '2017-04-21 11:46:46', '2018-07-13 10:44:51', 0, 1),
(11, 'asesor', 21, '2018-08-10', 'asesor@gmail.com', NULL, 'Interno', NULL, '123456', 'ebbJ9GJaHYuvNxTmOjAnlfscijnnkwzlA3n3lpVZCLMTZdIPM9dTrfPgUz99', '2017-04-21 11:53:03', '2018-07-17 15:39:01', 0, 1),
(12, 'prueba usuario', 12, '2018-04-19', 'abc@gmail.com', NULL, 'Interno', NULL, '$2y$10$Zm8eg8kcaxa7DCEvR2tg5eRGClY4/bYiSTJ2myb4DpoKgo0Lyd136', NULL, '2018-07-12 14:40:22', '2018-07-12 14:40:22', 0, 0),
(14, 'bbb3', 14, '2018-07-11', 'administrador22@gmail.com', NULL, 'Interno', NULL, '123456789', NULL, '2018-07-13 11:21:53', '2018-07-13 11:22:38', 1, 1),
(15, 'vvvbxxxx33', 12, '2018-07-09', 'demoeeerrrrr@gmail.com', NULL, 'Interno', NULL, '987654321', NULL, '2018-07-13 11:37:07', '2018-07-13 11:37:40', 1, 1),
(16, 'asesor', 14, '2018-07-27', 'asesor1@gmail.com', NULL, 'Interno', NULL, '123456', NULL, '2018-07-17 15:41:40', '2018-07-17 15:41:40', 1, 1),
(17, 'RRRR', 21, '2018-07-18', 'DDD@APP.COM', NULL, 'Interno', NULL, '$2y$10$F9SBVoWnSXiJJ5hV2AykdOU1e3edgKUxPQU1lBAezhcDoxqFtgiEC', NULL, '2018-07-18 11:30:02', '2018-07-18 11:30:02', 0, 0),
(18, 'AD20', 19, '2018-08-02', 'AD20@gmail.com', NULL, 'Interno', NULL, '$2y$10$2FE61G9R4Y5uPQovk/EsPOfWxQQw9XCMjUbnBUy96XpU6jbMeGwgS', 'Ug5UyVMKA8OmWkhOUlTmEeJMi335Ea7HdLwhracou6uBeJDKcgBlz1foye36', '2018-07-20 07:53:51', '2018-07-20 07:57:35', 0, 0),
(19, 'AD60', 21, '2018-08-04', 'AD60@GO.COM', NULL, 'Interno', NULL, '$2y$10$cxoRFIgNNeUVAEwdqapPtu50HbdC92SznYsWHXbJh/NKSVKVsfDK6', 'GMSLYCfU3VXjRV2dTGjyvd2OD2aq2Q65RbSPLBABcMU3Th8JKGtSBfVgHPV2', '2018-07-20 08:03:46', '2018-07-20 08:10:54', 0, 0),
(20, 'cliente1@gmail.com', 19, '2018-08-24', 'cliente1@gmail.com', NULL, 'Externo', NULL, '$2y$10$ByUpJO83SpMUz4hrDu8N5OMngB5CTmXBZ6m//sbkaBwzqGZ/NY8d2', 'ZL1eka2uufXEL5iim2oenT59fgCxcCGPuwkeoFTbGymNfmiODql5ZjeWnuzt', '2018-08-17 03:56:33', '2018-08-17 03:56:33', 1, 1),
(21, 'corpac@app.com', 20, '2018-08-24', 'corpac@app.com', NULL, 'Externo', NULL, '$2y$10$/Hzy1YMEJbQH4WiMpANwIujHvmKaSKLMRWbrsSieHxdr5IlODmwPy', NULL, '2018-08-17 05:04:35', '2018-08-17 05:04:35', 1, 1),
(22, 'DEMO10', 16, '2018-11-09', 'DEMO10@APP.COM', NULL, 'Interno', NULL, '$2y$10$Ff8jCwwQv4LYCnAyU64T.uaRATW6rOocu/YTYEr9JH5hutrPkJnRG', 'ILn7XNuMrqgZqcVkrwz05ERKcl0g6t5cmyX3z433iJ4m8aGzU5Kd6PG8bkFe', '2018-10-05 02:51:49', '2018-10-05 03:20:25', 0, 0),
(23, 'demo15', 19, '2018-10-26', 'demo15@app.com', NULL, 'Interno', NULL, '$2y$10$hMPCRMSeJpC2QoW.utvJ2ukOLl4fOegb0/dHUyonzN7xsXt80UxVC', NULL, '2018-10-06 03:06:45', '2018-10-06 03:06:45', 0, 0),
(24, 'demo20', 18, '2018-10-26', 'deno20@app.com', NULL, 'Interno', NULL, '$2y$10$DkYBX5jCagQMs8.infIxcuAkkvzxnXIx82U49J8pAFf/BxK/TW1oi', NULL, '2018-10-06 03:19:50', '2018-10-06 03:19:50', 0, 0),
(25, 'demo14', 21, '2018-10-27', 'demo14@app.com', NULL, 'Interno', NULL, '$2y$10$jf.XJMZY/da3HGiYYHTCWeIoJUP2aoopoUtz0cCUqzkB64w/0ImHm', NULL, '2018-10-07 03:27:44', '2018-10-07 03:27:44', 0, 0),
(26, 'demo16', 19, '2018-11-03', 'demo16@app.com', NULL, 'Interno', NULL, '$2y$10$WwA/oMjqBeHzl62SBpl.7.d0z6lDkfxy/G9X4QUmuWuUpEttVRTwG', NULL, '2018-10-07 03:30:50', '2018-10-07 03:30:50', 0, 0),
(27, 'demo17', 18, '2018-11-03', 'demo17@app.com', NULL, 'Interno', NULL, '$2y$10$OPLb1TaGoI.bvA/R1AiLE.Ztslrh4ACfBqId/WW9LCx.AMnlAN8c6', NULL, '2018-10-07 03:35:02', '2018-10-07 03:35:02', 0, 0),
(28, 'julio cesar', 18, '2018-11-09', 'julio15@app.com', NULL, 'Interno', NULL, '$2y$10$XYlSVmJnW/qNNx980ixjMub3B57FX/rPJ2fzhN8Ucg1IB61xWkZT6', 'ZJjgFDiX9XRZDop6aDLeRz44lTaignJA4uRSLey72OHEzxSeQoOlFpEC2PaA', '2018-10-08 21:07:06', '2018-10-08 21:08:28', 0, 0),
(29, 'miguel angel', 17, '2018-11-30', 'miguelangel@app.com', NULL, 'Interno', NULL, '$2y$10$LtJVIMMe69TTTBk4Xn4hJ.vTfy3w5mVCKJyjNE/ktv7UAQhVssffC', 'K9LkB19J7yRE1Qm1UnQ1CaamyNaaVrTU4oHGnYi0zXvrz3NjjuUmZLAEKgYS', '2018-11-20 09:45:45', '2018-11-23 05:00:59', 0, 0),
(30, 'jose pedro', 16, '2018-11-30', 'jose@app.com', NULL, 'Interno', NULL, '$2y$10$paaysn5soopyS7w3xzUCYOZw.dAg8ISthYk8Py6zrmyUxM1WfImvq', NULL, '2018-11-23 05:00:25', '2018-11-23 05:00:25', 0, 0),
(31, 'LAVADO1', 21, '2019-05-31', 'lavado1@app.com', NULL, 'Interno', NULL, '$2y$10$ujKKjBcYOS1zyO2Tdt.R.eMfP6uSzZ6HiEsU2E51Bi96/HbmaP1.2', NULL, '2019-05-07 16:21:22', '2019-05-07 16:21:22', 0, 0),
(32, 'WILSON', 19, '2019-09-05', 'WILSON@APP.CM', NULL, 'Interno', NULL, '$2y$10$Jy64ejEtmL3KgOZlBIrD6.9hIF0dFAHT8yASm8eQEdTp4Vqfo93VG', 'WBAzscLKyMzNDB38KWxAVl6QEGM4uXIqn0jYKBHX4UeMj7C6Bbh06UlL1Fgh', '2019-08-30 21:22:43', '2019-08-30 21:27:45', 0, 0),
(33, 'arojas', 27, '2021-04-04', 'arojas@app.com', NULL, 'Interno', NULL, '$2y$10$u46hIH.TN8Mnawk.KQ6Bd.RxxhNoz1H0THpF1q.Zui22klZQKvgUO', NULL, '2020-03-13 18:15:31', '2020-03-13 18:15:31', 0, 0),
(34, 'jubillus', 0, '2021-04-04', 'jdd@app.com', NULL, 'Interno', NULL, '$2y$10$5eCB.N8JMyGrtnpFjKWKROYsveDYb6GymV54iCr5LR9rpu8Z6vN4u', 'vj1BZzdaP0eRrOwvqssOFg2E6IKoBy66XLEAOpeHQWmEmtorO93VPcsmxKeW', '2020-03-13 18:16:04', '2020-06-05 17:05:11', 0, 0),
(35, 'temporal1', 28, '2050-06-14', 'temporal@app.com', NULL, 'Interno', NULL, '$2y$10$ouhiOaIQn5xiNdoor94Ds.ZycFkUDeV9YaVcdKvtwtMRaaOr/bhrG', NULL, '2020-06-09 20:28:11', '2020-06-09 20:28:11', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_permisos`
--

DROP TABLE IF EXISTS `users_permisos`;
CREATE TABLE IF NOT EXISTS `users_permisos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` int(10) UNSIGNED NOT NULL,
  `permisos` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_permisos_id_user_foreign` (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_permisos`
--

INSERT INTO `users_permisos` (`id`, `id_user`, `permisos`, `created_at`, `updated_at`) VALUES
(1, 10, '20', '2017-08-09 14:26:37', '2017-08-09 16:28:40'),
(2, 1, '5,10,20,30,35,37,40,45,50,60,70,80,90,100,990,32,25,33,110,29,28,31,34,6,36,120', '2017-08-13 14:33:37', '2020-06-09 12:31:52'),
(3, 18, '10, 20,30,40,50,60,120', '2017-08-13 14:33:37', '2017-08-13 14:33:37'),
(4, 11, '30', '2017-08-13 14:33:37', '2017-08-13 14:33:37'),
(5, 28, '20,37,40', '2018-10-08 21:07:06', '2018-10-08 21:10:34'),
(6, 29, '5,10,20,40,45', '2018-11-20 09:45:45', '2020-04-13 04:22:17'),
(7, 30, '32', '2018-11-23 05:00:25', '2018-11-23 05:00:39'),
(8, 9, '5,10,20,30,35,37,40,60,32', '2018-12-19 05:00:00', '2020-03-22 12:16:20'),
(9, 31, '25', '2019-05-07 16:21:22', '2019-05-07 16:21:49'),
(10, 32, '5', '2019-08-30 21:22:43', '2019-08-30 21:24:20'),
(11, 33, '', '2020-03-13 18:15:31', '2020-03-13 18:15:31'),
(12, 34, '30', '2020-03-13 18:16:04', '2020-03-16 13:32:28'),
(13, 35, '', '2020-06-09 20:28:11', '2020-06-09 20:28:11');

-- --------------------------------------------------------

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
CREATE TABLE IF NOT EXISTS `venta` (
  `idventa` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `serie_comprobante` varchar(7) COLLATE utf8_spanish2_ci NOT NULL,
  `num_comprobante` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `total_venta` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idventa`),
  KEY `fk_venta_cliente_idx` (`idcliente`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `venta`
--

INSERT INTO `venta` (`idventa`, `idcliente`, `tipo_comprobante`, `serie_comprobante`, `num_comprobante`, `fecha_hora`, `impuesto`, `total_venta`, `estado`) VALUES
(1, 1, 'Boleta', '001', '0001', '2016-09-01 00:00:00', '18.00', '120.00', 'A'),
(3, 1, 'Boleta', '001', '0005', '2016-09-28 15:42:43', '18.00', '303.60', 'A'),
(4, 1, 'Boleta', '001', '0005', '2016-09-28 15:43:02', '18.00', '303.60', 'A'),
(5, 3, 'Boleta', '001', '0005', '2016-09-28 15:43:17', '18.00', '6.60', 'C'),
(6, 6, 'Boleta', '001', '0002', '2016-09-28 15:44:02', '18.00', '355.30', 'A'),
(7, 1, 'Boleta', '001', '00002', '2016-09-28 16:16:35', '18.00', '25.00', 'A'),
(8, 1, 'Boleta', '007', '777', '2016-09-28 16:18:15', '18.00', '16512.80', 'C'),
(9, 4, 'Boleta', '001', '00005', '2016-09-28 22:23:28', '18.00', '31.60', 'A'),
(10, 3, 'Factura', '001', '0008', '2016-09-29 16:54:13', '18.00', '580.00', 'A'),
(11, 1, 'Factura', '001', '0005', '2016-10-02 13:46:13', '18.00', '15.00', 'A'),
(12, 1, 'Boleta', '001', '0007', '2016-10-02 13:46:38', '0.00', '15.00', 'A'),
(13, 3, 'Factura', '007', '00077', '2016-10-02 14:25:12', '18.00', '40.00', 'A'),
(14, 1, 'Factura', '001', '00010', '2016-11-01 10:39:26', '18.00', '161.25', 'A'),
(15, 1, 'Boleta', '1', '22222333', '2017-07-01 17:04:58', '0.00', '29.00', 'A'),
(16, 4, 'Boleta', '1', '1014', '2018-04-16 05:20:18', '0.00', '806.00', 'C'),
(17, 3, 'Boleta', '001', '123456', '2018-06-28 22:39:06', '18.00', '1625.50', 'C'),
(18, 3, 'Factura', '11', '111', '2019-10-09 23:38:55', '18.00', '161.25', 'A'),
(19, 3, 'Boleta', '11', '11', '2020-06-10 14:40:23', '0.00', '845.00', 'A');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
