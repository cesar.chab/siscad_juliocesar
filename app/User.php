<?php

namespace sisVentas;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table='users';

    protected $primaryKey='id';
    
    protected $fillable = [
        'name', 'email', 'password','idposition','person_id','effective_end_date'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function geteffective_end_date($date)
	{
	   return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
	}


	public function seteffective_end_date($date)
	{
	   $this->attributes['effective_end_date'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
	}
}
