<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class InvMaterialTrx extends Model
{
    protected $table = 'inv_material_transactions';

    protected $primaryKey = 'transaction_id';

    protected $fillable = [
        'transaction_set_id', 
		'item_id', 
        'site_id'
        ,'subinventory_id', 
        'locator_id',
        'transaction_type_id', 
        'transaction_quantity',
        'transaction_uom' , 
        'primary_quantity',
		'primary_uom_code', 
        'transaction_date',
        'pos_line_id',
        'source_code',
        'source_line_id',
        'type_doc_id', 
        'num_doc', 
        'ref_doc', 'obs_doc', 
        'last_updated_by', 
        'created_by'
    ];

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function sucursal()
    {
        return $this->belongsTo(Site::class, 'site_id');
    }

    public function subinv()
    {
        return $this->belongsTo(InvSubInventory::class, 'subinventory_id');
    }

    public function location()
    {
        return $this->belongsTo(Locator::class, 'locator_id');
    }

    public function uom()
    {
        return $this->belongsTo(Uom::class, 'transaction_uom_id');
    }

    public function type()
    {
        return $this->belongsTo(TipoTrx::class,  'transaction_type_id');
    }

    // METHODS  20200408

    public function scopeSearch($query, $date)
    {
        
        $site_id=array_get($date, 'site_id', false);
        $transaction_type_id=array_get($date, 'transaction_type_id', false);
        $item_id=array_get($date, 'item_id', false);
        $type_source_code=array_get($date, 'type_source_code', false);
   
        $fecdesde =array_get($date, 'fechadesde', false);
        $fechasta=array_get($date, 'fechahasta', false);
        
        return $query
            ->join('inv_sites as s', 's.id', '=', 'inv_material_transactions.site_id')
            ->join('inv_transaction_types as tt', 'tt.id', '=', 'inv_material_transactions.transaction_type_id')
            ->join('inv_item as ii', 'ii.inv_item_id', '=', 'inv_material_transactions.item_id')
            ->when($site_id, function ($query) use ($site_id) {
                return $query->where('inv_sites.id', $site_id);
            })
            ->when($transaction_type_id, function ($query) use ($transaction_type_id) {
                return $query->where('inv_material_transactions.transaction_type_id', $transaction_type_id);
            })
            ->when($item_id, function ($query) use ($item_id) {
                return $query->where('inv_material_transactions.item_id', $item_id);
            })
            ->when($fecdesde, function ($query) use ($fecdesde, $fechasta) {

                $from = date('Y-m-d'. ' 00:00:00', strtotime($fecdesde) );   
                $to = date('Y-m-d'. ' 23:59:00', strtotime($fechasta) ); 

                return $query->whereBetween('inv_material_transactions.created_at', [$from, $to]);
            })
            ->select('s.name as sucursal_name', 'ii.codigo as item_codigo', 
                'tt.name as type_name',   
                'inv_material_transactions.transaction_date as transaction_date', 
                'inv_material_transactions.primary_uom_code as transaction_uom', 
                'inv_material_transactions.transaction_quantity', 'inv_material_transactions.source_code as source_code', 'inv_material_transactions.transaction_id', 'inv_material_transactions.source_line_id',  
                    'inv_material_transactions.type_doc_id',
                    'inv_material_transactions.num_doc', 'inv_material_transactions.created_by');
              
    }

}
