<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class OeOrderLines extends Model
{
    protected $table = 'oe_order_lines_all';
	
	protected $primaryKey = 'line_id';
	
	public $timestamps=true;

    protected $fillable = [
        'site_id',
        'header_id',
        'line_category_id',
        'line_number',
        'item_id',
        'ordered_item',
        'request_date',
        'schedule_ship_date',
        'schedule_arrival_date',
        'order_quantity_uom',
        'ordered_price',
        'cancelled_quantity',
        'quantity_delivered',
		'shipped_quantity',
        'ordered_quantity',
        'delivery_lead_time',
        'ship_from_org_id',
        'ship_to_org_id',
        'invoice_to_org_id',
        'sold_from_org_id',
        'sold_to_org_id',
        'cust_po_number',
        'project_id',	
		'tax_date',
        'tax_id',
        'tax_rate',
        'price_list_id',
        'pricing_date',
        'payment_term_id',
        'latest_return_date',
        'cancelled_flag',
        'open_flag',
        'salesrep_id',		
		'return_reason_code',
        'allow_return_item',
        'lock_control',		
		'agreed_date',
        'primary_uom_code',
        'primary_quantity_uom',
        'status_delivered',
        'created_by',
        'last_updated_by'
        
		
    ];
 

}
