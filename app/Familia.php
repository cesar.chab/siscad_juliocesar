<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Familia extends Model
{
    protected $table='familia';

    protected $primaryKey='idfamilia';

    public $timestamps=true;


    protected $fillable =[
    	'nombre',
    	'condicion'
    ];

    protected $guarded =[

    ];
}
