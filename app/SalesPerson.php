<?php

namespace sisVentas;
use Illuminate\Database\Eloquent\Model;

class SalesPerson extends Model
{
    protected $table = 'cc_salesrep';

    public $timestamps= true;

    protected $fillable = ['sales_credit_type_id', 'name', 'status',  
    'salesrep_number', 'org_id', 'email_address','telephone', 'PERSON_ID', 
        'created_by', 'last_updated_by' ];

    protected $primaryKey = 'salesrep_id';
 
 //CODIGO PARA RELACIONAR CON LAS TABLAS
 public function personal()
    {
       // return $this->hasOne('sisVentas\Personal');
        return $this->belongsTo(Personal::class, 'PERSON_ID');
    }
    
 }