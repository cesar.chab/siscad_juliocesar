<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table = "cc_pay_method";    
    protected $primaryKey = 'cc_method_id';
}
