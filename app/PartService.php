<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;

class PartService extends Model
{

    protected $table = "mort_service_part";

    protected $fillable = [  
	   'tipomort', 'numpart', 'status',
       'service_natural_death',
       'telephone_contacts',
       'customer_id',
       'datetime_death',
       'fact',
       'certificate',
       'requestor_id',
       'exit_address',
       'exit_hour',
       'installation_address',
       'custody',
       'embalmment',
       'cemetery',
       'comments',
       'niche',
       'coffin_id',
       'installed_service',
       'floral_apparatus',
       'dispatch_date',
       'dispatch_type',
       'mobility_type',
       'chapel_model',
       'color_cloak_rug',
       'created_by',
       'last_updated_by'
	   ];

    public function cliente()
    {
        return $this->belongsTo('sisVentas\Cliente', 'customer_id', 'idcliente');
    }

    public function tipomovilidad()
    {
        return $this->hasOne(FndLookupValue::class,'idlvalue','mobility_type');
    }
  
    public function tipodespacho()
    {
        return $this->hasOne(FndLookupValue::class,'idlvalue','dispatch_type');
    }

    public function marca()
    {
        return $this->belongsTo('sisVentas\Marca', 'idmarca', 'idmarca');
    }

    public function user()
    {
        return $this->belongsTo('sisVentas\User', 'last_updated_by');
    }

    public function createby()
    {
        return $this->belongsTo('sisVentas\User', 'created_by');
    }
 
    
}