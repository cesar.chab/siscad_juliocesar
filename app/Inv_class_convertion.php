<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Inv_class_convertion extends Model
{
    //
    protected $table = 'inv_class_convertion';

    protected $fillable = [
        'item_id',
        'from_uom_code',
        'from_uom_class',
        'to_uom_code',
        'to_uom_class',
        'convertion_rate',
        'created_at',
        'created_by',
        'updated_at',
        'last_updated_by'
    ];
}
