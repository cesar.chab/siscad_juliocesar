<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class CCTrxTypesAll extends Model
{
    protected $table='cc_cust_trx_types_all';
 
    public $timestamps=true;
 
    protected $fillable =[
					'post_to_gl',
					'accounting_affect_flag',
					'credit_memo_type_id',
					'status',
					'name',
					'description',
					'type',
					'allow_freight_flag',
					'allow_overapplication_flag',
					'creation_sign',
					'end_date',
					'enabled_flag',
					'magnetic_format_code',
					'adj_post_to_legal',
					'tax_calculation_flag',
					'last_updated_by',
					'created_by'

    ];

    protected $guarded =[

    ];


    public function ConversionType()
    {
        return $this->belongsTo(ConversionType::class, 'conversion_type');
    }

    public function getCONVERSIONDATEAttribute($date)
    {
        return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
    }

    public function setCONVERSIONDATEAttribute($date)
    {
        $this->attributes['conversion_date'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
    }
}
