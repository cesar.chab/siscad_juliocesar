<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use sisVentas\Http\Requests\VehiculoFormRequest;
use sisVentas\Vehiculo;
use sisVentas\PartService;
use sisVentas\User;
use sisVentas\createby;
use sisVentas\FndLookup;
 
 
use sisVentas\Cliente;
 
use Laracasts\Flash\Flash;
use sisVentas\Http\Requests;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Illuminate\Support\Facades\Auth;

class MortuaryController extends Controller
{

    public function index()
    {
        $datos = PartService::with('cliente')->orderBy('id', 'DESC')->get();
        return view('mortuary.service.index')->with('datos', $datos);
    }


    public function create()
    {
        $date = Carbon::now()->format('Y-m-d');

        $tipomov =  FndLookup::where('lookup_type', 'MOVILIDAD_FUNERAL')->first();
        $tipomov = (isset($tipomov) ? $tipomov->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);

        $tipodes =  FndLookup::where('lookup_type', 'TIPO_DESPACHO_FUNERARIA')->first();
        $tipodes = (isset($tipodes) ? $tipodes->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);

        $modelocap =  FndLookup::where('lookup_type', 'MODELO_CAPILLA_FUNERAL')->first();
        $modelocap = (isset($modelocap) ? $modelocap->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);        
       
        $clientes = Cliente::orderBy('full_name', 'ASC')->where('effective_end_date', '>=', $date)->lists('full_name', 'idcliente');
        
        return view('mortuary.service.create')->with('clientes', $clientes)->with('modelocap', $modelocap)->with('tipodes', $tipodes)->with('tipomov', $tipomov);
    }
    
    /**
     * Function to save PartService
     */
    public function save_service(Request $request) {
        
        if ($request->action == 'Nuevo') {
            $service = new PartService();
        } else {
            $service = PartService::findOrFail($request->id);
        }
        
        $service->tipomort = $request->tipomort;
        $service->status = "Registrado";
        $service->numpart = "";
        $service->service_natural_death = $request->service_natural_death;
        $service->customer_id = $request->customer_id;
        $service->datetime_death = $request->datetime_death;
        $service->fact = $request->fact;
        $service->certificate = $request->certificate;
        $service->requestor_name = $request->requestor_name;
        $service->requestor_telephone = $request->requestor_telephone;
        $service->requestor_mail = $request->requestor_mail;
        $service->exit_address = $request->exit_address;
        $service->exit_hour = $request->exit_hour;
        $service->installation_address = $request->installation_address;
        $service->embalmment = 0;
        $service->cemetery_id = $request->cemetery_id;
        $service->comments = $request->comments;
        $service->niche = $request->niche;
        $service->coffin = $request->coffin;
        $service->chapel_model = $request->chapel_model;
        $service->chapel_id = $request->chapel_id;
        $service->chapel_date = $request->chapel_date;
        $service->chapel_time = $request->chapel_time;
        $service->batch_coffin = $request->batch_coffin;
        $service->coffin_code = $request->coffin_code;
        $service->installed_service = 0;
        $service->custody_flag = $request->custody_flag;
        $service->embalming_flag = $request->embalming_flag;
        $service->floral_apparatus = null;
        $service->dispatch_date = $request->dispatch_date;
        $service->dispatch_time = $request->dispatch_time;
        $service->dispatch_type = $request->dispatch_type;
        $service->mobility_type = $request->mobility_type;
        $service->carrier_id = $request->carrier_id;
        $service->color_cloak_rug = $request->color_cloak_rug;
        $service->created_by =  Auth()->user()->id;
        $service->last_updated_by =  Auth()->user()->id;
        if ($service->save()) {
           if ($request->action == 'Nuevo') {
                Flash::success("Se ha registrado de manera exitosa!")->important();
           } else {
                Flash::success("Se ha actualizado de manera exitosa!")->important();
           }           
           return redirect('funeraria/servicio');//->route('mortuary.service.index');
        }
    }

    /**
     * Function to view edit
     */
    public function edit($id) {
        $service = PartService::findOrFail($id);
        if ($service) {
            $date = Carbon::now()->format('Y-m-d');
            $tipomov =  FndLookup::where('lookup_type', 'MOVILIDAD_FUNERAL')->first();
            $tipomov = (isset($tipomov) ? $tipomov->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);
    
            $tipodes =  FndLookup::where('lookup_type', 'TIPO_DESPACHO_FUNERARIA')->first();
            $tipodes = (isset($tipodes) ? $tipodes->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);
    
            $modelocap =  FndLookup::where('lookup_type', 'MODELO_CAPILLA_FUNERAL')->first();
            $modelocap = (isset($modelocap) ? $modelocap->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);        
           
            $clientes = Cliente::orderBy('full_name', 'ASC')->where('effective_end_date', '>=', $date)->lists('full_name', 'idcliente');
            return view('mortuary.service.edit')->with('service', $service)->with('clientes', $clientes)->with('modelocap', $modelocap)->with('tipodes', $tipodes)->with('tipomov', $tipomov);;
        }
    }

    /**
     * Function to delete service
     * @param {id} Id del renglon
     */
    public function delete_service(Request $request) {
        $id = $request->id;
        $service = PartService::findOrFail($id);
        $result = array();
        if ($service) {
            $service->status = "Anulado";
            $service->updated_at = Carbon::now();
            $service->last_updated_by = Auth()->user()->id;
            if ($service->save()) {
                //Flash::success("Se ha anulado el registro de manera exitosa!")->important();
                //return redirect('funeraria/servicio');
                $result = array(
                    "status" => 1,
                    "type" => "success",
                    "message" => "Se ha anulado el registro de manera exitosa!"
                );
            } else {
                $result = array(
                    "status" => 0,
                    "type" => "error",
                    "message" => "No se pudo anular el registro, intenta mas tarde!"
                );
            }
        } else {
            $result = array(
                "status" => 0,
                "type" => "warning",
                "message" => "No se encontro el registro a anular, favor de verificar!"
            );
        }
        return response()->json($result);
    }


    public function store(VehiculoFormRequest $request)
    {
        $request = $request->all();

        $request['created_by'] = Auth()->user()->id;
        $request['last_updated_by'] = Auth()->user()->id;

        $dt = Carbon::create($request['año']);
        $dt->format('Y');
        $dt->startOfYear();
        $request['año'] = $dt;

        $vehiculos = new Vehiculo($request);
        $vehiculos->save();
        $vehiculos->manyCombustions()->sync($request['combustions']);

        Flash::success("Se ha registrado de manera exitosa!")->important();

        return redirect()->route('asesor.vehiculo.index');
    }

    public function show($id)
    {
        $vehiculo = Vehiculo::find($id);
        $combustions = Combustion::orderBy('nombre', 'ASC')->lists('nombre', 'id');
        return view('asesor.vehiculo.show', compact('vehiculo'))->with('combustions', $combustions);
    }

    public function edit2($id)
    {
        $vehiculos = Vehiculo::find($id);
        $date = Carbon::now()->format('Y-m-d');
        $tipoveh =  FndLookup::where('lookup_type', 'AS_TYPE_VEHICLE')->first();
        $tipoveh = (isset($tipoveh) ? $tipoveh->fndLookupValue->pluck('description', 'idlvalue')->toArray() : []);
        $marcas = Marca::orderBy('nombre', 'ASC')->where('condicion', 1)->lists('nombre', 'idmarca');
        $modelos = Modelo::orderBy('nombre', 'ASC')->where('condicion', 1)->where('idmarca', $vehiculos->idmarca)->lists('nombre', 'idmodelo');
        $clientes = Cliente::orderBy('full_name', 'ASC')->where('effective_end_date', '>=', $date)->lists('full_name', 'idcliente');
        $combustions = Combustion::orderBy('nombre', 'ASC')->lists('nombre', 'id');
        return view('asesor.vehiculo.edit')->with('marcas', $marcas)->with('modelos', $modelos)->with('clientes', $clientes)->with('vehiculos', $vehiculos)->with('combustions', $combustions)->with('tipoveh', $tipoveh);
    }

    public function update(VehiculoFormRequest $request, Vehiculo $vehiculo)
    {
        $request = $request->all();
        $request['last_updated_by'] = Auth()->user()->id;
        $dt = Carbon::create($request['año']);
        $dt->format('Y');
        $dt->startOfYear();
        $request['año'] = $dt;

		//dd($request);

        $vehiculo->update($request);
        $vehiculo->manyCombustions()->sync($request['combustions']);
        Flash::success("El vehiculo ha sido editado con exito!")->important();
        return redirect()->route('asesor.vehiculo.index');
    }

    public function export(Request $request, Vehiculo $vehiculos)
    {
       Excel::create('Listado de vehiculos', function($excel) {
            $excel->sheet('listado', function($sheet) {
                 $vehiculos = Vehiculo::orderBy('placa', 'ASC')->get();
                $sheet->loadView('asesor.vehiculo.excel.export')->with('vehiculos', $vehiculos);
            });
        })->export('xls');
    }

    public function selectAjax(Request $request)
    {
        $idmarca = request()->get('idmarca');
        if ($request->ajax()) {
            $modelos = Modelo::orderBy('nombre', 'ASC')->where('condicion', 1)->where('idmarca', $idmarca)->lists('nombre', 'idmodelo');
            $data = view('asesor.vehiculo.partials.ajax-select', compact('modelos'))->render();
            return response()->json(['options' => $data]);
        }
    }

    public function search()
    {
        $dat = Carbon::now()->format('Y-m-d');
        $marcas = Marca::where('condicion', 1)->orderBy('nombre', 'ASC')->lists('nombre', 'idmarca');
        $modelos = Modelo::orderBy('nombre', 'ASC')->where('condicion', 1)->lists('nombre', 'idmodelo');
        $clientes = Cliente::orderBy('full_name', 'ASC')->where('effective_end_date', '>=', $dat)->lists('full_name', 'idcliente');
        $combustions = Combustion::orderBy('nombre', 'ASC')->lists('nombre', 'id');
        return view('asesor.vehiculo.query.search')->with('marcas', $marcas)->with('modelos', $modelos)->with('clientes', $clientes)->with('combustions', $combustions);
    }

    public function query(Request $request)
    {
        $vehiculos = Vehiculo::search($request)->orderBy('placa', 'ASC')->get();
        $placa=1;
        Excel::create('Lista de vehiculos consultados', function($excel) use ($vehiculos){
            $excel->sheet('Listado', function($sheet) use ($vehiculos){
                $placa=1;
                $sheet->loadView('asesor.vehiculo.excel.exportquery')->with('placa', $placa)->with('vehiculos', $vehiculos);
            });
        })->store('xls', storage_path('excel/exports/'.Auth()->user()->id.'/'));
        return view('asesor.vehiculo.query.query')->with('vehiculos', $vehiculos)->with('placa', $placa);
    }

    public function exportquery()
    {
        return response()->download(storage_path('excel/exports/'.Auth()->user()->id.'/Lista de vehiculos consultados.xls'));
    }

    public function storeVehiculoApi(Request $request)
    {
        $request = $request->all();

        $request['created_by'] = Auth()->user()->id;
        $request['last_updated_by'] = Auth()->user()->id;

        $dt = Carbon::create($request['año']);
        $dt->format('Y');
        $dt->startOfYear();
        $request['año'] = $dt;

        $vehiculos = new Vehiculo($request);
        $vehiculos->save();
        $vehiculos->manyCombustions()->sync($request['combustions']);
        $combustions = Combustion::orderBy('nombre', 'ASC')->lists('nombre', 'id');

        return response()->json(['vehiculos' => $vehiculos]);
    }

    public function getVehiculoApi($id)
    {
        $vehiculo = Vehiculo::with('manyCombustions')->find($id);
        $combustions = Combustion::orderBy('nombre', 'ASC')->lists('nombre', 'id');

        return response()->json(['vehiculo' => $vehiculo]);
    }
}
