<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use sisVentas\Http\Requests;
use sisVentas\Personal;
use sisVentas\User;
use Illuminate\Support\Facades\Redirect;
use sisVentas\Http\Requests\UsuarioFormRequest;

use Laracasts\Flash\Flash;

use DB;

class UsuarioController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request)
        {   
            $query=trim($request->get('searchText'));
            $usuarios=DB::table('users')->where('name','LIKE','%'.$query.'%')
            ->orderBy('id','desc')
            ->paginate(7);
            return view('seguridad.usuario.index',["usuarios"=>$usuarios,"searchText"=>$query]);
        }
    }

    public function create()
    {
        
        $date = Carbon::now()->format('Y-m-d');
        $personal = Personal::orderBy('FULL_NAME', 'ASC')->where('EFFECTIVE_END_DATE', '>=', $date)->lists('FULL_NAME', 'PERSON_ID');
         
        return view("seguridad.usuario.create")->with('rspersonal', $personal);

    }
    public function store (UsuarioFormRequest $request)
    {
        $usuario=new User;
        $usuario->name=$request->get('name');
        $usuario->email=$request->get('email');
        $usuario->password=bcrypt($request->get('password'));
        $usuario->effective_end_date=$request->get('effective_end_date');
        $usuario->person_id=$request->get('person_id');
        $usuario->save();

        return Redirect::to('seguridad/usuario');
    }
    public function edit($id)
    {
        return view("seguridad.usuario.edit",["usuario"=>User::findOrFail($id)]);
    }    
    public function update(UsuarioFormRequest $request,$id)
    {
        $usuario=User::findOrFail($id);
        $usuario->name=$request->get('name');
        $usuario->email=$request->get('email');
        $usuario->password=bcrypt($request->get('password'));
        $usuario->update();
        return Redirect::to('seguridad/usuario');
    }
    public function destroy($id)
    {
        $usuario = DB::table('users')->where('id', '=', $id)->delete();
        return Redirect::to('seguridad/usuario');
    }
}
