<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Inv_class_convertion;
use sisVentas\invOnhandQuantitiesDetail;
use sisVentas\Uom;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function findItem(Request $request)
    {
        $result = array();
        if (!$request->ajax()) return redirect('/');
        
        //if there is no barcode and it is inline mode
        if (empty($request->code) && $request->mode == "inline") {
            return;
        }
        
        if ($request->mode === 'inline') {
            $item = invOnhandQuantitiesDetail::join('inv_item as i', 'inv_onhand_quantities_detail.item_id', '=', 'i.inv_item_id')
            ->where('i.qb_upc_ean', $request->code)
            ->select('inv_item_id', 'nombre as nombre_item', 'primary_transaction_quantity as stock_item', 'i.list_price_per_unit as price', 'idcategoria', 'primary_uom_code')
            //->take(1)
            ->get();
        } else {
            $item = invOnhandQuantitiesDetail::join('inv_item as i', 'inv_onhand_quantities_detail.item_id', '=', 'i.inv_item_id')
            ->where('i.qb_upc_ean', $request->code)
            ->orWhere('i.nombre', "like", "%{$request->code}%")
            ->orWhere('i.descripcion', 'like', "%{$request->code}%")
            ->select('inv_item_id', 'nombre as nombre_item', 'primary_transaction_quantity as stock_item', 'i.list_price_per_unit as price', 'i.qb_upc_ean as ean', 'idcategoria', 'primary_uom_code')
            ->orderby('i.nombre', 'ASC')
            ->paginate(10);
            //->paginate(10);
        } 
        
        $data_uom = array();

        foreach ($item as $key => $i) {
            $data_uom[$key]["inv_item_id"] = $i->inv_item_id;
            //$data_uom[$key]["inv_item_id"] = $i->inv_item_id;
            $data_uom[$key]["nombre_item"] = $i->nombre_item;
            
            //Get stock item by inv_item_id and uom
            /*$stock = invOnhandQuantitiesDetail::where('item_id', '=', $i->inv_item_id)
            ->where('site_id', '=', 4)
            ->where('transaction_uom_code', $i->primary_uom_code)
            ->take(1)
            ->first();
            
            ($stock['primary_transaction_quantity']) ? $stock['primary_transaction_quantity'] : 0; //*/
            $data_uom[$key]["stock_item"] = $i->stock_item;            
            $data_uom[$key]["price"] = $i->price;
            $data_uom[$key]["ean"] = $i->ean;
            $data_uom[$key]["idcategoria"] = $i->idcategoria;
            $data_uom[$key]["primary_uom_code"] = $i->primary_uom_code;
            
            $uom = Uom::select('uom_code', 'iduom', 'description')->whereIn('idclase', static function ($q) use ($i) {
                $q->select('idclase')->from((new Uom)->getTable())
                ->where('uom_code', $i->primary_uom_code);
            })->get();
            $data_uom[$key]["uom"] = $uom;
        }

        if ($item) {
            $result = array(
                "status" => 1,
                "type" => "success",
                "message" => "Item localizado",
                "data" => $data_uom             
            );
        } else {
            $result = array(
                "status" => 0,
                "type" => "error",
                "message" => "Item no localizado",
                "data" => array()
            );
        }
        return response()->json($result);
    }

    public function getConvertionRate(Request $request) {
        $CR = Inv_class_convertion::where('from_uom_code', $request->uom)
        ->where('to_uom_code', $request->uom_primary)
        ->select('convertion_rate')
        ->first();
        return response()->json($CR);
    }

}
