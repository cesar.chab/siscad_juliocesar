<?php

namespace sisVentas\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use sisVentas\Articulo;
use sisVentas\Cliente;
use sisVentas\FndLookup;
use sisVentas\FndLookupValue;
use sisVentas\Http\Requests;
use sisVentas\Impuesto;
use sisVentas\InvMaterialTrx;
use sisVentas\OeOrderHeaders;
use sisVentas\OeOrderLines;
use sisVentas\PaymentMethod;
use sisVentas\Recibo;
use sisVentas\ReciboDetalle;
use sisVentas\Site;
use sisVentas\SiteParameters;
use sisVentas\TipoTrx;
use sisVentas\User;
use sisVentas\Venta;
use Fpdf;
use sisVentas\CCTrxTypesAll;
use sisVentas\DailyRate;
use sisVentas\Factura;
use sisVentas\FacturaDetalle;
use sisVentas\invOnhandQuantitiesDetail;
use sisVentas\Uom;

//use PDF;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = OeOrderHeaders::join('cliente as c', 'oe_order_headers_all.customer_id', '=', 'c.idcliente')
            ->join('cc_cash_receipts_all as d', 'oe_order_headers_all.header_id', '=', 'd.sales_order_id')
            ->selectRaw('oe_order_headers_all.header_id, c.full_name, d.class_code, d.trx_serial as serie, d.trx_number, oe_order_headers_all.request_date, ifnull(oe_order_headers_all.tax_code, 0) as tax_code , d.amount')
            ->orderBy('oe_order_headers_all.header_id', 'DESC')
            ->where('oe_order_headers_all.conversion_type_code', 'Venta')
            ->get();
        return view('sales.index')->with('sales', $sales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        //Get tax rate
        $taxes = Impuesto::where([
            'tax_type' => 'VENTA',
            'enabled_flag' => 1
        ])
            ->select('code_tax', 'name', 'tax_rate')
            ->orderBy('id', 'ASC')
            ->get();

        //Customers
        $customers = Cliente::orderBy('idcliente', 'ASC')->get();

        //Voucher type 
        $type = CCTrxTypesAll::all();
              
        /*FndLookup::join('fnd_lookup_value as a', 'fnd_lookup.idlookup', '=', 'a.idlookup')
            ->where('lookup_type', 'TIPO_DOC_VENTA')
            ->select('a.description', 'a.code_value', 'a.idlvalue')
            ->get()->toArray();*/

        $rSite = Site::find(4);
        $siteP = SiteParameters::where('site_id', $rSite->id)->first();
        $validateStock = $siteP->apply_validation_stock_bysite;

        $paymentMethod = PaymentMethod::where([
            "condicion" => 1,
            "apply_pos" => 1
        ])
            ->select('cc_method_id', 'name', 'pay_credit', 'description', 'req_inf_additional as additional')
            ->get();

        $uom = Uom::select('iduom', 'uom_code', 'description')->get();

        return view('sales.create')->with([
            'taxes' => $taxes,
            'customers' => $customers,
            'types' => $type, //$this->types = ['' => 'Seleccione el tipo'] + $type,
            'validateStock' => $validateStock,
            'site' => $rSite,
            'paymentMethod' => $paymentMethod,
            'uom' => $uom
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = array();
        if (!$request->ajax()) return redirect('/');
        $discount_stock = 1; //Por default mientras se valida la logica
        $data = json_decode($request->parameters);

        $order_number = OeOrderHeaders::orderBy('header_id', 'desc')->first();
        $salesrep_id = User::join('cc_salesrep as s', 'users.person_id', '=', 's.person_id')
            ->join('hr_per_people_inf as i', function ($q) {
                $q->on('s.person_id', '=', 'i.PERSON_ID')
                    ->on('users.person_id', '=', 'i.PERSON_ID');
            })
            ->select('s.salesrep_id')
            ->where('users.id', Auth()->user()->id)
            ->first();
        $receipt_number = Recibo::selectRaw('max(receipt_number) as ultimo')->first();

        try {
            \DB::beginTransaction();
            $order = new OeOrderHeaders();
            $order->order_number = $order_number->header_id + 1;
            $order->salesrep_id = (isset($salesrep_id->salesrep_id) && !empty($salesrep_id->salesrep_id)) ? $salesrep_id->salesrep_id : 0;
            $order->site_id = 4;
            $order->order_type_id = 1;
            $order->sales_channel_code = 'In Shop';
            $order->conversion_rate = 1;
            $order->conversion_rate_date = Carbon::now();
            $order->conversion_type_code = 'Venta';
            $order->customer_id  = $data[0]->customer_id;
            $order->ordered_date = Carbon::now();
            $order->shipping_method_code = $data[0]->delivery_type;
            $order->header_status = 'Created';
            $order->request_date = Carbon::now();
            $order->currency_code = $data[0]->currency;
            $order->tax_code = $data[0]->tax;
            $order->deliver_time = Carbon::now();
            $order->apply_tax = ($data[0]->tax === 0) ? 'N' : 'Y';
            $order->created_by = Auth()->user()->id;
            $order->last_updated_by = Auth()->user()->id;
            $amount_includes_tax_flag = CCTrxTypesAll::find($data[0]->voucher_type);
            if ($order->save()) {

                //Valid Post Legal
                if ($data[0]->post_legal === 'Y') {                    
                    $factura = new Factura();
                    $factura->trx_number =  $data[0]->voucher_number;
                    $factura->site_id = 999; //VALOR FIJO
                    $factura->trx_serial = $data[0]->serie;
                    $factura->class_code = $data[0]->voucher_type; //cc_cust_trx_types_all. ID  (viene del tipo de comprobante de cabecera)
                    $factura->class_trx = $factura->trx_number . "," . $factura->class_code;
                    $factura->trx_date = date('Y-m-d H:i:s');
                    $factura->amount_includes_tax_flag = $amount_includes_tax_flag->amount_includes_tax_flag; //cc_cust_trx_types_all. amount_includes_tax_flag;
                    $factura->bill_to_customer_id = $order->customer_id;
                    $factura->ship_to_customer_id = $order->customer_id;                    
                    $factura->term_due_date = date('Y-m-d H:i:s');
                    $factura->salesrep_id = Auth()->user()->id;
                    $factura->comments = "Pago Boleta";
                    $factura->exchange_rate_type = "Venta";
                    $factura->exchange_date = date('Y-m-d');
                    $factura->exchange_rate = $data[0]->tax; // VALOR_TIPO_CAMBIO
                    $factura->dayofcredit = 999; //VALOR FIJO
                    $factura->invoice_currency_code = $data[0]->currency; //CODIGO_MONEDA_SELECCIONADA
                    $factura->complete_flag = "Y";
                    $factura->status_trx = "CL";
                    $factura->tax_id = $amount_includes_tax_flag->tax_id;  //cc_cust_trx_types_all. Tax_id
                    if (!$factura->save()) {
                        $result = array(
                            "status" => 0,
                            "type" => 'error',
                            "message" => 'No se pudo generar la venta, intente de nuevo!',
                            "data" => array()
                        );
                        \DB::rollback();
                    }        
                }

                //Detail order
                $line_number = 1;
                foreach ($data[0]->detail as $key => $item) {

                    $detail = new OeOrderLines();
                    $detail->site_id = 4;
                    $detail->header_id = $order->header_id;
                    $detail->line_category_id  = $item->category;
                    $detail->line_number = $line_number;
                    $detail->item_id = $item->id;
                    $detail->ordered_item = $item->name;
                    $detail->request_date = Carbon::now();
                    $detail->schedule_ship_date = Carbon::now();
                    $detail->schedule_arrival_date = Carbon::now();
                    $detail->order_quantity_uom = $item->uom;
                    $detail->ordered_price = $item->price;
                    $detail->ordered_disccount = $item->discount;
                    $detail->ordered_quantity = $item->quantity;
                    $detail->delivery_lead_time = 0;
                    $detail->primary_uom_code = $item->uom;
                    $detail->primary_quantity_uom = 0;
                    $detail->status_delivered = 'UNDELIVERED';
                    $detail->created_by  = Auth()->user()->id;
                    $detail->last_updated_by  = Auth()->user()->id;
 
                    //Factura detalle
                    if ($factura) {
                        $factura_detalle = new FacturaDetalle();
                        $factura_detalle->customer_trx_id = $factura->customer_trx_id;
                        $factura_detalle->line_number = $line_number; // correlativo por cabecera
                        $factura_detalle->item_id = $detail->item_id;
                        $factura_detalle->description =  $detail->ordered_item; //DESCRIPCION DE ITEM
                        //$factura_detalle->previous_customer_trx_id = ;
                        //$factura_detalle->previous_customer_trx_line_id;
                        $factura_detalle->quantity_ordered = $detail->ordered_quantity; // CANTIDAD DE VENTA
                        $factura_detalle->quantity_credited = 0;
                        $factura_detalle->quantity_invoiced = $detail->ordered_quantity; //CANTIDAD DE VENTA
                        $factura_detalle->list_item_price=  $detail->ordered_price; //PRECIO DEL ARTICULO (INV_ITEM.LIST … )
                        $factura_detalle->price_disccount=  $detail->ordered_disccount;//DESCUENTO QUE SE APLICA
                        $factura_detalle->old_description = NULL;
                        //$factura_detalle->sales_order ;
                        //$factura_detalle->sales_order_revision;
                        //$factura_detalle->sales_order_line;
                        //$factura_detalle->sales_order_date;
                        $factura_detalle->line_type = "Sales"; //valor fijo = Sales;
                        //$factura_detalle->tax_id;
                        //$factura_detalle->taxable_flag;
                        //$factura_detalle->link_to_cust_trx_line_id;
                        //$factura_detalle->tax_rate;
                        $factura_detalle->uom_code = $detail->primary_uom_code;  //UDM DE VENTA EN EL DETALLE;
                        $factura_detalle->amount_includes_tax_flag = $amount_includes_tax_flag->amount_includes_tax_flag; //cc_cust_trx_types_all. amount_includes_tax_flag;
                        $factura_detalle->taxable_amount = '999999';
                        $factura_detalle->extended_amount = 1;
                        // $factura_detalle->revenue_amount;
                        $factura_detalle->last_updated_by = Auth()->user()->id;
                        $factura_detalle->created_by = Auth()->user()->id;
                        if (!$factura_detalle->save()) {
                            $result = array(
                                "status" => 0,
                                "type" => 'error',
                                "message" => 'No se pudo generar la venta, intente de nuevo!',
                                "data" => array()
                            );
                            \DB::rollback();
                        }
                    }    

                    if ($detail->save()) {

                        //save history
                        $inv = new InvMaterialTrx();
                        $inv->site_id = 4;
                        $inv->item_id = $detail->item_id; //$item->id;
                        $inv->subinventory_id = 1;
                        $inv->locator_id = 1;
                        $inv->transaction_type_id = 24; //Venta
                        $inv->transaction_quantity = $detail->ordered_quantity * -1;
                        $inv->transaction_uom = $detail->primary_uom_code;
                        $inv->transaction_price = $detail->ordered_price;
                        $inv->primary_quantity =  $detail->ordered_quantity;
                        $inv->primary_uom_code = $detail->primary_uom_code;
                        $inv->transaction_date = Carbon::now();
                        $inv->source_code = 'Venta';
                        $inv->source_header_id = $order->header_id;
                        $inv->source_line_id = $detail->line_id;
                        $inv->source_line = $detail->line_number;
                        $inv->type_doc_id = $data[0]->voucher_id;
                        $inv->num_doc = $data[0]->serie . $data[0]->voucher_number;
                        $inv->last_updated_by = Auth()->user()->id;
                        $inv->created_by = Auth()->user()->id;
                        if (!$inv->save()) {
                            \DB::rollback();
                        }

                        if ($discount_stock == 1 && $data[0]->delivery_type == 'I') {
                            //Discount stock
                            $discount = invOnhandQuantitiesDetail::where('item_id', '=', $detail->item_id)
                                ->where('site_id', '=', 4)
                                ->first();
                            if ($discount) {
                                $discount->primary_transaction_quantity = ($discount->primary_transaction_quantity - $detail->ordered_quantity);
                                if (!$discount->save()) {
                                    \DB::rollback();
                                }
                            }
                        }
                    }
                    $line_number++;
                }

                //Save payments
                $receipt = new Recibo();
                $receipt->type = 'standart';
                $receipt->receipt_number = $receipt_number->ultimo + 1;
                $receipt->receipt_date = Carbon::now();
                $receipt->currency_code = $data[0]->currency;
                $receipt->amount = $data[0]->amount;
                $receipt->receipt_due_date = Carbon::now();
                $receipt->status = 'APP';
                $receipt->pay_from_customer = $data[0]->customer_id;
                $receipt->exchange_rate_type = 'Venta';
                $receipt->exchange_rate = $data[0]->exchange_rate;
                $receipt->exchange_date = Carbon::now();
                $receipt->confirmed_flag = 'Y';
                $receipt->deposit_date = Carbon::now();
                $receipt->site_id = 4;
                $receipt->created_by = Auth()->user()->id;
                $receipt->last_updated_by = Auth()->user()->id;
                $receipt->sales_order_id = $order->header_id;
                $receipt->trx_serial = $data[0]->serie;
                $receipt->class_code = $data[0]->voucher_type;
                $receipt->trx_number = $data[0]->serie . '-' . $data[0]->voucher_number;
                if ($receipt->save()) {
                    foreach ($data[0]->payments as $key => $item) {
                        $payment = new ReciboDetalle();
                        $payment->amount_due_original = $data[0]->amount;
                        $payment->amount_due_remaining = $item->amount;
                        $payment->status = 'OP';
                        $payment->invoice_currency_code = $data[0]->currency;
                        $payment->customer_id = $data[0]->customer_id;
                        $payment->document_reference = $item->reference;
                        $payment->cash_receipt_id = $receipt->id;
                        $payment->delivery_type = $data[0]->delivery_type;
                        $payment->term_id = 5;
                        $payment->method_id = $item->method_id;
                        $payment->om_header_id =  $order->header_id;
                        $payment->amount_applied = $payment->amount_due_original;
                        $payment->last_updated_by = Auth()->user()->id;
                        $payment->created_by = Auth()->user()->id;
                        if (!$payment->save()) {
                            \DB::rollback();
                        }
                    }
                }

                $result = array(
                    "status" => 1,
                    "type" => 'success',
                    "message" => 'Venta generada correctamente # ' . $order->order_number,
                    "data" => $order
                );
                \DB::commit();
            } else {
                $result = array(
                    "status" => 0,
                    "type" => 'error',
                    "message" => 'No se pudo generar la venta, intente de nuevo!',
                    "data" => array()
                );
                \DB::rollback();
            }
        } catch (Exception $th) {
            \DB::rollback();
            $result = array(
                "status" => 0,
                "type" => 'error',
                "message" => 'No se pudo generar la venta, intente de nuevo!',
                "data" => $th->errorInfo[2]
            );
        }

        return response()->json($result);
    }

    public function show(Request $request)
    {
        $header = OeOrderHeaders::findOrFail($request->id);
        $rows = OeOrderLines::where('header_id', $header->header_id)->get();
        $receipt = Recibo::where('sales_order_id', $header->header_id)->first();
        $customers = Cliente::findOrFail($header->customer_id);
        $site = Site::findOrFail($header->site_id);
        $request['date'] = $header->ordered_date;
        $request['currency'] = $receipt->currency_code;
        $exchange_rate = self::exchange_rate($request)->getData()->data->exchange_rate;
        $payments = ReciboDetalle::join('cc_pay_method as pm', 'cc_payment_schedules_all.method_id', '=', 'pm.cc_method_id')
            ->select('pm.name', 'cc_payment_schedules_all.amount_due_remaining as amount', 'cc_payment_schedules_all.invoice_currency_code as currency', 'cc_payment_schedules_all.document_reference as reference')
            ->where('cc_payment_schedules_all.om_header_id', $header->header_id)
            ->get();
        return view('sales.ticket')->with([
            'headers' => $header,
            'rows' => $rows,
            'receipt' => $receipt,
            'customer' => $customers,
            'site' => $site,
            'exchange_rate' => $exchange_rate,
            'payments' => $payments
        ]);
    }

    public static function exchange_rate(Request $request)
    {
        $moneda = Site::find(4); //code hardcode
        $moneda_Factura = $request->currency;
        $date = (isset($request->date) || !empty($request->date)) ? $request->date : date('Y-m-d');

        if ($moneda_Factura === $moneda->currency_code) {
            return response()->json([
                'status' => 1,
                'type' => 'success',
                'message' => 'Tipo de cambio',
                'data' => [
                    'exchange_rate' => 1
                ]
            ]);
        } else {
            try {
                $daily = DailyRate::where('from_currency', '=', $moneda->currency_code)
                    ->where('to_currency', '=', $moneda_Factura)
                    ->where('conversion_date', '=', $date)
                    ->first();

                if ($daily != null) {
                    if ($daily->conversion_rate > 0) {
                        //$saveinv->exchange_rate = $daily->conversion_rate;
                        return response()->json([
                            'status' => 1,
                            'type' => 'success',
                            'message' => 'Tipo de cambio',
                            'data' => [
                                'exchange_rate' => $daily->conversion_rate
                            ]
                        ]);
                    } else {
                        return response()->json([
                            'status' => 0,
                            'type' => 'error',
                            'message' => 'No se ha ingresado tipo de cambio	no se registrara la factura',
                            'data' => []
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => 0,
                        'type' => 'error',
                        'message' => 'No se ha ingresado tipo de cambio	no se registrara la factura',
                        'data' => []
                    ]);
                }
            } catch (Exception $exception) {
                return response()->json([
                    'status' => 0,
                    'type' => 'error',
                    'message' => $exception->getCode() . ' ' . $exception->getMessage(),
                    'data' => []
                ]);
            }
        }
    }
}
