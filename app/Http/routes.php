<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Route::get('/acerca', function () {
    return view('acerca');
});

Route::resource('almacen/categoria','CategoriaController');
Route::resource('almacen/articulo','ItemController'); //'ArticuloController');
Route::resource('almacen/marca','MarcaController');
Route::resource('almacen/familia','FamiliaController');
Route::resource('almacen/modelo','ModeloController');
Route::resource('almacen/labor','LaborController');
Route::resource('asesor/vehiculo','VehiculoController');
Route::resource('ventas/cliente','ClienteController');
Route::resource('ventas/venta','VentaController');
Route::resource('compras/proveedor','ProveedorController');
Route::resource('compras/ingreso','IngresoController');
Route::resource('seguridad/usuario','UsuarioController');
Route::resource('hr/position','PositionController');
Route::resource('configuracion/uom','UomController');
Route::auth();


// personal
Route::group(['prefix' => 'hr'], function () {
    Route::post('/personal/{flag}', 'PersonalController@index')->name('all');
    Route::resource('personal', 'PersonalController');
});

Route::get('excel',  'PersonalController@export')->name('export');

Route::resource('funeraria/servicio','MortuaryController');

/*Ruta para guardar el servicio */
Route::post('funeraria/servicio/save','MortuaryController@save_service');
Route::post('funeraria/servicio/delete','MortuaryController@delete_service');

//Item
Route::get('items/search','ItemController@findItem');
Route::get('items/convertionrate','ItemController@getConvertionRate');

//Sales
Route::post('sales/store','SalesController@store');
Route::get('sales/exchangerate','SalesController@exchange_rate');
Route::resource('sales','SalesController');



 

 

Route::get('/{slug?}', 'HomeController@index');

