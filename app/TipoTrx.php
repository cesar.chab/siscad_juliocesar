<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class TipoTrx extends Model
{
    protected $table='inv_transaction_types';

    protected $primaryKey='id';

    public $timestamps=true;

    protected $fillable = [
        'name',
        'code',
        'description',
        'stock',
        'flag_system',
        'enabled',
        'created_by',
        'last_updated_by',
    ];

    
    public function user()
    {
        return $this->belongsTo('sisVentas\User', 'last_updated_by');
    }

    public function createby()
    {
        return $this->belongsTo('sisVentas\User', 'created_by');
    }

    

}