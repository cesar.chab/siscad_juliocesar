<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class OeOrderHeaders extends Model
{
    protected $table = 'oe_order_headers_all';
	
	protected $primaryKey = 'header_id';
	
	public $timestamps=true;

    protected $fillable = [
        'open_flag',
        'order_number',
        'payment_term_id',
        'salesrep_id',
        'site_id',
        'order_type_id',
        'expiration_date',
        'agreement_id',
        'cust_po_number',
        'cancelled_flag',
        'sales_channel_code',
        'conversion_rate',
        'conversion_rate_date',
		'conversion_type_code',
        'customer_id',
        'actual_address',
        'ordered_date',
        'shipment_priority_code',
        'service_type_code',
        'shipping_method_code',
        'header_status',
        'order_source',
        'request_date',	
		'currency_code',
        'billing_address',
        'orig_sys_document_ref',
        'tax_code',
        'credit_card',
        'credit_card_approval',
        'credit_card_titular',
        'deliver_time',
        'packing_instructions',
        'payment_type_code',		
		'check_number',
        'card_number',
        'credit_card_expiration_date',
        'created_by',
        'last_updated_by'
        
		
    ];
	
	
	public function cliente()
    {
        return $this->belongsTo('sisVentas\Cliente', 'customer_id', 'idcliente');
    }

    public function vendedor()
    {
        return $this->belongsTo('sisVentas\SalesPerson', 'salesrep_id', 'salesrep_id');
    }
 

}
