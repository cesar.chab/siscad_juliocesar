<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class invOnhandQuantitiesDetail extends Model
{
    protected $table = "inv_onhand_quantities_detail";
    
    protected $primaryKey = 'item_id';
}
