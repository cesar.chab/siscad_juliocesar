<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class DailyRate extends Model
{
    protected $table='gl_daily_rate';

    protected $primaryKey='rowid';

    public $timestamps=true;


    protected $fillable =[
    	'from_currency',
    	'to_currency',
    	'conversion_date',
        'conversion_type',
        'conversion_rate',
        'condicion',
        'last_updated_by',
        'created_by'

    ];

    protected $guarded =[

    ];


    public function ConversionType()
    {
        return $this->belongsTo(ConversionType::class, 'conversion_type');
    }

    public function getCONVERSIONDATEAttribute($date)
    {
        return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
    }

    public function setCONVERSIONDATEAttribute($date)
    {
        $this->attributes['conversion_date'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
    }
}
