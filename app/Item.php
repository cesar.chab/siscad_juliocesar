<?php
 
namespace sisVentas;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

use sisVentas\Locator;
use sisVentas\Inv_Onhand_Quantities_Detail;


class Item extends Model
{
    protected $table = "inv_item";
    
 	protected $primaryKey = 'inv_item_id';

    public $timestamps=true;

    protected $fillable = ['codigo','nombre','descripcion'
    						,'inventory_item_flag',
                            'stock_enabled_flag',
                            'mtl_transactions_enabled_flag',
                            'idcategoria',
							'sub_inv_id',
                            'locator_id',
                            'primary_uom_code',
                            'last_updated_by',
                            'created_by',
                            'purchasing_item_flag',
                            'allow_item_desc_update_flag',
                            'list_price_per_unit',
                            'price_buy',
                            'inventory_item_status_code',
                            'inventory_planning_code',
                            'min_minmax_quantity',
                            'max_minmax_quantity',
                            'service_item_flag',
                            'invoiceable_item_flag',
                            'returnable_flag',
                            'reserve_without_stock',
                            'customer_order_enabled_flag',
                             'percentage_of_sale',
                             'qb_upc_ean',
                             'sell_carwash'

							 ];

   
 
     public function categoria()
    {
        //return $this->belongsTo('sisVentas\Categoria', 'idcategoria', 'idcategoria');
         return $this->belongsTo(Categoria::class, 'idcategoria');
    }
 
      public function ubicacion()
    {        
         return $this->belongsTo(Locator::class, 'locator_id');
    }

	 //YASER - ITEM-MMA
	 public function mma()
	{
		return $this->hasMany(ItemMma::class, 'item_id');
	}

	//despacho de materiales
	 public function locator()
    {
        return $this->belongsTo(Locator::class, 'locator_id','location_id');
    }

    public function validateStock()
    {
        return $this->belongsTo(Inv_Onhand_Quantities_Detail::class,'inv_item_id','item_id');
    }
	
	//recepcion de OC
	    public function polinesall()
    {
        return $this->hasMany('sisVentas\PoLinesAll', 'item_id');
    }
}
