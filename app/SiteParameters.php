<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class SiteParameters extends Model
{
    protected $table = 'mtl_parameters';
	
	protected $fillable =[
    	'name','site_id','enabled',
    	 'apply_validation_stock_bysite',
    	 'days_due_date','notes_receipt',
    	 'estimate_days_due_date',
    	'last_updated_by', 'created_by'
    ];

	public $timestamps=true;
	
	protected $primaryKey='id';

      public function sucursal()
    {
        return $this->hasOne(Site::class,'id','site_id');
    }
 
	 
}
