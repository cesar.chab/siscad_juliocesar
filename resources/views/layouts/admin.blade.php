<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISCAD | www.dymhostingweb.com</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
  <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
  <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">

  <!-- Datepicker O.Leon -->
  <link rel="stylesheet" href="{{asset('css/jquery-ui-1.10.4.custom.min.css')}}">


  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('css/datepicker3.css')}}">

  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE/plugins/datatables/dataTables.bootstrap.css')}}">

  <!-- RAFAEL TORREALBA - VEN -->
  <link href="{{ asset ('AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset ('AdminLTE/plugins/datepicker/bootstrap-datepicker3.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset ('AdminLTE/plugins/datepicker/bootstrap-datepicker.standalone.css') }}" rel="stylesheet"
    type="text/css">
  <!-- /. RAFAEL TORREALBA - VEN -->

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

      <!-- Logo -->
      <a href="{{url('home')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SISCAD</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>SISCAD</b></span>
      </a>

      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Navegación</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <small class="bg-red">Online</small>
                <span class="hidden-xs">{{ Auth::user()->name }}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">

                  <p>
                    www.dymhostingweb.com - Desarrollando Software
                    <small>www.youtube.com/julio.yanarico</small>
                  </p>
                </li>

                <!-- Menu Footer-->
                <li class="user-footer">

                  <div class="pull-right">
                    <a href="{{url('/logout')}}" class="btn btn-default btn-flat">Cerrar Sesión</a>
                  </div>
                </li>
              </ul>
            </li>

          </ul>
        </div>

      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header"></li>

          <li id="liEscritorio">
            <a href="{{url('home')}}">
              <i class="fa fa-dashboard"></i> <span>Escritorio</span>
            </a>
          </li>

          <!-- ALMACEN -->
          <li id="liAlmacen" class="treeview">
            <a href="#">
              <i class="fa fa-laptop"></i>
              <span>Inventario</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Artículos
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{url('almacen/articulo')}}"><i class="fa fa-circle-o"></i> Crear Artículos</a></li>

                  <li><a href="{{url('almacen/subinventario')}}"><i class="fa fa-circle-o"></i> Asignar Marca-Modelo</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <!-- Sales -->
          <li id="liAsesor" class="treeview">
            <a href="#">
              <i class="fa fa-shopping-cart"></i>
              <span>Ventas</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li id="lismItems"><a href="{{url('sales')}}"><i class="fa fa-angle-left pull-right"></i> Ventas</a></li>
            </ul>
          </li>
          <!-- /.Sales-->

          <!-- INICIO Funeraria -->

          <li id="liAsesor" class="treeview">
            <a href="#">
              <i class="fa fa-sellsy"></i>
              <span>Funeraria</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>

            <ul class="treeview-menu">
              <li id="lismItems"><a href="{{url('funeraria/servicio')}}"><i class="fa fa-cube"></i> Parte de
                  Servicio</a></li>
              <li id="lismItems"><a href="{{url('cp/revenue')}}"><i class="fa fa-cube"></i> Aprobacion de Parte</a></li>

              <li id="lismPuestos"><a href="{{url('cp/tiposexp')}}"><i class="fa fa-circle-o"></i> Tipos
                  Egresos-Ingresos</a></li>

            </ul>

          </li>

          <!-- FIN Funeraria -->


          </li>
          <li id="liAcceso" class="treeview">
            <a href="#">
              <i class="fa fa-folder"></i> <span>Acceso</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li id="liUsuarios"><a href="{{url('seguridad/usuario')}}"><i class="fa fa-circle-o"></i> Usuarios</a>
              </li>

            </ul>
          </li>

          <li>
            <a href="{{url('acerca')}}">
              <i class="fa fa-info-circle"></i> <span>Ayuda...</span>
              <small class="label pull-right bg-yellow">IT</small>
            </a>
          </li>

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>





    <!--Contenido-->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Sistema de Ventas</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <!--Contenido-->
                    @yield('contenido')
                    <!--Fin Contenido-->
                  </div>
                </div>

              </div>
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

  </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2016-2020 <a href="www.dymhostingweb.com">Dym Hosting Web</a>.</strong> All rights
    reserved.
  </footer>


  <!-- jQuery 2.1.4 -->
  <!--
     old 02.05.17 <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script> 
     -->
  <!-- NUEVO 02.05.17  -->
  <script src="{{asset('AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
  <!--/ NUEVO -->
  @stack('scripts')
  <!-- Bootstrap 3.3.5 -->
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('js/app.min.js')}}"></script>

  <!-- Datepicker O.Leon -->
  <script type="text/javascript" src="{{asset('js/jquery-ui-1.10.4.custom.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/custom.js')}}"></script>

  <!-- DataTables App 
    <script type="text/javascript" charset="utf8" src="{{asset('js/datatables/jquery.dataTables.js')}}"></script>
-->

  <!-- RAFAEL TORREALBA - VEN. -->
  <!-- SlimScroll -->
  <script src="{{asset('AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- AdminLTE plugins-->

  <script src="{{asset('AdminLTE/plugins/datepicker/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/datepicker/locales/bootstrap-datepicker.es.js')}}"></script>

  <script type="text/javascript" src="{{ asset('AdminLTE/plugins/datatables/datatables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('AdminLTE/plugins/input-mask/jquery.inputmask.js') }}"></script>
  <script type="text/javascript" src="{{ asset('AdminLTE/plugins/input-mask/jquery.inputmask.phone.extensions.js') }}">
  </script>

  <!-- / RAFAEL TORREALBA - VEN. -->

  @yield('js')


</body>

</html>