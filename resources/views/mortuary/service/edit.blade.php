@extends('layouts.admin')
@section('title', 'Editar Servicio')
@section('contenido')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">                   
				<h3 class="box-title">Editar Parte de Servicio</h3>
			</div>
			{!!Form::open(array('url'=>'/funeraria/servicio/save','method'=>'POST','autocomplete'=>'off'))!!}
			{{Form::token()}}
			<input type="hidden" name="action" value="Editar">
			<input type="hidden" name="id" value="{{$service->id}}">			
			<div class="box-body">
				<div class="nav-tabs-custom"> 
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab_1" data-toggle="tab">INF. GENERAL</a>
						</li>
						<li>
							<a href="#tab_2" data-toggle="tab">INF. COMPLEMENTARIA</a>
						</li>
						<li>
							<a href="#tab_3" data-toggle="tab">DATOS DE APROBACION</a>
						</li>
						<li>
							<a href="#tab_4" data-toggle="tab">ADJUNTOS</a>
						</li>							 
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">								 
							<div class="row">
								<div class="col-md-12">									 
									<!--Contenido-->
									@include('mortuary.service.partials.fields')									  
								</div> 									
							</div>								 
						</div>
						<div class="tab-pane" id="tab_2">								 
							<div class="row">
								<div class="col-md-12">
									<!--Contenido-->									
									@include('mortuary.service.partials.complemtos')							
								</div>              
							</div>								 
						</div>							
						<div class="tab-pane" id="tab_3">								 
							<div class="row">
								<div class="col-md-12">
									<!--Contenido-->									
										@include('mortuary.service.partials.aprobacion') 																	 
								</div>              
							</div>								 
						</div>
						<div class="tab-pane" id="tab_4">								
							<div class="row">
								<div class="col-md-12">
									<!--Contenido-->									
									Adjunte archivos			
								</div>              
							</div>								 
						</div>							 
					</div>
				</div>
			</div>
			<div class="box-footer pull-right">
				{!! Form::submit('Registrar', ['class'=> 'btn btn-primary']) !!}
				<a class="btn btn-danger" href="">
					Cancelar
				</a>
			</div>
			{!!Form::close()!!}	
		</div>
	</div>
</div>	
@endsection

@section('js')
<script>

		

</script>
@endsection