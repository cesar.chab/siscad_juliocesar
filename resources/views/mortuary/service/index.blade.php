@extends('layouts.admin')

@section('title', 'Listado de Sucursal')

@section('contenido')
    <div class="box">
        @include('util.success')
        <div class="box-header with-border">
            <h3 class="box-title">
                Listado de Parte de Servicio
            </h3>
            <div class="box-tools">
                <div class="text-center">                     
                    <a class="btn btn-danger btn-sm" href="{{ route('funeraria.servicio.create') }}">
                        NUEVO REGISTRO
                    </a>                     
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive_ no-padding">
                        <table class="table table-hover display_ table-responsive table-condensed" id="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Num</th>
                                <th>CLIENTE</th>
 								<th>ESTADO</th> 
                                <th>ACCIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($datos as $var)
                                <tr>
                                    <td> {{ $var->id }} </td>
                                    <td> {{ $var->numpart }} </td>
									<td> {{ $var->cliente->full_name }} </td>
									<td> <span class="success">{{ $var->status }}</span> </td>									  
                                    <td>                                        
                                        @if ($var->status != "Anulado")
                                        <a href="{{URL::action('MortuaryController@edit',$var->id)}}">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" class="delete_service" data-id="{{$var->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.box -->
    </div>
@endsection

@section('js')
    <!-- SweetAlert 2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                "order": [[ 1, "desc" ]],
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
            
            //Promesa para anular el registro
            const _delete = function (id) {
                return new Promise( (resolve, reject) => {
                    $.ajax({
                        url: '/funeraria/servicio/delete',
                        method: 'post',
                        dataType: 'json',
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                        },
                        data: {id: id},
                    }).done(resolve).fail(reject);
                });
            }

            $('.delete_service').on('click', function (e) {
                e.preventDefault();
                let _id = $(this).data('id');                
                Swal.fire({
                    title: 'Estas seguro de anular el registro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, anular!',
                    cancelButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.value) {                       
                        _delete(_id).then(result => {
                            if (result.status) {
                                Swal.fire({
                                    title: 'Anulado!',
                                    text: result.message,
                                    icon: 'success'
                                }).then( () => {
                                    location.reload();
                                });
                            } else {
                                Swal.fire({
                                    title: 'Detalle encontrado!',
                                    text: result.message,
                                    icon: result.type
                                }); 
                            }
                        }).catch(error => {
                            console.log(error)
                        });
                    }
                });
            })
        });
    </script>
@endsection