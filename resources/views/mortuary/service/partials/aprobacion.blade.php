<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion de Administrador</h3>
        </div> 
		<div class="form-group">
            {!! Form::label('coffin_code', 'Codigo Ataud') !!}
            {!! Form::text('coffin_code', (isset($service->coffin_code)) ? $service->coffin_code : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
        </div>
		<div class="form-group">
            {!! Form::label('batch_coffin', 'Lote Ataud') !!}
            {!! Form::text('batch_coffin', (isset($service->batch_coffin)) ? $service->batch_coffin : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
        </div>		 
		<div class="form-group" >
            <div class="form-group row">
				<label class="col-sm-3 col-form-label-sm" align="left">Capilla </label>
				<div class="col-sm-9">
					<div>
						{!! Form::select('chapel_id', $clientes, (isset($service->chapel_id)) ? $service->chapel_id : null, ['class' => 'form-control select2', ''])!!}
					</div>
				</div>											
			</div>
        </div>		
		<div class="form-group" >
            <div class="form-group row">
				<label class="col-sm-3 col-form-label-sm"align=left >Fecha </label>
				<div class="col-sm-9">
					<div>
						<input  type="date" name="chapel_date" class="form-control form-control-sm"
						value="{{(isset($service->chapel_date)) ? $service->chapel_date : null}}">	
					</div>
				</div>											
			</div>
        </div>		
		<div class="form-group" >
            <div class="form-group row">
				<label class="col-sm-3 col-form-label-sm"align=left >Hora </label>
				<div class="col-sm-9">
					<div>
						<input type="time" name="chapel_time" class="form-control form-control-sm"
						value="{{(isset($service->chapel_time)) ? $service->chapel_time : null}}">	
					</div>
				</div>											
			</div>
        </div>		 
    </div>
</div>
<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion Movilidad </h3>
        </div>		
		<div class="form-group">
            {!! Form::label('carrier_id', 'Transportista') !!}
            {!! Form::select('carrier_id', $clientes, (isset($service->carrier_id)) ? $service->carrier_id : null, ['class' => 'form-control select2', ''])!!}
        </div>	 
    </div>
</div> 