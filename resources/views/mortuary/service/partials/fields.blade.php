<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Generales</h3>
        </div>		
		<div class="form-group">
            <div class="form-group row">
				<label class="col-sm-4 col-form-label-sm"align=left >Desceso</label>
				<div class="col-sm-6">
					<div>
						<select name="tipomort" id="tipomort" class="form-control select2" required="true">
							<option {{ (isset($service->tipomort) && $service->tipomort == 1) ? "selected": ""}} value="1">Covid-19</option>
							<option {{ (isset($service->tipomort) && $service->tipomort == 2) ? "selected": ""}} value="2">Natural</option>							 
						</select>	
					</div>
				</div>																
			</div>
        </div>		
		<label class="form-check-label" > Solo para Muerte Natural </label>
		<div class="form-group">
			<div class="form-check">
				<input class="form-check-input" value="I" type="radio" name="service_natural_death" {{ (isset($service->service_natural_death) && $service->service_natural_death == 'I') ? "checked": ""}}>
				<label class="form-check-label" >
				Instalacion
				</label>&nbsp &nbsp &nbsp  &nbsp  &nbsp
				<input class="form-check-input" value="C" type="radio" name="service_natural_death" {{ (isset($service->service_natural_death) && $service->service_natural_death == 'C') ? "checked": ""}}>
				<label class="form-check-label" >
				Covid-19
				</label>
			</div>			 
		</div>
		<div class="form-group">
			<label>Fallecimiento: Fecha y Hora</label>
			<input type="datetime-local" name="datetime_death"  class="form-control form-control-sm" 
			value="{{ (isset($service->datetime_death)) ?  \Carbon\Carbon::parse($service->datetime_death)->format('Y-m-d\TH:i') :""}}">			 
		</div>		
		<div class="form-group">
            <div class="form-group row">
				<label class="col-sm-4 col-form-label-sm"align=left >Fact</label>
				<div class="col-sm-6">
					<div>
						<input type="text" name="fact" class="form-control form-control-sm" 
						value="{{ (isset($service->fact)) ?  $service->fact :""}}">	
					</div>
				</div>											
			</div>
        </div>		
		<div class="form-group">
            <div class="form-group row">
				<label class="col-sm-4 col-form-label-sm"align=left >Certificado</label>
				<div class="col-sm-6">
					<div>
						<input  type="text" name="certificate"  class="form-control form-control-sm"
						value="{{ (isset($service->certificate)) ?  $service->certificate :""}}">	
					</div>
				</div>											
			</div>
        </div>		 
    </div>
</div>
<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion Solicitante</h3>
        </div>
		<div class="form-group">
            {!! Form::label('customer_id', 'Fallecido') !!}
            {!! Form::select('customer_id', $clientes, (isset($service->customer_id)) ? $service->customer_id : null, ['class' => 'form-control select2', ''])!!}
        </div>
		<div class="form-group">
            {!! Form::label('requestor_name', 'Nombre Solicitante') !!}
            {!! Form::text('requestor_name', (isset($service->requestor_name)) ? $service->requestor_name : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
        </div>
		<div class="form-group">
            {!! Form::label('requestor_telephone', 'Telefono(s) Solicitante') !!}
            {!! Form::text('requestor_telephone', (isset($service->requestor_telephone)) ? $service->requestor_telephone : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
        </div>
		<div class="form-group">
            {!! Form::label('requestor_mail', 'Correo Solicitante') !!}
            {!! Form::text('requestor_mail', (isset($service->requestor_mail)) ? $service->requestor_mail : null, ['class' => 'form-control', 'placeholder' => '', '']) !!}
        </div>		 
    </div>
</div>
<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informacion Salida</h3>
        </div>		  
		<div class="form-group">
			{!! Form::label('exit_address', 'Direccion Salida') !!}
			{!! Form::textarea('exit_address', (isset($service->exit_address)) ? $service->exit_address : null, ['class' => 'form-control' , 'placeholder' => 'Ingrese Direccion Instalacion' , 'rows' => 3, 'cols' => 40 , 'placeholder' => '', 'required']) !!}	 
        </div>
		<div class="form-group">
            {!! Form::label('exit_hour', 'Hora Salida') !!}
			{!! Form::time('exit_hour', (isset($service->exit_hour)) ? $service->exit_hour : null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
        </div>		
		<div class="form-group">
			{!! Form::label('installation_address', 'Direccion Instalacion') !!}
			{!! Form::textarea('installation_address', (isset($service->installation_address)) ? $service->installation_address : null, ['class' => 'form-control' , 'placeholder' => 'Ingrese Direccion Instalacion' , 'rows' => 2, 'cols' => 40 , 'placeholder' => '', 'required']) !!}	 
        </div>		 
    </div>
</div> 