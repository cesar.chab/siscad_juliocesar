@extends('layouts.admin')

@section('title', 'Listado de Categorias')

@section('contenido')
    <div class="box box-primary">
         
        <div class="box-header with-border">
            <h3 class="box-title">
                Listado de Categorias
            </h3>
            <div class="box-tools">

                <div class="text-center">
                    <a class="btn btn-danger btn-sm" href="categoria/create">
                        NUEVO REGISTRO
                    </a>
                    <a class="btn btn-success btn-sm" href="{{url('reportemarcas')}}" target="_blank">
                        IMPRIMIR REPORTE
                    </a>
                </div>

            </div>

        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive no-padding">
                        <!--<table id="table" class="hover" cellspacing="0" width="100%">-->
                        <table id="table" class="table table-bordered table-striped dataTable" role="grid" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>CATEGORIA</th>
                                <th>DESCRIPCION</th>
                                <th>ESTADO</th>                                
                                <th>ACCIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($categorias as $cat)
                                <tr>
                                    <td>{{ $cat->nombre}}</td>
                                    <td>{{ $cat->descripcion}}</td>
                                    <td>@if($cat->condicion == 1)
                                          Activo
                                      @else
                                          Inactivo
                                      @endif
                                    </td> 
                                    <td>
                                         
                                        <a href="{{URL::action('CategoriaController@edit',$cat->idcategoria)}}">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">

                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <!-- footer-->
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>

@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush

@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
        });
    </script>
@endsection