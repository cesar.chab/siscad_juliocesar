<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ticket | Sale</title>
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        /* @media print {
            #footer {
                position: absolute;
                bottom: 0;
            }
        } */
    </style>
</head>

<body onload="window.print();">
    <div class="wrapper">
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> SISCAD
                        <small class="pull-right">Fecha: {{date('d/m/Y', strtotime($headers->ordered_date))}}</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <address>
                        <strong>{{$site->name}}</strong><br>
                        {{$site->address}}<br>
                        {{$site->city}}, {{$site->code_postal}}<br>
                        {{$site->telef}}<br>
                        Email: {{$site->email}}
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    Cliente
                    <address>
                        <strong>{{$customer->full_name}}</strong><br>
                        {{$customer->address}}<br>
                        Tel: {{$customer->telef1}}<br>
                        Email: {{$customer->email_address}}
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>Boleta #</b> {{$headers->order_number}}<br>
                    <br>
                    <b>Serie: </b> {{$receipt->trx_number}}<br>
                    <b>Moneda:</b> {{$receipt->currency_code}}<br>
                    <b>Tipo Cambio:</b> {{number_format($exchange_rate, 2)}}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    @php
                    $labelTax = ($receipt->currency_code=='USD') ? "$" : $site->label_tax_code;
                    @endphp
                    <table class="table table-striped table-bordered_">
                        <thead>
                            <tr>
                                <th>CANT.</th>
                                <th>ÍTEM</th>
                                <th class="text-right">VALOR U.</th>
                                <th class="text-right">DESC.</th>
                                <th class="text-right">SUBTOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $subtotal = 0; @endphp
                            @foreach ($rows as $item)
                            @php
                            $subtotal += (($item->ordered_price * $item->ordered_quantity) * $exchange_rate)
                            @endphp
                            <tr>
                                <td>{{$item->ordered_quantity}}</td>
                                <td>{{$item->ordered_item}}</td>
                                <td class="text-right">{{$item->ordered_price}}</td>
                                <td class="text-right">{{$item->ordered_disccount}}</td>
                                <td class="text-right">
                                    {{number_format(($item->ordered_price * $item->ordered_quantity * $exchange_rate), 2)}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                    {{-- <table class="table table-striped table-bordered_" cellspacing="0" cellpadding="0">
                        <tr>
                            <td rowspan="3">
                                <table class="table-striped" border="1" cellspacing="0" cellpadding="0">
                                    <caption>Pago efectuado:</caption>
                                    <thead>
                                        <th>Metodo</th>
                                        <th>Importe</th>
                                        <th>Referencia</th>
                                    </thead>
                                    @foreach ($payments as $key=>$p)
                                    <tr>
                                        <td>{{$p->name}}</td>
                    <td>{{$labelTax}} {{$p->amount}}</td>
                    <td>{{$p->reference}}</td>
                    </tr>
                    @endforeach
                    </table>
                    </td>
                    <td class="pull-right_ text-bold">Subtotal:</td>
                    <td class="pull-right">
                        {{number_format(((($subtotal - (($headers->tax_code * 1 * $subtotal) / 100) ) * 1) + 0) * 1, 2)}}
                    </td>
                    </tr>
                    <tr>
                        <td class="pull-right_ text-bold">Impuesto ({{$headers->tax_code}} %)</td>
                        <td class="pull-right">
                            {{ number_format(($headers->tax_code * 1 * $subtotal) / 100, 2) }}</td>
                    </tr>
                    <tr>
                        <td class="pull-right_ text-bold">TOTAL:</td>
                        <td class="pull-right">{{$labelTax}} {{number_format($subtotal, 2)}}</td>
                    </tr>
                    </table> --}}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-6">
                    <p class="lead">Metodos de Pago:</p>
                    <table class="table">
                        <thead>
                            <th>Metodo</th>
                            <th>Importe</th>
                            <th>Referencia</th>
                        </thead>
                        @foreach ($payments as $key=>$p)
                        <tr>
                            <td>{{$p->name}}</td>
                            <td>{{$labelTax}} {{$p->amount}}</td>
                            <td>{{$p->reference}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.col -->
                <div class="col-xs-6">
                    <table class="table pull-right">
                        <tr>
                            <td class="text-bold text-right">Subtotal:</td>
                            <td class="pull-right">
                                {{number_format(((($subtotal - (($headers->tax_code * 1 * $subtotal) / 100) ) * 1) + 0) * 1, 2)}}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right text-bold">Impuesto ({{$headers->tax_code}} %)</td>
                            <td class="pull-right">
                                {{ number_format(($headers->tax_code * 1 * $subtotal) / 100, 2) }}</td>
                        </tr>
                        <tr>
                            <td class="text-right text-bold">TOTAL:</td>
                            <td class="pull-right">{{$labelTax}} {{number_format($subtotal, 2)}}</td>
                        </tr>
                       
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- ./wrapper -->
</body>

</html>