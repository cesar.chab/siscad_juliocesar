@extends('layouts.admin')

@section('title', 'Listado de Sucursal')

@section('contenido')
<div class="box">
    @include('util.success')
    <div class="box-header with-border">
        <h3 class="box-title">
            <i class="fa fa-sales"></i> Ventas
        </h3>
        <div class="box-tools">
            <div class="text-center">
                <a class="btn btn-success" href="{{ route('sales.create') }}">
                   <i class="fa fa-shopping-cart"></i> Nueva Venta
                </a>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-body table-responsive_ no-padding">
                    <table class="table table-hover display_ table-responsive table-condensed" id="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Tipo Comprobante</th>
                                <th>Serie Comprobante</th>
                                <th>Núm. Comprobante</th>
                                <th>Fecha Venta</th>
                                <th>Impuesto</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sales as $s)
                            @php
                            $slice = explode('-', $s->trx_number);
                            @endphp
                            <tr>
                                <td>{{$s->header_id}}</td>
                                <td>{{$s->full_name}}</td>
                                <td>{{$s->class_code}}</td>
                                <td>{{$s->serie}}</td>
                                <td>{{$slice[1]}}</td>
                                <td>{{$s->request_date}}</td>
                                <td>{{number_format(($s->tax_code * 1 * $s->amount) / 100, 2)}}</td>
                                <td>{{number_format($s->amount, 2)}}</td>
                                <td>
                                    <a target="_blank" href="/sales/ticket?id={{$s->header_id}}" class="btn btn-info btn-xs"><i class="fa fa-2x fa-print"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <!-- /.box -->
</div>
@endsection
@section('js')
<!-- SweetAlert 2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            "order": [
                [0, "desc"]
            ]
            , "language": {
                "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
            }
        });
    });

</script>
@endsection
